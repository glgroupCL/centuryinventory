package glgroup.centuryinventory.Services.EnvioCorreo;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Environment;
import android.os.StrictMode;
import android.widget.Toast;

//import com.ajts.androidmads.library.SQLiteToExcel;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.util.WorkbookUtil;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import es.dmoral.toasty.Toasty;
import glgroup.centuryinventory.Layout.MainActivity;
import glgroup.centuryinventory.R;
import glgroup.centuryinventory.Services.Memory.Local;
import glgroup.centuryinventory.Services.Model.CargaDescarga;
import glgroup.centuryinventory.Services.Model.Inventario;
import glgroup.centuryinventory.Services.SQLite.SQLOpenHelper;
import glgroup.centuryinventory.Services.SQLite.SQLiteQuery;

public class CorreoInformacion {

    public void exportar(final Context context, Activity activity) {
        Local.setData("correo", context, "estado","");
        Date fecha = new Date();
        String fecha_actual = new SimpleDateFormat("dd-MM-yyyy").format(fecha);
        SQLOpenHelper adminSQLOpenHelper = new SQLOpenHelper(context);

        final String nombreArchivo = "Inventario_"+fecha_actual;

        final String directory_path = Environment.getExternalStorageDirectory().getPath() + "/ExcelInventario/";
        File file = new File(directory_path);
        if (!file.exists()) {
            file.mkdirs();
        }
        onWriteClick(nombreArchivo, directory_path, context, activity);
        // Export SQLite DB as EXCEL FILE
//        SQLiteToExcel sqliteToExcel = new SQLiteToExcel(context, adminSQLOpenHelper.DATABASE_NAME, directory_path);
//        sqliteToExcel.exportSpecificTables(table1List, nombreArchivo , new SQLiteToExcel.ExportListener() {
//            @Override
//            public void onStart() {
//            }
//
//            @Override
//            public void onCompleted(String filePath) {
//                EnviarCorreo(directory_path, nombreArchivo, context, Local.getData("correo", context, "observacion"));
//            }
//
//            @Override
//            public void onError(Exception e) {
//                Local.setData("correo", context, "estado","false");
//                Toasty.error(context, "Error al exportar la información", Toast.LENGTH_SHORT, true).show();
//            }
//        });
    }

    public void onWriteClick(String nombre, String ruta, Context context, Activity activity) {

        XSSFWorkbook workbook = new XSSFWorkbook();
        XSSFSheet sheet = workbook.createSheet(WorkbookUtil.createSafeSheetName("Inventario"));

        Row row = sheet.createRow(0);
        Cell cell = row.createCell(0);
        Cell cell2 = row.createCell(1);
        Cell cell3 = row.createCell(2);
        cell.setCellValue("SKU");
        cell2.setCellValue("DESCRIPCION");
        cell3.setCellValue("CANTIDAD");

        SQLiteQuery sqLiteQuery = new SQLiteQuery(context);
        ArrayList<Inventario> data = sqLiteQuery.spSel_resumenInventarioCorreo();

        for (int i=1; i<= data.size();i++) {
            row = sheet.createRow(i);
            cell = row.createCell(0);
            cell2 = row.createCell(1);
            cell3 = row.createCell(2);

            cell.setCellValue(data.get(i-1).getSku());
            cell2.setCellValue(data.get(i-1).getDescripcion());
            cell3.setCellValue(data.get(i-1).getCantidad());
        }
        String outFileName = nombre+".xlsx";
        try {
            //       printlnToUser("writing file " + outFileName);
            //   File cacheDir = getCacheDir();
            File outFile = new File(ruta, outFileName);
            OutputStream outputStream = new FileOutputStream(outFile.getAbsolutePath());
            workbook.write(outputStream);
            outputStream.flush();
            outputStream.close();

            EnviarCorreo(ruta, outFileName, context, Local.getData("correo", context, "observacion"), "Inventario RFID ", "default");

        } catch (Exception e) {
            System.out.println(e.toString());


        }
    }


    public void EnviarCorreo(String RutaArchivo, String NombreArchivo, Context context,String observaciones, String asunto, String correo){

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        SQLiteQuery sqLiteQuery = new SQLiteQuery(context);

        ArrayList<String> correos = sqLiteQuery.spSel_Correos();

        Properties props = new Properties();
        props.put("mail.smtp.auth", true);
        props.put("mail.smtp.starttls.enable",true);
        props.put("mail.smtp.host", "smtp.office365.com");
        props.put("mail.smtp.port", "587");

        try {

            Session session = Session.getInstance(props,
                    new javax.mail.Authenticator() {
                        protected PasswordAuthentication getPasswordAuthentication() {
                            return new PasswordAuthentication("no-reply@glgroup.cl", "ToQNGL391#.");
                        }
                    });

            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress("no-reply@glgroup.cl"));
            if(correo.equals("default")) {
                for (String correoTo : correos) {
                    message.addRecipient(Message.RecipientType.TO, new InternetAddress(correoTo.trim()));
                }
            }else{
                message.addRecipient(Message.RecipientType.TO, new InternetAddress(correo.trim()));
            }
            message.setSubject(asunto+new SimpleDateFormat("dd-MM-yyyy HH:mm:ss").format(new Date()));
            BodyPart messageBodyPart = new MimeBodyPart();
            messageBodyPart.setText(observaciones);
            Multipart multipart = new MimeMultipart();
            multipart.addBodyPart(messageBodyPart);
            messageBodyPart = new MimeBodyPart();
            String filename = RutaArchivo+NombreArchivo;
            DataSource source = new FileDataSource(filename);
            messageBodyPart.setDataHandler(new DataHandler(source));
            messageBodyPart.setFileName(NombreArchivo);
            String msgBody = context.getString(R.string.correo_enviado_desde_century);
            DataHandler dh = new DataHandler(msgBody,"text/plain");
            message.setDataHandler(dh);
            multipart.addBodyPart(messageBodyPart);
            message.setContent(multipart);
            Transport.send(message);

            sqLiteQuery.clearResumenInventario();
            sqLiteQuery.clearInventario();
            EliminarArchivos(new File("/storage/emulated/0/ExcelInventario"));
            Local.setData("correo", context, "estado","true");



        } catch (MessagingException e) {
            Local.setData("correo", context, "estado","false");
            EliminarArchivos(new File("/storage/emulated/0/ExcelInventario"));

        }
    }

    void EliminarArchivos(File ArchivoDirectorio) { /* Parametro File (Ruta) */
        if (ArchivoDirectorio.isDirectory()) /* Si es Directorio */
        {
            /* Recorremos sus Hijos y los ELiminamos */
            for (File hijo : ArchivoDirectorio.listFiles())
                EliminarArchivos(hijo); /*Recursividad Para Saber si no hay Subcarpetas */
        }
        else
            ArchivoDirectorio.delete(); /* Si no, se trata de un File ,solo lo Eliminamos*/
    }
}
