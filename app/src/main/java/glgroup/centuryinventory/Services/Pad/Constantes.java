package glgroup.centuryinventory.Services.Pad;

public class Constantes {

    public static final String VERSION_APP = "3.10.5463";
    public static final String PARAMETRO_ACT = "url";

    public static final String TOKEN = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJkYXRhIjpbeyJpZF91c3VhcmlvIjoxOCwiZGVzY3JpcGNpb24iOiJVc3VhcmlvOiBVc3VhcmlvIFRlc3QgMSBDZW50dXJ5In1dLCJpYXQiOjE1OTUyNjMwNjksImV4cCI6MTU5NTI5MTg2OX0.5YNGzGwZwNSCZFGXltllntLU5xmPM5Q9RNt1ICBMfLM";

    /** firebase **/
    public static final String COLLECTION_CONEXION = "Conexion";
    public static final String DOCUMENT_CONEXION = "CenturyInventory";

    public static final String COLLECTION_ACTUALIZACION= "VersionCenturyMovil";
    public static final String DOCUMENT_ACTUALIZACION = "Actualizacion";

    public static final String COLLECTION_LOGIN = "DataLogin";

    public static final String BD_NAME = "bdCenturyMovil";

    /**
     * codigo para acceso a la camara
     **/
    public static final int CODIGO_PERMISOS_CAMARA = 3;
    public static final int CODIGO_INTENT = 4;

    /** imei **/
    final public static String IMEI = "866502031287673";


    /** Vistas **/
    public static final String LOGIN_CREDENTIAL = "LoginCredential";

    /** bloqueo de etiquetas**/
    final public static String KILL_PASSWORD = "27324640";
    final public static String ACCESS_PASSWORD = "86144571";


    /** enlaces **/
    final public static String ENLACE = "1";
    final public static String ENLACE_UNICO = "1";
    final public static String ENLACE_MASIVO = "2";


    /** desenlace **/
    final public static String DESENLACE = "1";
    final public static String DESENLACE_UNICO = "1";
    final public static String DESENLACE_MASIVO = "2";


    /** mensajes de lectura **/

    final public static int FLAG_START = 0;//开始
    final public static int FLAG_STOP = 1;//停止
    final public static int FLAG_UPDATE_TIME = 2; // 更新时间
    final public static int FLAG_UHFINFO = 3;
    final public static int FLAG_SUCCESS = 10;//成功
    final public static int FLAG_FAIL = 11;//失败
    public static final int FLAG_BARCODE = 4;

    public static final String SHOW_HISTORY_CONNECTED_LIST = "showHistoryConnectedList";
    public static final String TAG_DATA = "tagData";
    public static final String TAG_EPC = "tagEpc";
    public static final String TAG_TID = "tagTid";
    public static final String TAG_LEN = "tagLen";
    public static final String TAG_COUNT = "tagCount";
    public static final String TAG_RSSI = "tagRssi";

    /** tipos de documentos **/
    final public static String DOC_ENTRADA = "1";
    final public static String DOC_SALIDA = "2";
}
