package glgroup.centuryinventory.Services.Alert;

import android.app.Activity;
import android.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import glgroup.centuryinventory.R;


public class AlertCarga {

    AlertDialog alertImagen;

    String txt;
    Activity context;
    LayoutInflater layoutInflater;

    public AlertCarga(String txt, Activity context, LayoutInflater layoutInflater) {
        this.txt = txt;
        this.context = context;
        this.layoutInflater = layoutInflater;
    }

    /** ventana de carga para las vistas **/
    public  void alertCarga() {

        final android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(context);
        LayoutInflater inflater = layoutInflater;

        View dialoglayout = inflater.inflate(R.layout.alert_carga, null);
        final TextView mensaje = dialoglayout.findViewById(R.id.mensaje);
        mensaje.setText(txt);

        builder.setView(dialoglayout);
        alertImagen = builder.create();
        alertImagen.setCancelable(false);
    }

    public void alertShow(){
        if(!alertImagen.isShowing()){
            alertImagen.show();
        }
    }

    public void alerDismiss(){
        if(alertImagen.isShowing()){
            alertImagen.dismiss();
        }
    }
}
