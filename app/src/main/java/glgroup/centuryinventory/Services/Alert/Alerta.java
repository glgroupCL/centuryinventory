package glgroup.centuryinventory.Services.Alert;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;

import androidx.appcompat.app.AlertDialog;
import androidx.cardview.widget.CardView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

import glgroup.centuryinventory.Layout.CargaItem.MainCargaItemActivity;
import glgroup.centuryinventory.Layout.Desenlace.MainDesenlaceActivity;
import glgroup.centuryinventory.Layout.Desenlace.MainDesenlaceMasivoActivity;
import glgroup.centuryinventory.Layout.MainActivity;
import glgroup.centuryinventory.R;
import glgroup.centuryinventory.Services.Adaptador.AdaptadorCargaDescarga;
import glgroup.centuryinventory.Services.Adaptador.AdaptadorListaCargaDescarga;
import glgroup.centuryinventory.Services.Model.CargaDescarga;


public class Alerta {

    Context context;
    LayoutInflater inflater;

    public Alerta(Context context, LayoutInflater layoutInflater) {
        this.context = context;
        this.inflater = layoutInflater;
    }

    public Alerta(Context context){
        this.context = context;
    }

    public Alerta() {

    }

//    //mensaje error de conexión
//    public void server_error(String msj, String title){
//
//
//        View dialoglayout = inflater.inflate(R.layout.conecction_error, null);
//
//        final AlertDialog.Builder builder = new AlertDialog.Builder(context);
//        builder.setTitle(title);
//        builder.setMessage(msj);
//
//        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
//            @Override
//            public void onClick(DialogInterface dialog, int which) {
//            }
//        });
//        builder.setView(dialoglayout);
//        builder.show();
//
//    }

    /** muestra las observaciones de un documento **/
    public static void errorArchivo(Activity activity){

        //Alerta para mostrar ayuda de carga
        final android.app.AlertDialog alertDescarga;
        final android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(activity);
        final LayoutInflater inflater = activity.getLayoutInflater();

        View dialoglayout = inflater.inflate(R.layout.alert_error_archivo, null);
        Button ok = (Button) dialoglayout.findViewById(R.id.btnOk);
        ImageView img = dialoglayout.findViewById(R.id.img);
        Glide.with(activity.getApplicationContext()).asGif().load("file:///android_asset/img_test.gif").into(img);
        builder.setView(dialoglayout);

        alertDescarga = builder.create();
        alertDescarga.setCancelable(false);
        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDescarga.dismiss();
            }
        });
        alertDescarga.show();
    }

    public static void alertErrorCarga(Activity activity){
        final android.app.AlertDialog alertCarga;
        final android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(activity);
        final LayoutInflater inflater = activity.getLayoutInflater();

        View dialoglayout = inflater.inflate(R.layout.alert_referencia_doc, null);
        Button ok = (Button) dialoglayout.findViewById(R.id.btnOk);
        builder.setView(dialoglayout);

        alertCarga = builder.create();
        alertCarga.setCancelable(false);
        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                alertCarga.dismiss();
            }
        });
        alertCarga.show();
    }

    public static void alertErrorCargaBase(Activity activity){
        final android.app.AlertDialog alertCarga;
        final android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(activity);
        final LayoutInflater inflater = activity.getLayoutInflater();

        View dialoglayout = inflater.inflate(R.layout.alert_error_carga, null);
        Button ok = (Button) dialoglayout.findViewById(R.id.btnOk);
        builder.setView(dialoglayout);

        alertCarga = builder.create();
        alertCarga.setCancelable(false);
        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                alertCarga.dismiss();
            }
        });
        alertCarga.show();
    }

    public static void alertListErrorCargaBase(Activity activity, ArrayList<CargaDescarga> errorCarga){
        final android.app.AlertDialog alertCarga;
        final AdaptadorListaCargaDescarga mAdapter;
        final android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(activity);
        final LayoutInflater inflater = activity.getLayoutInflater();

        View dialoglayout = inflater.inflate(R.layout.alert_list_error_carga, null);
        Button ok = (Button) dialoglayout.findViewById(R.id.btnOk);
        ListView listaElementos = dialoglayout.findViewById(R.id.listaElementos);

        mAdapter = new AdaptadorListaCargaDescarga(activity.getApplicationContext(),errorCarga);
        listaElementos.setAdapter(mAdapter);

        builder.setView(dialoglayout);

        alertCarga = builder.create();
        alertCarga.setCancelable(false);
        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                alertCarga.dismiss();
            }
        });
        alertCarga.show();
    }


    //Mensaje de alerta general
    public static void Alerta(String msj, String tit, Activity activity) {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setTitle(tit);
        builder.setMessage(msj)
                .setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        //do things
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
    }

    public static void AlertaAutorizado(String msj, String tit, final Activity activity) {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setTitle(tit);
        builder.setMessage(msj)
                .setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        activity.finish();
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
    }

    /** solicita el tipo de etiquetas a buscar **/
    public static void alertDesenlace(Context context, LayoutInflater layoutInflater, final Activity activity){
        final android.app.AlertDialog alertEnlace;
        final android.app.AlertDialog.Builder busqueda = new android.app.AlertDialog.Builder(activity);


        View dialog = layoutInflater.inflate(R.layout.alert_select_desenlace, null);

        final Button unico = dialog.findViewById(R.id.btnUnico);
        final Button masivo = dialog.findViewById(R.id.btnMasivo);

        busqueda.setView(dialog);
        alertEnlace = busqueda.create();
        alertEnlace.setCancelable(true);

        unico.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertEnlace.dismiss();
                activity.startActivity(new Intent(activity, MainDesenlaceActivity.class));

            }
        });
        masivo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertEnlace.dismiss();
                activity.startActivity(new Intent(activity, MainDesenlaceMasivoActivity.class));
            }
        });
        alertEnlace.show();
    }

    /** alerta para indicar que la aplicación no puede ser utilizada, ya que la entena no esta disponible**/
    public static void antenaEnUso(final Activity activity){
        final androidx.appcompat.app.AlertDialog.Builder dialogo1 = new androidx.appcompat.app.AlertDialog.Builder(activity);
        dialogo1.setTitle(activity.getApplication().getString(R.string.aviso));
        dialogo1.setMessage(activity.getString(R.string.error_antena));
        dialogo1.setCancelable(false);
        dialogo1.setPositiveButton(activity.getApplication().getString(R.string.confirmar), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogo1, int id) {
                dialogo1.dismiss();
                //activity.startActivity(new Intent(activity, MainActivity.class));
                activity.finish(); activity.finishAffinity(); System.exit(0);
            }
        });
        dialogo1.show();
    }

    public static void idioma(LayoutInflater layoutInflater, final MainActivity activity){
        final android.app.AlertDialog alertLanguage;
        final android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(activity);

        View dialoglayout = layoutInflater.inflate(R.layout.alert_lenguaje, null);

        final CardView cardEsp = dialoglayout.findViewById(R.id.cardEsp);
        final CardView cardIng = dialoglayout.findViewById(R.id.cardIng);
        final CardView cardFra = dialoglayout.findViewById(R.id.cardFra);
        final CardView cardPor = dialoglayout.findViewById(R.id.cardPort);
        final CardView cardChi = dialoglayout.findViewById(R.id.cardChi);

        builder.setView(dialoglayout);

        alertLanguage = builder.create();
        alertLanguage.setCancelable(true);

        cardEsp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertLanguage.dismiss();
                activity.cambiarLenguaje("es");
            }
        });
        cardIng.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertLanguage.dismiss();
                activity.cambiarLenguaje("en");
            }
        });
        cardFra.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertLanguage.dismiss();
                activity.cambiarLenguaje("fr");
            }
        });
        cardPor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertLanguage.dismiss();
                activity.cambiarLenguaje("pt");
            }
        });
        cardChi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertLanguage.dismiss();
                activity.cambiarLenguaje("zh");
            }
        });

        alertLanguage.show();
    }


}
