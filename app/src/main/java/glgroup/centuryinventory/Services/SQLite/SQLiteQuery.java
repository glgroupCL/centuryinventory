package glgroup.centuryinventory.Services.SQLite;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;

import glgroup.centuryinventory.Services.Model.CargaDescarga;
import glgroup.centuryinventory.Services.Model.Correo;
import glgroup.centuryinventory.Services.Model.Inventario;

public class SQLiteQuery {

    private Context context;

    public SQLiteQuery(Context context) {
        this.context = context;
    }


    /******************************** ACCIONES PARA TABLA ITEM **************************************/

    /*** Inserta item ***/
    public long spIns_Item(String sku, String desc){
        //Se crea el objeto admin que nos permite obtener la base de datos creada en la clase AdminSQLiteOpenHelper
        SQLOpenHelper admin = new SQLOpenHelper(context);
        //Se crea el objeto bd que nos permite abrir la base de datos
        SQLiteDatabase db = admin.getWritableDatabase();
        //Se crea el objeto registro para guardar los datos de cada editor de texto
        ContentValues reg = new ContentValues();
        //se guardan los datos de cada editor de texto en el objeto registro
        reg.put("sku", sku);
        reg.put("descripcion", desc);
        //se insertan los registros en la base de datos
        long resp = db.insert("item", null, reg);
        //se cierra la base de datos
        db.close();

        return resp;
    }
    /*** Actualiza la información del item ***/
    public void spMod_Item(String sku, String desc){
        //Se crea el objeto admin que nos permite obtener la base de datos creada en la clase AdminSQLiteOpenHelper
        SQLOpenHelper admin = new SQLOpenHelper(context);
        //Se crea el objeto bd que nos permite abrir la base de datos
        SQLiteDatabase db = admin.getWritableDatabase();
        //Se crea el objeto registro para guardar los datos de cada editor de texto
        ContentValues reg = new ContentValues();
        //se guardan los datos de cada editor de texto en el objeto registro
        reg.put("descripcion", desc);

        //se actualizan los registros en la base de datos
        db.update("item", reg, "sku=?", new String[]{sku});
        //se cierra la base de datos
        db.close();
    }



    /*** VALIDA SI EL SKU YA EXSITE ***/
    public boolean spVal_item(String sku) {

        //Se crea el objeto admin que nos permite obtener la base de datos creada en la clase AdminSQLiteOpenHelper
        SQLOpenHelper admin = new SQLOpenHelper(context);
        //Se crea el objeto bd que nos permite abrir la base de datos
        SQLiteDatabase db = admin.getWritableDatabase();
        Cursor fila = db.rawQuery("SELECT * FROM item WHERE sku = ?", new String[]{sku});

        boolean data = false;
        if (fila.moveToFirst()) {
            data = true;
        }
        db.close();
        return data;
    }



    /*** Obtiene todos los items de la tabla ***/
    public List spSel_items() {

        //Se crea el objeto admin que nos permite obtener la base de datos creada en la clase AdminSQLiteOpenHelper
        SQLOpenHelper admin = new SQLOpenHelper(context);
        //Se crea el objeto bd que nos permite abrir la base de datos
        SQLiteDatabase db = admin.getWritableDatabase();
        Cursor fila = db.rawQuery("SELECT * FROM item ORDER BY descripcion ASC", null);

        List<String> items = new ArrayList<>();
        if (fila.moveToFirst()) {
            do{
                items.add(fila.getString(2));
            }while(fila.moveToNext());
        }
        db.close();
        return items;
    }

    /*** Obtiene el id de un item por su descripcion***/
    public ArrayList spSel_idItem_desc(String desc) {

        //Se crea el objeto admin que nos permite obtener la base de datos creada en la clase AdminSQLiteOpenHelper
        SQLOpenHelper admin = new SQLOpenHelper(context);
        //Se crea el objeto bd que nos permite abrir la base de datos
        SQLiteDatabase db = admin.getWritableDatabase();
        Cursor fila = db.rawQuery("SELECT * FROM item WHERE descripcion = ?", new String[]{desc});

        ArrayList<String> item = new ArrayList<>();
        if (fila.moveToFirst()) {
            do{
                item.add(fila.getString(0));
                item.add(fila.getString(1));
            }while(fila.moveToNext());
        }
        db.close();
        return item;
    }

    /*** Obtiene el id de un item por su SKU***/
    public ArrayList spSel_idItem_sku(String sku) {

        //Se crea el objeto admin que nos permite obtener la base de datos creada en la clase AdminSQLiteOpenHelper
        SQLOpenHelper admin = new SQLOpenHelper(context);
        //Se crea el objeto bd que nos permite abrir la base de datos
        SQLiteDatabase db = admin.getWritableDatabase();
        Cursor fila = db.rawQuery("SELECT * FROM item WHERE sku = ?", new String[]{sku});

        ArrayList<String> item = new ArrayList<>();
        if (fila.moveToFirst()) {
            do{
                item.add(fila.getString(0));
                item.add(fila.getString(2));
            }while(fila.moveToNext());
        }
        db.close();
        return item;
    }

    /*** Obtiene el id de un item por su SKU***/
    public String spSel_idItem_sku_ins(String sku) {

        //Se crea el objeto admin que nos permite obtener la base de datos creada en la clase AdminSQLiteOpenHelper
        SQLOpenHelper admin = new SQLOpenHelper(context);
        //Se crea el objeto bd que nos permite abrir la base de datos
        SQLiteDatabase db = admin.getWritableDatabase();
        Cursor fila = db.rawQuery("SELECT * FROM item WHERE sku = ?", new String[]{sku});

        String id = "";
        if (fila.moveToFirst()) {
            do{
                id = fila.getString(0);
            }while(fila.moveToNext());
        }
        db.close();
        return id;
    }





    /******************************** ACCIONES PARA TABLA ITEM **************************************/
    public long spIns_rfid(String rfid, String idSku){
        //Se crea el objeto admin que nos permite obtener la base de datos creada en la clase AdminSQLiteOpenHelper
        SQLOpenHelper admin = new SQLOpenHelper(context);
        //Se crea el objeto bd que nos permite abrir la base de datos
        SQLiteDatabase db = admin.getWritableDatabase();
        //Se crea el objeto registro para guardar los datos de cada editor de texto
        ContentValues reg = new ContentValues();
        //se guardan los datos de cada editor de texto en el objeto registro
        reg.put("codigo", rfid);
        reg.put("id_sku", idSku);

        //se insertan los registros en la base de datos
        long resp = db.insert("rfid", null, reg);
        //se cierra la base de datos
        db.close();

        return resp;
    }
    /*** Valida si existen items enlazados ***/
    public boolean spSel_rfid() {

        //Se crea el objeto admin que nos permite obtener la base de datos creada en la clase AdminSQLiteOpenHelper
        SQLOpenHelper admin = new SQLOpenHelper(context);
        //Se crea el objeto bd que nos permite abrir la base de datos
        SQLiteDatabase db = admin.getWritableDatabase();
        Cursor fila = db.rawQuery("SELECT * FROM rfid", null);

        boolean resp = false;
        if (fila.moveToFirst()) {
            do{
                resp = true;
            }while(fila.moveToNext());
        }
        db.close();
        return resp;
    }


    /*** Valida si el RFID ya está registrado ***/
    public boolean spVal_rfid(String rfid) {

        //Se crea el objeto admin que nos permite obtener la base de datos creada en la clase AdminSQLiteOpenHelper
        SQLOpenHelper admin = new SQLOpenHelper(context);
        //Se crea el objeto bd que nos permite abrir la base de datos
        SQLiteDatabase db = admin.getWritableDatabase();
        Cursor fila = db.rawQuery("SELECT * FROM rfid WHERE codigo=?", new String[]{rfid});

        boolean resp = false;
        if (fila.moveToFirst()) {
            do{
                resp = true;
            }while(fila.moveToNext());
        }
        db.close();
        return resp;
    }

    public ArrayList spSel_InfoRfid(String rfid) {

        //Se crea el objeto admin que nos permite obtener la base de datos creada en la clase AdminSQLiteOpenHelper
        SQLOpenHelper admin = new SQLOpenHelper(context);
        //Se crea el objeto bd que nos permite abrir la base de datos
        SQLiteDatabase db = admin.getWritableDatabase();
        Cursor fila = db.rawQuery("SELECT * FROM rfid INNER JOIN item ON rfid.id_sku = item.idSku WHERE rfid.codigo = ?", new String[]{rfid});

        ArrayList<String> data = new ArrayList<>();
        if (fila.moveToFirst()) {
            do{
                data.add(fila.getString(4));
                data.add(fila.getString(5));
            }while(fila.moveToNext());
        }
        db.close();
        return data;
    }

    /*** Se elimina un rfid ***/
    public int spDel_rfid(String rfid){
        //Se crea el objeto admin que nos permite obtener la base de datos creada en la clase AdminSQLiteOpenHelper
        SQLOpenHelper admin = new SQLOpenHelper(context);
        //Se crea el objeto bd que nos permite abrir la base de datos
        SQLiteDatabase db = admin.getWritableDatabase();
        //instruccion para eliminar un dato, retorna 1 o 0
        int resp = db.delete("rfid", "codigo = ?", new String[]{rfid});
        //se cierra la conexion
        db.close();
        return resp;
    }





    /******************************** ACCIONES PARA TABLA INVENTARIO **************************************/

    public long spIns_inventario(String sku, String desc){
        //Se crea el objeto admin que nos permite obtener la base de datos creada en la clase AdminSQLiteOpenHelper
        SQLOpenHelper admin = new SQLOpenHelper(context);
        //Se crea el objeto bd que nos permite abrir la base de datos
        SQLiteDatabase db = admin.getWritableDatabase();
        //Se crea el objeto registro para guardar los datos de cada editor de texto
        ContentValues reg = new ContentValues();
        //se guardan los datos de cada editor de texto en el objeto registro
        reg.put("sku", sku);
        reg.put("descripcion", desc);

        //se insertan los registros en la base de datos
        long resp = db.insert("inventario", null, reg);
        //se cierra la base de datos
        db.close();

        return resp;
    }

    /*** Valida si existen elementos en la tabla de inventarios ***/
    public boolean spSel_inventario() {

        //Se crea el objeto admin que nos permite obtener la base de datos creada en la clase AdminSQLiteOpenHelper
        SQLOpenHelper admin = new SQLOpenHelper(context);
        //Se crea el objeto bd que nos permite abrir la base de datos
        SQLiteDatabase db = admin.getWritableDatabase();
        Cursor fila = db.rawQuery("SELECT * FROM inventario", null);

        boolean resp = false;
        if (fila.moveToFirst()) {
            do{
                resp = true;
            }while(fila.moveToNext());
        }
        db.close();
        return resp;
    }

    /*** Valida si existen elementos en la tabla de inventarios ***/
    public void spSel_groupInventario() {

        //Se crea el objeto admin que nos permite obtener la base de datos creada en la clase AdminSQLiteOpenHelper
        SQLOpenHelper admin = new SQLOpenHelper(context);
        //Se crea el objeto bd que nos permite abrir la base de datos
        SQLiteDatabase db = admin.getWritableDatabase();
        Cursor fila = db.rawQuery("SELECT descripcion, sku, COUNT(sku) AS cantidad FROM inventario GROUP BY sku, descripcion", null);

        ArrayList<String> data = new ArrayList<>();
        if (fila.moveToFirst()) {
            do{
                spIns_ResumenInventario(fila.getString(1),fila.getString(0), fila.getString(2));
            }while(fila.moveToNext());
        }
        db.close();
    }

    public long spIns_ResumenInventario(String sku, String desc, String cant){
        //Se crea el objeto admin que nos permite obtener la base de datos creada en la clase AdminSQLiteOpenHelper
        SQLOpenHelper admin = new SQLOpenHelper(context);
        //Se crea el objeto bd que nos permite abrir la base de datos
        SQLiteDatabase db = admin.getWritableDatabase();
        //Se crea el objeto registro para guardar los datos de cada editor de texto
        ContentValues reg = new ContentValues();
        //se guardan los datos de cada editor de texto en el objeto registro
        reg.put("sku",sku);
        reg.put("descripcion", desc);
        reg.put("cantidad", cant);

        //se insertan los registros en la base de datos
        long resp = db.insert("resumenInventario", null, reg);
        //se cierra la base de datos
        db.close();

        return resp;
    }

    public ArrayList spSel_resumenInventario() {

        //Se crea el objeto admin que nos permite obtener la base de datos creada en la clase AdminSQLiteOpenHelper
        SQLOpenHelper admin = new SQLOpenHelper(context);
        //Se crea el objeto bd que nos permite abrir la base de datos
        SQLiteDatabase db = admin.getWritableDatabase();
        Cursor fila = db.rawQuery("SELECT * FROM resumenInventario", null);

        ArrayList<Inventario> data = new ArrayList<>();
        if (fila.moveToFirst()) {
            do{
                data.add(new Inventario(fila.getString(1), fila.getString(2)));
            }while(fila.moveToNext());
        }
        db.close();
        return data;
    }

    public ArrayList spSel_resumenInventarioCorreo() {

        //Se crea el objeto admin que nos permite obtener la base de datos creada en la clase AdminSQLiteOpenHelper
        SQLOpenHelper admin = new SQLOpenHelper(context);
        //Se crea el objeto bd que nos permite abrir la base de datos
        SQLiteDatabase db = admin.getWritableDatabase();
        Cursor fila = db.rawQuery("SELECT * FROM resumenInventario", null);

        ArrayList<Inventario> data = new ArrayList<>();
        if (fila.moveToFirst()) {
            do{
                data.add(new Inventario(fila.getString(0), fila.getString(1), fila.getString(2)));
            }while(fila.moveToNext());
        }
        db.close();
        return data;
    }

    public boolean spVal_resumenInventario() {

        //Se crea el objeto admin que nos permite obtener la base de datos creada en la clase AdminSQLiteOpenHelper
        SQLOpenHelper admin = new SQLOpenHelper(context);
        //Se crea el objeto bd que nos permite abrir la base de datos
        SQLiteDatabase db = admin.getWritableDatabase();
        Cursor fila = db.rawQuery("SELECT * FROM resumenInventario", null);

        boolean data = false;
        if (fila.moveToFirst()) {
            do{
                data = true;
            }while(fila.moveToNext());
        }
        db.close();
        return data;
    }



    /******************************** ACCIONES PARA TABLA CORREO **************************************/

    public long spIns_correo(String desc, String correo){
        //Se crea el objeto admin que nos permite obtener la base de datos creada en la clase AdminSQLiteOpenHelper
        SQLOpenHelper admin = new SQLOpenHelper(context);
        //Se crea el objeto bd que nos permite abrir la base de datos
        SQLiteDatabase db = admin.getWritableDatabase();
        //Se crea el objeto registro para guardar los datos de cada editor de texto
        ContentValues reg = new ContentValues();
        //se guardan los datos de cada editor de texto en el objeto registro
        reg.put("descripcion", desc);
        reg.put("correo", correo);

        //se insertan los registros en la base de datos
        long resp = db.insert("correo", null, reg);
        //se cierra la base de datos
        db.close();

        return resp;
    }

    public int spDel_Correo(String correo){
        //Se crea el objeto admin que nos permite obtener la base de datos creada en la clase AdminSQLiteOpenHelper
        SQLOpenHelper admin = new SQLOpenHelper(context);
        //Se crea el objeto bd que nos permite abrir la base de datos
        SQLiteDatabase db = admin.getWritableDatabase();
        //instruccion para eliminar un dato, retorna 1 o 0
        int resp = db.delete("correo", "correo=?", new String[]{correo});
        //se cierra la conexion
        db.close();
        return resp;
    }

    /** VALIDA SI EXISTEN CORREOS **/
    public ArrayList spSel_Correos() {
        //Se crea el objeto admin que nos permite obtener la base de datos creada en la clase AdminSQLiteOpenHelper
        SQLOpenHelper admin = new SQLOpenHelper(context);
        //Se crea el objeto bd que nos permite abrir la base de datos
        SQLiteDatabase db = admin.getWritableDatabase();
        Cursor fila = db.rawQuery("SELECT * FROM correo" , null);

        ArrayList<String> correos = new ArrayList<>();
        if (fila.moveToFirst()) {
            do{
                correos.add(fila.getString(1));
            }while (fila.moveToNext());
        }
        db.close();
        return correos;

    }
    /** Se valida si existen correos registrados **/
    public boolean spVal_ExistCorreo() {
        //Se crea el objeto admin que nos permite obtener la base de datos creada en la clase AdminSQLiteOpenHelper
        SQLOpenHelper admin = new SQLOpenHelper(context);
        //Se crea el objeto bd que nos permite abrir la base de datos
        SQLiteDatabase db = admin.getWritableDatabase();
        Cursor fila = db.rawQuery("SELECT * FROM correo", null);
        boolean resp = false;

        if (fila.moveToFirst()) {
            resp = true;
        }
        db.close();
        return resp;
    }

    /** VALIDA SI EL CORREO YA ESTA REGISTRADO **/
    public boolean spSel_ExistCorreo(String correo) {
        //Se crea el objeto admin que nos permite obtener la base de datos creada en la clase AdminSQLiteOpenHelper
        SQLOpenHelper admin = new SQLOpenHelper(context);
        //Se crea el objeto bd que nos permite abrir la base de datos
        SQLiteDatabase db = admin.getWritableDatabase();
        Cursor fila = db.rawQuery("SELECT * FROM correo WHERE correo = ?" , new String[]{correo});
        boolean resp = false;

        if (fila.moveToFirst()) {
            resp = true;
        }
        db.close();
        return resp;
    }

    /** Retorna todos los correos**/
    public ArrayList spSel_ListaCorreo() {
        //Se crea el objeto admin que nos permite obtener la base de datos creada en la clase AdminSQLiteOpenHelper
        SQLOpenHelper admin = new SQLOpenHelper(context);
        //Se crea el objeto bd que nos permite abrir la base de datos
        SQLiteDatabase db = admin.getWritableDatabase();
        Cursor fila = db.rawQuery("SELECT * FROM correo ", null);
        ArrayList<Correo> listaCorreo = new ArrayList<>();

        if (fila.moveToFirst()) {
            do{
                listaCorreo.add(new Correo(fila.getString(0), fila.getString(1)));
            }while (fila.moveToNext());
        }
        db.close();
        return listaCorreo;

    }

    /** Obtiene los datos del correo **/
    public ArrayList spSel_infoCorreo(String correo) {
        //Se crea el objeto admin que nos permite obtener la base de datos creada en la clase AdminSQLiteOpenHelper
        SQLOpenHelper admin = new SQLOpenHelper(context);
        //Se crea el objeto bd que nos permite abrir la base de datos
        SQLiteDatabase db = admin.getWritableDatabase();
        Cursor fila = db.rawQuery("SELECT * FROM correo WHERE correo = ?", new String[]{correo});
        ArrayList<String> listaCorreo = new ArrayList<>();

        if (fila.moveToFirst()) {
            do{
                listaCorreo.add(fila.getString(0));
                listaCorreo.add(fila.getString(1));
            }while (fila.moveToNext());
        }
        db.close();
        return listaCorreo;

    }


    /******************************** ACCIONES PARA TABLA AUXDESCARGA **************************************/
    public ArrayList spSel_Enlaces() {

        //Se crea el objeto admin que nos permite obtener la base de datos creada en la clase AdminSQLiteOpenHelper
        SQLOpenHelper admin = new SQLOpenHelper(context);
        //Se crea el objeto bd que nos permite abrir la base de datos
        SQLiteDatabase db = admin.getWritableDatabase();
        Cursor fila = db.rawQuery("SELECT *, COUNT(*) as cantidad FROM rfid INNER JOIN item ON rfid.id_sku = item.idSku GROUP BY rfid.id_sku", null);

        ArrayList<CargaDescarga> data = new ArrayList<>();
        if (fila.moveToFirst()) {
            do{
                data.add(new CargaDescarga(fila.getString(4),fila.getString(5), fila.getString(6)));
            }while(fila.moveToNext());
        }
        db.close();
        return data;
    }

    public boolean spSel_DescargaBase() {

        //Se crea el objeto admin que nos permite obtener la base de datos creada en la clase AdminSQLiteOpenHelper
        SQLOpenHelper admin = new SQLOpenHelper(context);
        //Se crea el objeto bd que nos permite abrir la base de datos
        SQLiteDatabase db = admin.getWritableDatabase();
        Cursor fila = db.rawQuery("SELECT * FROM rfid INNER JOIN item ON rfid.id_sku = item.idSku", null);

        boolean status = false;
        if (fila.moveToFirst()) {
            do{
                spIns_auxDescarga(fila.getString(4), fila.getString(5), fila.getString(1));
                status = true;
            }while(fila.moveToNext());
        }
        db.close();
        return status;
    }

    public long spIns_auxDescarga(String sku, String desc, String rfid){
        //Se crea el objeto admin que nos permite obtener la base de datos creada en la clase AdminSQLiteOpenHelper
        SQLOpenHelper admin = new SQLOpenHelper(context);
        //Se crea el objeto bd que nos permite abrir la base de datos
        SQLiteDatabase db = admin.getWritableDatabase();
        //Se crea el objeto registro para guardar los datos de cada editor de texto
        ContentValues reg = new ContentValues();
        //se guardan los datos de cada editor de texto en el objeto registro
        reg.put("sku", sku);
        reg.put("descripcion", desc);
        reg.put("rfid", rfid);

        //se insertan los registros en la base de datos
        long resp = db.insert("auxDescarga", null, reg);
        //se cierra la base de datos
        db.close();

        return resp;
    }

    public ArrayList spSel_EnlacesCargados() {

        //Se crea el objeto admin que nos permite obtener la base de datos creada en la clase AdminSQLiteOpenHelper
        SQLOpenHelper admin = new SQLOpenHelper(context);
        //Se crea el objeto bd que nos permite abrir la base de datos
        SQLiteDatabase db = admin.getWritableDatabase();
        Cursor fila = db.rawQuery("SELECT *, COUNT(*) as cantidad FROM auxDescarga GROUP BY sku", null);

        ArrayList<CargaDescarga> data = new ArrayList<>();
        if (fila.moveToFirst()) {
            do{
                data.add(new CargaDescarga(fila.getString(0), fila.getString(1), fila.getString(3)));
            }while(fila.moveToNext());
        }
        db.close();
        return data;
    }

    public ArrayList spSel_AuxDescarga() {

        //Se crea el objeto admin que nos permite obtener la base de datos creada en la clase AdminSQLiteOpenHelper
        SQLOpenHelper admin = new SQLOpenHelper(context);
        //Se crea el objeto bd que nos permite abrir la base de datos
        SQLiteDatabase db = admin.getWritableDatabase();
        Cursor fila = db.rawQuery("SELECT * FROM auxDescarga", null);

        ArrayList<CargaDescarga> data = new ArrayList<>();
        if (fila.moveToFirst()) {
            do{
                data.add(new CargaDescarga(fila.getString(0), fila.getString(1), fila.getString(2)));
            }while(fila.moveToNext());
        }
        db.close();
        return data;
    }




    /**** LIMPIAR LAS TABLAS ****/
    public void clearauxDescarga(){
        //Se crea el objeto admin que nos permite obtener la base de datos creada en la clase AdminSQLiteOpenHelper
        SQLOpenHelper admin = new SQLOpenHelper(context);
        //Se crea el objeto bd que nos permite abrir la base de datos
        SQLiteDatabase db = admin.getWritableDatabase();
        db.execSQL("DELETE FROM auxDescarga;");
        db.execSQL("VACUUM;");
        db.close();
    }

    public void clearInventario(){
        //Se crea el objeto admin que nos permite obtener la base de datos creada en la clase AdminSQLiteOpenHelper
        SQLOpenHelper admin = new SQLOpenHelper(context);
        //Se crea el objeto bd que nos permite abrir la base de datos
        SQLiteDatabase db = admin.getWritableDatabase();
        db.execSQL("DELETE FROM inventario;");
        db.execSQL("VACUUM;");
        db.close();
    }

    public void clearResumenInventario(){
        //Se crea el objeto admin que nos permite obtener la base de datos creada en la clase AdminSQLiteOpenHelper
        SQLOpenHelper admin = new SQLOpenHelper(context);
        //Se crea el objeto bd que nos permite abrir la base de datos
        SQLiteDatabase db = admin.getWritableDatabase();
        db.execSQL("DELETE FROM resumenInventario;");
        db.execSQL("VACUUM;");
        db.close();
    }

    public void clearItem(){
        //Se crea el objeto admin que nos permite obtener la base de datos creada en la clase AdminSQLiteOpenHelper
        SQLOpenHelper admin = new SQLOpenHelper(context);
        //Se crea el objeto bd que nos permite abrir la base de datos
        SQLiteDatabase db = admin.getWritableDatabase();
        db.execSQL("DELETE FROM item;");
        db.execSQL("VACUUM;");
        db.close();
    }

    public void clearRfid(){
        //Se crea el objeto admin que nos permite obtener la base de datos creada en la clase AdminSQLiteOpenHelper
        SQLOpenHelper admin = new SQLOpenHelper(context);
        //Se crea el objeto bd que nos permite abrir la base de datos
        SQLiteDatabase db = admin.getWritableDatabase();
        db.execSQL("DELETE FROM rfid;");
        db.execSQL("VACUUM;");
        db.close();
    }

}
