package glgroup.centuryinventory.Services.SQLite;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class SQLOpenHelper extends SQLiteOpenHelper {

    public static final String DATABASE_NAME  = "CenturyInventory";
    public static final int DATABASE_VERSION  = 1;

    public SQLOpenHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    public SQLOpenHelper(Context context, int version) {
        super(context, DATABASE_NAME, null, version);
    }


    @Override public void onOpen(SQLiteDatabase db) {
        onCreate(db);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE IF NOT EXISTS item(idSku INTEGER PRIMARY KEY AUTOINCREMENT, sku TEXT, descripcion TEXT)");
        db.execSQL("CREATE TABLE IF NOT EXISTS rfid(idRfid INTEGER PRIMARY KEY AUTOINCREMENT, codigo TEXT, id_sku INTEGER, FOREIGN KEY (id_sku) REFERENCES item (idSku))");
      //  db.execSQL("CREATE TABLE IF NOT EXISTS enlace(id INTEGER PRIMARY KEY AUTOINCREMENT, id_sku INTEGER, id_rfid INTEGER, FOREIGN KEY (id_sku) REFERENCES item (idSku), FOREIGN KEY (id_rfid) REFERENCES rfid (idRfid))");
        db.execSQL("CREATE TABLE IF NOT EXISTS inventario(sku TEXT, descripcion TEXT)");
        db.execSQL("CREATE TABLE IF NOT EXISTS resumenInventario(sku TEXT, descripcion TEXT, cantidad TEXT)");
        db.execSQL("CREATE TABLE IF NOT EXISTS correo(descripcion TEXT, correo TEXT)");

        db.execSQL("CREATE TABLE IF NOT EXISTS auxDescarga(sku TEXT, descripcion TEXT, rfid TEXT)");
    }


    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS item");
        db.execSQL("DROP TABLE IF EXISTS rfid");
        db.execSQL("DROP TABLE IF EXISTS inventario");
        db.execSQL("DROP TABLE IF EXISTS resumenInventario");
        db.execSQL("DROP TABLE IF EXISTS correo");
        db.execSQL("DROP TABLE IF EXISTS auxDescarga");
    }
}
