package glgroup.centuryinventory.Services.Retrofit.Modelos;

import com.google.gson.annotations.SerializedName;

public class Resultado {

    @SerializedName("id_respuesta")
    private int idRespuesta;

    @SerializedName("respuesta")
    private String respuesta;

    @SerializedName("fecha")
    private String fecha;

    public Resultado(int idRespuesta, String respuesta, String fecha) {
        this.idRespuesta = idRespuesta;
        this.respuesta = respuesta;
        this.fecha = fecha;
    }

    public int getIdRespuesta() {
        return idRespuesta;
    }

    public void setIdRespuesta(int idRespuesta) {
        this.idRespuesta = idRespuesta;
    }

    public String getRespuesta() {
        return respuesta;
    }

    public void setRespuesta(String respuesta) {
        this.respuesta = respuesta;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }
}
