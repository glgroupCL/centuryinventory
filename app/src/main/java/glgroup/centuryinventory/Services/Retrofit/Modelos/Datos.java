package glgroup.centuryinventory.Services.Retrofit.Modelos;

import com.google.gson.JsonObject;
import com.google.gson.annotations.SerializedName;

public class Datos {

    @SerializedName("datos")
    private DatosContent datos;

    public DatosContent getDatos() {
        return datos;
    }

    public void setDatos(DatosContent datos) {
        this.datos = datos;
    }
}
