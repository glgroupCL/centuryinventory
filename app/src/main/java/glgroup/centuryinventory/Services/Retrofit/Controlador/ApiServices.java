package glgroup.centuryinventory.Services.Retrofit.Controlador;

import glgroup.centuryinventory.Services.Retrofit.Modelos.Datos;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface ApiServices {

    @FormUrlEncoded
    @POST("/ActivarProducto")
    Call<Datos> getActivacion(
            @Field("id_marca") int idMarca,
            @Field("id_idioma") int idIdioma,
            @Field("imei_mac") String imeiMac,
            @Field("id_software") int idSoftware,
            @Field("clave_activacion") String claveActivacion
    );


}
