package glgroup.centuryinventory.Services.Retrofit.Modelos;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class DatosContent {

    @SerializedName("exito")
    private int exito;

    @SerializedName("mensaje")
    private String mensaje;

    @SerializedName("resultado")
    private List<Resultado> resultado;

    public int getExito() {
        return exito;
    }

    public void setExito(int exito) {
        this.exito = exito;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    public List<Resultado> getResultado() {
        return resultado;
    }

    public void setResultado(List<Resultado> resultado) {
        this.resultado = resultado;
    }
}
