package glgroup.centuryinventory.Services.Adaptador;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import glgroup.centuryinventory.R;
import glgroup.centuryinventory.Services.Model.Correo;

public class AdaptadorCorreo extends RecyclerView.Adapter<AdaptadorCorreo.exViewHolder>{

    private ArrayList<Correo> mExampleList;
    private OnItemClickListener mListener;

    public interface OnItemClickListener {
        void onItemClick(int position);

        void onDeleteClick(int position);

        void onEditClick(int position);
    }

    public void setOnItemClickListener(OnItemClickListener listener) {
        mListener = listener;
    }

    public static class exViewHolder extends RecyclerView.ViewHolder {
        public TextView descripcion;
        public TextView correo;
        public ImageView mDeleteImage;
        public ImageView mEditImage;

        public exViewHolder(View itemView, final OnItemClickListener listener) {
            super(itemView);

            descripcion = itemView.findViewById(R.id.descipcion);
            correo = itemView.findViewById(R.id.correo);
            mDeleteImage = itemView.findViewById(R.id.image_delete);
            mEditImage = itemView.findViewById(R.id.image_edit);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listener != null) {
                        int position = getAdapterPosition();
                        if (position != RecyclerView.NO_POSITION) {
                            listener.onItemClick(position);
                        }
                    }
                }
            });

            mDeleteImage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listener != null) {
                        int position = getAdapterPosition();
                        if (position != RecyclerView.NO_POSITION) {
                            listener.onDeleteClick(position);
                        }
                    }
                }
            });

            mEditImage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (listener != null) {
                        int position = getAdapterPosition();
                        if (position != RecyclerView.NO_POSITION) {
                            listener.onEditClick(position);
                        }
                    }
                }
            });
        }
    }

    public AdaptadorCorreo(ArrayList<Correo> exampleList) {
        mExampleList = exampleList;
    }

    @Override
    public exViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_correo, parent, false);
        exViewHolder evh = new exViewHolder(v, mListener);
        return evh;
    }

    @Override
    public void onBindViewHolder(exViewHolder holder, int position) {
        Correo currentItem = mExampleList.get(position);

        holder.descripcion.setText(currentItem.getDescripcion());
        holder.correo.setText(currentItem.getCorreo());
    }

    @Override
    public int getItemCount() {
        return mExampleList.size();
    }
}
