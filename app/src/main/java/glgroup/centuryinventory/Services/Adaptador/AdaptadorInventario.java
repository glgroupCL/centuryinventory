package glgroup.centuryinventory.Services.Adaptador;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import glgroup.centuryinventory.R;
import glgroup.centuryinventory.Services.Model.Inventario;

public class AdaptadorInventario extends BaseAdapter {

    private Context context;
    private ArrayList<Inventario> listItems;

    public AdaptadorInventario(Context context, ArrayList<Inventario> listItems) {
        this.context = context;
        this.listItems = listItems;
    }

    @Override
    public int getCount() {
        return listItems.size();
    }

    @Override
    public Object getItem(int i) {
        return listItems.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }


    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {

        //se obtiene el valor de la lista en la posición i, y si transorma a un elemento de la clase inventario
        Inventario item = (Inventario) getItem(i);

        view = LayoutInflater.from(context).inflate(R.layout.vista_inventario, null);

        //se obtienen los elementos del layout
        TextView dato = view.findViewById(R.id.dato);
        TextView valor = view.findViewById(R.id.valor);

        dato.setText(item.getDescripcion());
        valor.setText(item.getCantidad());


        return view;
    }
}
