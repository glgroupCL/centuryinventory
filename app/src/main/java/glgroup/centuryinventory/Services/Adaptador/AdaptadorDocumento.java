package glgroup.centuryinventory.Services.Adaptador;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import glgroup.centuryinventory.R;
import glgroup.centuryinventory.Services.Model.Documento;

public class AdaptadorDocumento extends BaseAdapter {

    private Context context;
    private ArrayList<Documento> listItems;

    public AdaptadorDocumento(Context context, ArrayList<Documento> listItems) {
        this.context = context;
        this.listItems = listItems;
    }
    @Override
    public int getCount() {
        return listItems.size();
    }

    @Override
    public Object getItem(int i) {
        return listItems.get(i);
    }


    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        //se obtiene el valor de la lista en la posición i, y si transorma a un elemento de la clase inventario
        Documento item = (Documento) getItem(i);

        view = LayoutInflater.from(context).inflate(R.layout.vista_documento, null);

        //se obtienen los elementos del layout
        TextView sku = view.findViewById(R.id.sku);
        TextView desc = view.findViewById(R.id.descripcion);

        sku.setText(item.getSku());
        desc.setText(item.getDescripcion());


        return view;
    }
}
