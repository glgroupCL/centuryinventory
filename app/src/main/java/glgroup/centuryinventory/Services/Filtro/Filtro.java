package glgroup.centuryinventory.Services.Filtro;

import java.util.ArrayList;
import java.util.Map;

public class Filtro {

    private ArrayList<String> etiquetas = new ArrayList<String>();
    private String etiquetas2;

    //metodo que toma las etiquetas leidas y filtra los campos para evitar datos innecesarios.
    public String Filtro(Map<String, String> tagList) {
        etiquetas.clear();
        etiquetas2 = "";
        try{
            for(String key : tagList.keySet()) {
                etiquetas.add(key.trim());
            }
            String aux;
            String aux2 = String.valueOf(etiquetas);
            String aux3 =aux2.replace("[","");
            aux = aux3.replace("]","");
            etiquetas2 = aux.replace(" ","");

        }catch(Exception e){
            etiquetas2 = String.valueOf(e);
        }
        return etiquetas2;
    }
}
