package glgroup.centuryinventory.Services.Sound;

import android.content.Context;
import android.media.AudioAttributes;
import android.media.AudioManager;
import android.media.SoundPool;
import android.os.Build;
import android.util.Log;

import glgroup.centuryinventory.R;


public class Sonido {

    //Variable Declarations - I have a subclass for all my Activities where these are declared.
    public SoundPool soundPool;
    public AudioManager audioManager;
    // Maximumn sound stream.
    public static final int MAX_STREAMS = 1;
    public float volume;

    //Sound files
    public int soundIdBeep;

    boolean loaded;


    Context context;

    public Sonido() {

    }

    public Sonido(AudioManager audioManager, Context context) {
        this.audioManager = audioManager;
        this.context = context;
    }

    // Stream type.
    public static final int streamType = AudioManager.STREAM_MUSIC;

    //I call this function in OnCreate
    //Function for sound
    public void setupSound()
    {
        //Sound studd!!!!!!!!!!!!!
        // AudioManager audio settings for adjusting the volume

        // Volumn (0 --> 1)
        //this.volume = currentVolumeIndex / maxVolumeIndex;
        this.volume = 0.5f;

        // For Android SDK >= 21
        if (Build.VERSION.SDK_INT >= 21 ) {

            AudioAttributes audioAttrib = new AudioAttributes.Builder()
                    .setUsage(AudioAttributes.USAGE_GAME)
                    .setContentType(AudioAttributes.CONTENT_TYPE_SONIFICATION)
                    .build();

            SoundPool.Builder builder= new SoundPool.Builder();
            builder.setAudioAttributes(audioAttrib).setMaxStreams(MAX_STREAMS);

            this.soundPool = builder.build();
        }
        // for Android SDK < 21
        else {
            // SoundPool(int maxStreams, int streamType, int srcQuality)
            this.soundPool = new SoundPool(MAX_STREAMS, AudioManager.STREAM_MUSIC, 0);
        }

        // When Sound Pool load complete.
        this.soundPool.setOnLoadCompleteListener(new SoundPool.OnLoadCompleteListener() {
            @Override
            public void onLoadComplete(SoundPool soundPool, int sampleId, int status) {
                loaded = true;
            }
        });

        // Load sound file (destroy.wav) into SoundPool.
        this.soundIdBeep = this.soundPool.load(context, R.raw.beep,1);

    }

    //Function to actually play sound, this is also in my Activity subclass
    public void playBeep()
    {
        try
        {
            soundPool.play(soundIdBeep,volume, volume, 1, 0, 1f);
        }
        catch (Exception ex)
        {
            Log.i("Error", ex.getMessage());
        }
    }
}
