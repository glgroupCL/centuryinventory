package glgroup.centuryinventory.Services.Utilidades;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.widget.Toast;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

public class TaskRunner {

    public static class TaskChk extends AsyncTask<String, Integer, Boolean> {
        Context context;
        String failMsg;
        CountDownLatch latch;
        String msg;
        ProgressDialog mypDialog;

        TaskChk(Context context2, String msg2, String failMsg2, CountDownLatch latch2) {
            this.msg = msg2;
            this.failMsg = failMsg2;
            this.context = context2;
            this.latch = latch2;
        }

        /* access modifiers changed from: protected */
        public Boolean doInBackground(String... params) {
            for (int i = 0; i < 100 && this.latch.getCount() != 0; i++) {
                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                }
            }
            return Boolean.valueOf(this.latch.getCount() == 0);
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(Boolean result) {
            super.onPostExecute(result);
            this.mypDialog.cancel();
            if (!result.booleanValue() && this.failMsg != null) {
                Toast.makeText(this.context, this.failMsg, 0).show();
            }
        }

        /* access modifiers changed from: protected */
        public void onPreExecute() {
            super.onPreExecute();
            this.mypDialog = new ProgressDialog(this.context);
            this.mypDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            this.mypDialog.setMessage("Leyendo Etiqueta");
            this.mypDialog.setCanceledOnTouchOutside(false);
            this.mypDialog.show();
        }
    }

    public static void runTask(Context context, String msg, Runnable runnable) {
        runTask(context, msg, null, runnable);
    }

    public static void runTask(Context context, String msg, String failMsg, final Runnable runnable) {
        final CountDownLatch latchStop = new CountDownLatch(1);
        new Thread(new Runnable() {
            public void run() {
                runnable.run();
                latchStop.countDown();
            }
        }).start();
        try {
            if (!latchStop.await(1000, TimeUnit.MILLISECONDS)) {
                new TaskChk(context, msg, failMsg, latchStop).execute(new String[0]);
            }
        } catch (InterruptedException e) {
        }
    }
}
