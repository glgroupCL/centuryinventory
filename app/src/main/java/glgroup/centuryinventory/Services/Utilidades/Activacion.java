package glgroup.centuryinventory.Services.Utilidades;

import android.app.Activity;
import android.content.Context;
import android.widget.TextView;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import glgroup.centuryinventory.R;

public class Activacion {

    public static int fechaValidez(TextView tvFecha, String fecha, Activity activity) {

        if(!fecha.equals("")) {
            Date fechaActual = new Date();
            DateFormat formatoFecha = new SimpleDateFormat("dd-MM-yyyy");

            String dateActual = formatoFecha.format(fechaActual);

            Date fechaInicial = null;
            Date fechaFinal = null;
            try {
                fechaInicial = formatoFecha.parse(dateActual);
                fechaFinal = formatoFecha.parse(fecha);
            } catch (ParseException e) {
                e.printStackTrace();
            }

            int dias = (int) ((fechaFinal.getTime() - fechaInicial.getTime()) / 86400000);
            if(dias <= 0 ){
                tvFecha.setText(activity.getString(R.string.licencia_expirada));
                return 0;
            }else{
                if(dias == 1){
                    tvFecha.setText(activity.getString(R.string.licencia_expira_en)+" " + dias + " "+activity.getString(R.string.dia));
                }else {
                    tvFecha.setText(activity.getString(R.string.licencia_expira_en)+" " + dias + " "+activity.getString(R.string.dias));
                }
                return 1;
            }
        }else{
            return 2;
        }
    }
}
