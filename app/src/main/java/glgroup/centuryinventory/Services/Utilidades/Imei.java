package glgroup.centuryinventory.Services.Utilidades;

import android.Manifest;
import android.app.Activity;
import android.content.pm.PackageManager;
import android.telephony.TelephonyManager;

import androidx.core.app.ActivityCompat;

import static android.content.Context.TELEPHONY_SERVICE;

public class Imei {

    //obtener el imei del equipo
    public static String getImei(Activity activity){
        String imei = "";
        TelephonyManager device = (TelephonyManager) activity.getSystemService(TELEPHONY_SERVICE);

        if (ActivityCompat.checkSelfPermission(activity, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
            return imei;
        }
        imei = device.getDeviceId();
        return imei;
    }
}
