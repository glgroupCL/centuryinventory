package glgroup.centuryinventory.Services.Utilidades;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.text.SpannableString;
import android.text.style.StyleSpan;
import android.util.Log;
import android.view.Display;
import android.view.WindowManager;
import android.widget.Toast;

import androidx.cardview.widget.CardView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.ContextCompat;

import com.getkeepsafe.taptargetview.TapTarget;
import com.getkeepsafe.taptargetview.TapTargetSequence;
import com.getkeepsafe.taptargetview.TapTargetView;

import es.dmoral.toasty.Toasty;
import glgroup.centuryinventory.Layout.MainActivity;
import glgroup.centuryinventory.R;
import glgroup.centuryinventory.Services.Memory.Local;

public class AyudaInicial {

    public void inicio(WindowManager windowManager, final Activity activity, CardView cCarga, CardView cEnlace, CardView cDesenlace, CardView cInventario, CardView cConfig){

        // We load a drawable and create a location to show a tap target here
        // We need the display to get the width and height at this point in time
        final Display display = windowManager.getDefaultDisplay();
        // Load our little droid guy
        final Drawable droid = ContextCompat.getDrawable(activity, R.drawable.ic_alert);
        // Tell our droid buddy where we want him to appear
        final Rect droidTarget = new Rect(0, 0, droid.getIntrinsicWidth() * 2, droid.getIntrinsicHeight() * 2);
        // Using deprecated methods makes you look way cool
        droidTarget.offset(display.getWidth() / 2, display.getHeight() / 2);

        final SpannableString sassyDesc = new SpannableString(activity.getString(R.string.puedes_enlazar_con_rfid));
        sassyDesc.setSpan(new StyleSpan(Typeface.ITALIC), sassyDesc.length() - "sometimes".length(), sassyDesc.length(), 0);

        // We have a sequence of targets, so lets build it!
        final TapTargetSequence sequence = new TapTargetSequence(activity)
                .targets(
                        TapTarget.forView(cCarga, activity.getString(R.string.bienvenido_century), activity.getString(R.string.para_comenzar_carga_maestra))
                                .dimColor(android.R.color.black)
                                .outerCircleColor(R.color.colorAlert)
                                .targetCircleColor(android.R.color.black)
                                .transparentTarget(true)
                                .textColor(android.R.color.white)
                                .id(1),
                        // This tap target will target the back button, we just need to pass its containing toolbar
                        TapTarget.forView(cEnlace, activity.getString(R.string.enlace), activity.getString(R.string.puedes_enlazar_con_rfid)).id(2)
                                .dimColor(android.R.color.black)
                                .outerCircleColor(R.color.colorAlert)
                                .targetCircleColor(android.R.color.black)
                                .transparentTarget(true)
                                .textColor(android.R.color.white),
                        // Likewise, this tap target will target the search button
                        TapTarget.forView(cDesenlace, activity.getString(R.string.desenlace), activity.getString(R.string.permite_desenlace))
                                .dimColor(android.R.color.black)
                                .outerCircleColor(R.color.colorAlert)
                                .targetCircleColor(android.R.color.black)
                                .transparentTarget(true)
                                .textColor(android.R.color.white)
                                .id(3),
                        // You can also t3arget the overflow button in your toolbar
                        TapTarget.forView(cInventario, activity.getString(R.string.inventario), activity.getString(R.string.ayuda_inventario))
                                .dimColor(android.R.color.black)
                                .outerCircleColor(R.color.colorAlert)
                                .targetCircleColor(android.R.color.black)
                                .transparentTarget(true)
                                .textColor(android.R.color.white)
                                .id(4),
                        TapTarget.forView(cConfig, activity.getString(R.string.configuracion), activity.getString(R.string.ayuda_configuracion))
                                .dimColor(android.R.color.black)
                                .outerCircleColor(R.color.colorAlert)
                                .targetCircleColor(android.R.color.black)
                                .transparentTarget(true)
                                .textColor(android.R.color.white)
                                .id(5)
                )
                .listener(new TapTargetSequence.Listener() {
                    // This listener will tell us when interesting(tm) events happen in regards
                    // to the sequence
                    @Override
                    public void onSequenceFinish() {
                        Local.setData("inicio", activity, "ayuda", "1");
                        Toasty.success(activity, activity.getString(R.string.listo_comenzar), Toast.LENGTH_LONG, true).show();
                    }

                    @Override
                    public void onSequenceStep(TapTarget lastTarget, boolean targetClicked) {
                        Local.setData("inicio", activity, "ayuda", "1");
                        Log.d("TapTargetView", "Clicked on " + lastTarget.id());
                    }

                    @Override
                    public void onSequenceCanceled(TapTarget lastTarget) {
                        Local.setData("inicio", activity, "ayuda", "1");
//                        final AlertDialog dialog = new AlertDialog.Builder(activity)
//                                .setTitle("Uh oh")
//                                .setMessage("You canceled the sequence")
//                                .setPositiveButton("Oops", null).show();
//                        TapTargetView.showFor(dialog,
//                                TapTarget.forView(dialog.getButton(DialogInterface.BUTTON_POSITIVE), "Uh oh!", "You canceled the sequence at step " + lastTarget.id())
//                                        .cancelable(false)
//                                        .tintTarget(false), new TapTargetView.Listener() {
//                                    @Override
//                                    public void onTargetClick(TapTargetView view) {
//                                        super.onTargetClick(view);
//                                        dialog.dismiss();
//                                    }
//                                });
                    }
                });
        sequence.start();
    }
}
