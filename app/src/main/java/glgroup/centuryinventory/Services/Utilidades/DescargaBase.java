package glgroup.centuryinventory.Services.Utilidades;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Environment;
import android.view.View;
import android.widget.Toast;

import androidx.core.app.ActivityCompat;

//import com.ajts.androidmads.library.SQLiteToExcel;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.util.WorkbookUtil;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import es.dmoral.toasty.Toasty;
import glgroup.centuryinventory.Layout.CargaItem.CargaBaseRfidActivity;
import glgroup.centuryinventory.Layout.MainActivity;
import glgroup.centuryinventory.R;
import glgroup.centuryinventory.Services.Adaptador.AdaptadorCargaDescarga;
import glgroup.centuryinventory.Services.Alert.Alerta;
import glgroup.centuryinventory.Services.Memory.Local;
import glgroup.centuryinventory.Services.Model.CargaDescarga;
import glgroup.centuryinventory.Services.SQLite.SQLOpenHelper;
import glgroup.centuryinventory.Services.SQLite.SQLiteQuery;

public class DescargaBase {

    Activity activity;
    String nombre;
    String path;

    public void exportarBase(final Context context, String nombre, final Activity activity) {

        SQLiteQuery sqLiteQuery = new SQLiteQuery(context);
        sqLiteQuery.clearauxDescarga();
        sqLiteQuery.spSel_DescargaBase();

        this.activity = activity;
        final String directory_path = Environment.getExternalStorageDirectory()+ "/ExcelDescargaBase/";
        File file = new File(Environment.getExternalStorageDirectory(), "ExcelDescargaBase");
        if(isStoragePermissionGranted(activity)){
            if (!file.exists()) {
                file.mkdir();
            }
        }else{
            Toast.makeText(context, "error",Toast.LENGTH_SHORT).show();
        }

        this.nombre = nombre;
        this.path = directory_path;

        new procesarLectura().execute();

  //      onWriteClick(nombre, directory_path, activity);
        // Export SQLite DB as EXCEL FILE
//        SQLiteToExcel sqliteToExcel = new SQLiteToExcel(context, adminSQLOpenHelper.DATABASE_NAME, directory_path);
//        sqliteToExcel.exportSpecificTables(table1List, nombreArchivo , new SQLiteToExcel.ExportListener() {
//            @Override
//            public void onStart() {
//            }
//
//            @Override
//            public void onCompleted(String filePath) {
//                Toasty.success(context, "Exportado correctamente en: "+directory_path, Toast.LENGTH_SHORT).show();
//                activity.startActivity(new Intent(activity, MainActivity.class));
//            }
//
//            @Override
//            public void onError(Exception e) {
//                Toasty.error(context, "Error al exportar la información", Toast.LENGTH_SHORT, true).show();
//            }
//        });
    }
    //metodo para inicializar el lector
    public class procesarLectura extends AsyncTask<String, Integer, Boolean> {
        ProgressDialog dialogAlmacena;
        @Override
        protected Boolean doInBackground(String... params) {

            boolean retorno;

            //    printlnToUser("writing xlsx file");
            XSSFWorkbook workbook = new XSSFWorkbook();
            XSSFSheet sheet = workbook.createSheet(WorkbookUtil.createSafeSheetName("BaseRFID"));

            Row row = sheet.createRow(0);
            Cell cell = row.createCell(0);
            Cell cell2 = row.createCell(1);
            Cell cell3 = row.createCell(2);
            cell.setCellValue("RFID");
            cell2.setCellValue("SKU");
            cell3.setCellValue("DESCRIPCION");

            SQLiteQuery sqLiteQuery = new SQLiteQuery(activity.getApplicationContext());
            ArrayList<CargaDescarga> data = sqLiteQuery.spSel_AuxDescarga();

            for (int i=1; i<= data.size();i++) {
                row = sheet.createRow(i);
                cell = row.createCell(0);
                cell2 = row.createCell(1);
                cell3 = row.createCell(2);

                cell.setCellValue(data.get(i-1).getCantidad());
                cell2.setCellValue(data.get(i-1).getSku());
                cell3.setCellValue(data.get(i-1).getDescripcion());
            }
            String outFileName = nombre+".xlsx";
            try {
                //       printlnToUser("writing file " + outFileName);
                //   File cacheDir = getCacheDir();
                File outFile = new File(path, outFileName);
                OutputStream outputStream = new FileOutputStream(outFile.getAbsolutePath());
                workbook.write(outputStream);
                outputStream.flush();
                outputStream.close();
               retorno = true;

            } catch (Exception e) {
                retorno = false;
                System.out.println(e.toString());


            }


            return retorno;
        }
        @Override
        protected void onPostExecute(Boolean result) {
            super.onPostExecute(result);
            dialogAlmacena.dismiss();

            if(result){
                Toasty.success(activity.getApplicationContext(), activity.getString(R.string.exportado_correctamente)+": "+path, Toast.LENGTH_SHORT).show();
                activity.startActivity(new Intent(activity, MainActivity.class));
            }else{
                Toasty.error(activity.getApplicationContext(), activity.getString(R.string.error_al_exportar), Toast.LENGTH_SHORT).show();
            }



        }
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialogAlmacena = new ProgressDialog(activity);
            dialogAlmacena.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            dialogAlmacena.setMessage(activity.getString(R.string.procesando_documento));
            dialogAlmacena.setCanceledOnTouchOutside(false);
            dialogAlmacena.show();
        }
    }




    public void onWriteClick(String nombre, String ruta, Activity activity) {
        //    printlnToUser("writing xlsx file");
        XSSFWorkbook workbook = new XSSFWorkbook();
        XSSFSheet sheet = workbook.createSheet(WorkbookUtil.createSafeSheetName("BaseRFID"));

        Row row = sheet.createRow(0);
        Cell cell = row.createCell(0);
        Cell cell2 = row.createCell(1);
        Cell cell3 = row.createCell(2);
        cell.setCellValue("RFID");
        cell2.setCellValue("SKU");
        cell3.setCellValue("DESCRIPCION");

        SQLiteQuery sqLiteQuery = new SQLiteQuery(activity.getApplicationContext());
        ArrayList<CargaDescarga> data = sqLiteQuery.spSel_AuxDescarga();

        for (int i=1; i<= data.size();i++) {
            row = sheet.createRow(i);
            cell = row.createCell(0);
            cell2 = row.createCell(1);
            cell3 = row.createCell(2);

            cell.setCellValue(data.get(i-1).getCantidad());
            cell2.setCellValue(data.get(i-1).getSku());
            cell3.setCellValue(data.get(i-1).getDescripcion());
        }
        String outFileName = nombre+".xlsx";
        try {
            //       printlnToUser("writing file " + outFileName);
         //   File cacheDir = getCacheDir();
            File outFile = new File(ruta, outFileName);
            OutputStream outputStream = new FileOutputStream(outFile.getAbsolutePath());
            workbook.write(outputStream);
            outputStream.flush();
            outputStream.close();
            Toasty.success(activity.getApplicationContext(), activity.getString(R.string.exportado_correctamente)+": "+ruta, Toast.LENGTH_SHORT).show();
            activity.startActivity(new Intent(activity, MainActivity.class));

        } catch (Exception e) {
            System.out.println(e.toString());
            Toasty.error(activity.getApplicationContext(), activity.getString(R.string.error_al_exportar), Toast.LENGTH_SHORT).show();

        }
    }

    public  boolean isStoragePermissionGranted(Activity activity) {
        if (Build.VERSION.SDK_INT >= 23) {
            if (activity.checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_GRANTED) {
                return true;
            } else {
                ActivityCompat.requestPermissions(activity,
                        new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
                return false;
            }
        }
        else { //permission is automatically granted on sdk<23 upon installation
            return true;
        }
    }
}
