package glgroup.centuryinventory.Services.Model;

public class Correo {

    private String descripcion;
    private String correo;

    public Correo(String descripcion, String correo) {
        this.descripcion = descripcion;
        this.correo = correo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }
}
