package glgroup.centuryinventory.Services.Model;

public class Inventario {

    private String sku;
    private String descripcion;
    private String cantidad;

    public Inventario(String descripcion, String cantidad) {
        this.descripcion = descripcion;
        this.cantidad = cantidad;
    }


    public Inventario(String sku, String descripcion, String cantidad) {
        this.sku = sku;
        this.descripcion = descripcion;
        this.cantidad = cantidad;
    }

    public String getSku() {
        return sku;
    }

    public void setSku(String sku) {
        this.sku = sku;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getCantidad() {
        return cantidad;
    }

    public void setCantidad(String cantidad) {
        this.cantidad = cantidad;
    }
}
