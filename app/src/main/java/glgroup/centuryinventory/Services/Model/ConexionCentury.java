package glgroup.centuryinventory.Services.Model;

public class ConexionCentury {

    String urlActualizacion;
    String Conexion;
    String puerto;
    String version;
    String imeiRestringido;

    public ConexionCentury() {
    }

    public ConexionCentury(String urlActualizacion, String conexion, String puerto, String version, String imeiRestringido) {
        this.urlActualizacion = urlActualizacion;
        Conexion = conexion;
        this.puerto = puerto;
        this.version = version;
        this.imeiRestringido = imeiRestringido;
    }

    public String getUrlActualizacion() {
        return urlActualizacion;
    }

    public void setUrlActualizacion(String urlActualizacion) {
        this.urlActualizacion = urlActualizacion;
    }

    public String getConexion() {
        return Conexion;
    }

    public void setConexion(String conexion) {
        Conexion = conexion;
    }

    public String getPuerto() {
        return puerto;
    }

    public void setPuerto(String puerto) {
        this.puerto = puerto;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getImeiRestringido() {
        return imeiRestringido;
    }

    public void setImeiRestringido(String imeiRestringido) {
        this.imeiRestringido = imeiRestringido;
    }
}
