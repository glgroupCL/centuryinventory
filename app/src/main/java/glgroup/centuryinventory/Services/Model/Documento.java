package glgroup.centuryinventory.Services.Model;

public class Documento {

    private String sku;
    private String descripcion;

    public Documento(String sku, String descripcion) {
        this.sku = sku;
        this.descripcion = descripcion;
    }

    public String getSku() {
        return sku;
    }

    public void setSku(String sku) {
        this.sku = sku;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
}
