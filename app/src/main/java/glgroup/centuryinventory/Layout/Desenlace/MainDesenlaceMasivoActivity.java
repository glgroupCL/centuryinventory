package glgroup.centuryinventory.Layout.Desenlace;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.NavUtils;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.media.AudioManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.rscja.deviceapi.RFIDWithUHF;
import com.rscja.deviceapi.exception.ConfigurationException;

import java.util.HashMap;
import java.util.Map;

import es.dmoral.toasty.Toasty;
import glgroup.centuryinventory.R;
import glgroup.centuryinventory.Services.Alert.Alerta;
import glgroup.centuryinventory.Services.Filtro.Filtro;
import glgroup.centuryinventory.Services.SQLite.SQLiteQuery;
import glgroup.centuryinventory.Services.Sound.Sonido;

public class MainDesenlaceMasivoActivity extends AppCompatActivity implements View.OnClickListener{

    //botones
    private Button leer, procesar;

    // boton para limpiar datos
    private ImageButton limpiar;

    //textos de la vista
    private TextView contador, lectura;

    //variable para el reader
    private RFIDWithUHF reader;
    boolean lectorRun;

    //Sonido
    Sonido sound;

    //Lista para elmacenar etiquetas
    private Map<String, String> etiquetas = new HashMap<String, String>();

    //SQLite
    SQLiteQuery sqLiteQuery;

    int desenlaceError = 0, desenlaceCorrecto = 0;

    //flecha atras
    private ImageButton atras;

    private Boolean looper = false;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_desenlace_masivo);
        if(getSupportActionBar() != null){
            getSupportActionBar().hide();
        }

        sqLiteQuery = new SQLiteQuery(getApplicationContext());

        sound = new Sonido((AudioManager) getSystemService(AUDIO_SERVICE), MainDesenlaceMasivoActivity.this);
        sound.setupSound();

        new InitTask().execute();
        init();
    }

    public void init(){

        limpiar = findViewById(R.id.btnLimpiar);
        limpiar.setOnClickListener(this);

        procesar = findViewById(R.id.btnProcesar);
        procesar.setOnClickListener(this);

        contador = findViewById(R.id.contador);
        lectura = findViewById(R.id.msgLectura);

        atras = findViewById(R.id.atras);
        atras.setOnClickListener(this);

        leer  = findViewById(R.id.btnLeer);
        leer.setOnClickListener(this);

    }

    //metodo para inicializar el lector
    public class InitTask extends AsyncTask<String, Integer, Boolean> {
        ProgressDialog dialogInventario;
        @Override
        protected Boolean doInBackground(String... params) {
            try {
                // initialize RFID interface and obtain a global RFID Reader instance
                reader = RFIDWithUHF.getInstance();
                reader.init();
                reader.setPower(10);
                lectorRun = true;
            } catch(ConfigurationException e) {
                lectorRun = false;
            }
            return lectorRun;
        }
        @Override
        protected void onPostExecute(Boolean result) {
            super.onPostExecute(result);
            dialogInventario.hide();
            if(result){
                //   Toast.makeText(getApplicationContext(),"Reader Success",Toast.LENGTH_SHORT).show();
            }else{
                Alerta.antenaEnUso(MainDesenlaceMasivoActivity.this);
                //  Toast.makeText(getApplicationContext(),"fail",Toast.LENGTH_SHORT).show();
            }
        }
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialogInventario = new ProgressDialog(MainDesenlaceMasivoActivity.this);
            dialogInventario.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            dialogInventario.setMessage(getString(R.string.iniciando_lector));
            dialogInventario.setCanceledOnTouchOutside(false);
            dialogInventario.show();
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){

            case R.id.btnLimpiar:
                detener();
                limpiar();
                break;

            case R.id.btnProcesar:
                detener();
                if(!contador.getText().toString().equals("0")) {
                    new procesarLectura().execute();
                }else{
                    Toasty.info(getApplicationContext(), getString(R.string.lee_etiquetas_para_desenlace), Toast.LENGTH_SHORT, true).show();
                }
                break;

            case R.id.atras:
                detener();
                NavUtils.navigateUpFromSameTask(this);
                finish();
                break;

            case R.id.btnLeer:
                if(leer.getText().toString().equals(getString(R.string.leer))){
                    iniciar();
                }else{
                    detener();
                }
                break;

        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        //se validan los botones presionados
        //boton regresar
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            sqLiteQuery.clearInventario();
            detener();
            NavUtils.navigateUpFromSameTask(this);
            finish();
            return true;
        }

        if(lectorRun == true) {
            //boton de lectura
            if(keyCode == 139 || keyCode == 280 || keyCode == 293) {
                iniciar();
                return true;
            }
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {


        if(lectorRun == true) {
            if(keyCode == 139 || keyCode == 280 || keyCode == 293) {
                detener();
                return true;
            }
        }
        return super.onKeyUp(keyCode, event);
    }

    public void detener(){

        lectura.setVisibility(View.INVISIBLE);
        leer.setText(getString(R.string.leer));
        leer.setBackground(getResources().getDrawable(R.drawable.main_button_iniciar));
        stopScan();
    }

    public void iniciar(){

        lectura.setVisibility(View.VISIBLE);
        leer.setText(getString(R.string.detener));
        startScan();
        leer.setBackground(getResources().getDrawable(R.drawable.main_button_detener));
    }

    private void stopScan() {
        if (reader == null || !reader.isPowerOn()) return;
//        try {
        looper = false;
            reader.stopInventory();
//        }
//        catch(ReaderException e) {
//            Toast.makeText(this, "ERROR: " + e, Toast.LENGTH_LONG).show();
//        }
    }

    //metodo para iniciar la lectura de etiquetas
    private void startScan() {
        //se comprueba la existencia de la instancia del lector
        if (reader == null || !reader.isPowerOn()) return;

//        try {
//            //se llama un proceso propio para la lectura
//            reader.inventory(new RFIDCallback() {
//                @Override
//                public void onTagRead(Tag tag) {
//                    addTag(tag);
//                }
//            });
//        }
//        catch (ReaderException e) {
//            Toast.makeText(this, "Error RFID: " + e, Toast.LENGTH_LONG).show();
//        }
        looper = true;
        if (reader.startInventoryTag(0,0,0)) {
            new readTask().execute();
        }
    }

    @SuppressLint("StaticFieldLeak")
    public class readTask extends AsyncTask<String, Integer, Boolean> {

        @Override
        protected Boolean doInBackground(String... strings) {
            String[] result = null;
            String tag;
            while (looper) {
                result = reader.readTagFromBuffer();
                if (result != null) {
                    tag = reader.convertUiiToEPC(result[1]);
                    //hilo de lecutra
                    addTag(tag);
                }
            }
            return null;
        }
    }

    //metodo para capturar el rfid
    public void addTag(final String tag) {
        //se comprueba que se ha leído algo
//        if(tag.getEPC().isEmpty()) return;
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (!etiquetas.containsKey(tag)) {
                            etiquetas.put(tag, tag);
                            sound.playBeep();
                            contador.setText(String.valueOf(etiquetas.size()));
                        }
                    }
                });

    }

    public void limpiar(){
        contador.setText("0");
        etiquetas.clear();
        desenlaceCorrecto = 0;
        desenlaceError = 0;
    }


    //metodo para inicializar el lector
    public class procesarLectura extends AsyncTask<String, Integer, Boolean> {
        ProgressDialog dialogAlmacena;
        @Override
        protected Boolean doInBackground(String... params) {

            Filtro filtro = new Filtro();
            String tags = filtro.Filtro(etiquetas);
            String[] partTags = tags.split(",");

            for(int i = 0 ; partTags.length > i; i++){
                if(sqLiteQuery.spVal_rfid(partTags[i])){
                    if(sqLiteQuery.spDel_rfid(partTags[i]) > 0){
                        desenlaceCorrecto ++;
                    }else{
                        desenlaceError ++;
                    }
                }else{
                    desenlaceError ++;
                }
            }

            return true;
        }
        @Override
        protected void onPostExecute(Boolean result) {
            super.onPostExecute(result);
            dialogAlmacena.hide();
            Alerta.Alerta("["+desenlaceCorrecto+"] "+ getString(R.string.etiquetas_correctas) +"\n"+"["+desenlaceError+"] "+getString(R.string.etiquetas_incorrectas)+"\n", getString(R.string.resultado), MainDesenlaceMasivoActivity.this);

            limpiar();

        }
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialogAlmacena = new ProgressDialog(MainDesenlaceMasivoActivity.this);
            dialogAlmacena.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            dialogAlmacena.setMessage(getString(R.string.almacenando_informacion));
            dialogAlmacena.setCanceledOnTouchOutside(false);
            dialogAlmacena.show();
        }
    }
}