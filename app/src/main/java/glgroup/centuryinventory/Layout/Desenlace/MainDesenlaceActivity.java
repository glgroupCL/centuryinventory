package glgroup.centuryinventory.Layout.Desenlace;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.NavUtils;

import android.app.ProgressDialog;
import android.media.AudioManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.alien.rfid.Mask;
import com.alien.rfid.RFID;
import com.alien.rfid.RFIDCallback;
import com.alien.rfid.RFIDReader;
import com.alien.rfid.ReaderException;
import com.alien.rfid.Tag;
import com.google.android.material.textfield.TextInputLayout;
import com.rscja.deviceapi.RFIDWithUHF;
import com.rscja.deviceapi.exception.ConfigurationException;

import java.util.ArrayList;

import es.dmoral.toasty.Toasty;
import glgroup.centuryinventory.Layout.Enlace.MainEnlaceActivity;
import glgroup.centuryinventory.R;
import glgroup.centuryinventory.Services.Alert.Alerta;
import glgroup.centuryinventory.Services.SQLite.SQLiteQuery;
import glgroup.centuryinventory.Services.Sound.Sonido;

public class MainDesenlaceActivity extends AppCompatActivity implements View.OnClickListener {

    //campo de lectura
    public TextView rfid;

    //información de la etiqueta
    public TextView info;

    //Linear de elementos
    public LinearLayout linearElementos;

    //Botones
    public Button limpiar, desenlace;

    //variable para el reader
    private RFIDWithUHF reader;
    boolean lectorRun;

    //SQLite
    SQLiteQuery sqLiteQuery;

    //Sonido
    Sonido sonido;

    //flecha atras
    private ImageButton atras;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_desenlace);
        getSupportActionBar().hide();

        sqLiteQuery = new SQLiteQuery(getApplicationContext());
        new InitTask().execute();
        init();
        //InitReader();
    }

    public void init(){

        sonido = new Sonido((AudioManager) getSystemService(AUDIO_SERVICE), MainDesenlaceActivity.this);
        sonido.setupSound();

        rfid = findViewById(R.id.idRfid);

        linearElementos = findViewById(R.id.linDesenlace);

        info = findViewById(R.id.txtInfo);

        limpiar = findViewById(R.id.btnLimpiar);
        limpiar.setOnClickListener(this);

        desenlace = findViewById(R.id.btnDesenlace);
        desenlace.setOnClickListener(this);

        atras = findViewById(R.id.atras);
        atras.setOnClickListener(this);
    }

    //metodo para inicializar el lector
    public class InitTask extends AsyncTask<String, Integer, Boolean> {
        ProgressDialog mypDialog;
        @Override
        protected Boolean doInBackground(String... params) {
            try {
                // initialize RFID interface and obtain a global RFID Reader instance
                reader = RFIDWithUHF.getInstance();
                reader.init();
                reader.setPower(8);
                lectorRun = true;
            }
            catch(ConfigurationException e) {
                lectorRun = false;
            }
            return lectorRun;
        }
        @Override
        protected void onPostExecute(Boolean result) {
            super.onPostExecute(result);
            mypDialog.hide();
            if(result){
              //  Toast.makeText(getApplicationContext(),"Reader Success",Toast.LENGTH_SHORT).show();
            }else{
                Alerta.antenaEnUso(MainDesenlaceActivity.this);
               // Toast.makeText(getApplicationContext(),"fail",Toast.LENGTH_SHORT).show();
            }
        }
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mypDialog = new ProgressDialog(MainDesenlaceActivity.this);
            mypDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            mypDialog.setMessage(getString(R.string.iniciando_lector));
            mypDialog.setCanceledOnTouchOutside(false);
            mypDialog.show();
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btnDesenlace:
                desenlazar();
                break;

            case R.id.btnLimpiar:
                limpiar();
                break;

            case R.id.atras:
                NavUtils.navigateUpFromSameTask(this);
                break;
        }

    }



    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        //se validan los botones presionados
        //boton regresar
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            NavUtils.navigateUpFromSameTask(this);
            return true;
        }

        if(lectorRun == true) {
            //boton de lectura
            if(keyCode == 139 || keyCode == 280 || keyCode == 293) {
                startScan();
                return true;
            }
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {


        if(lectorRun == true) {
            if(keyCode == 139 || keyCode == 280 || keyCode == 293) {
                stopScan();

                return true;
            }
        }
        return super.onKeyUp(keyCode, event);
    }

    private void stopScan() {
        if (reader == null || !reader.isPowerOn()) return;
//        try {
            reader.stopInventory();
//        }
//        catch(ReaderException e) {
//            Toast.makeText(this, "ERROR: " + e, Toast.LENGTH_LONG).show();
//        }
    }

    //metodo para iniciar la lectura de etiquetas
    private void startScan() {
        //se comprueba la existencia de la instancia del lector
        if (reader == null || !reader.isPowerOn()) return;
            if (reader.startInventoryTag(0,0,0)) {
                addTag();
            }
//        try {
//            //se llama un proceso propio para la lectura
//            reader.inventory(new RFIDCallback() {
//                @Override
//                public void onTagRead(Tag tag) {
//                    addTag(tag);
//                }
//            });
//        }
//        catch (ReaderException e) {
//            Toast.makeText(this, "Error RFID: " + e, Toast.LENGTH_LONG).show();
//        }
    }

    //metodo para capturar el rfid
    public void addTag() {
        //se comprueba que se ha leído algo
        String[] result = null;
        result = reader.readTagFromBuffer();
        if(result == null) return;
        final String tag;
        tag = reader.convertUiiToEPC(result[1]);
        //hilo de lecutra
        runOnUiThread(new Runnable() {
            @Override
            public void run() {

                //se obtiene la información del campo rfid
                String codigo = rfid.getText().toString();

                //se valida el contenido del txtCod, ya que si encuentra una etiqueta ésta tomará un valor de encontrado, así se evita volver a leer otra etiqueta
                if (codigo.equals("")) {
//                    String epc = tag.getEPC();
                    sonido.playBeep();
                    //se asgina el valor encontrado al campo rfid
                    rfid.setText(tag);
                    stopScan();
                    obtieneInfo(tag);
                }
            }
        });
    }

    public void obtieneInfo(String epc){

        if(sqLiteQuery.spVal_rfid(epc)){
            ArrayList<String> data = sqLiteQuery.spSel_InfoRfid(epc);
            if(data.size() > 0){
                info.setText(getString(R.string.codigo)+": "+data.get(0)+"\n"+getString(R.string.descripcion)+": "+data.get(1));
                linearElementos.setVisibility(View.VISIBLE);
            }
        }else{
            rfid.setText("");
            Toasty.error(getApplicationContext(), getString(R.string.rfid_no_enlazado), Toast.LENGTH_SHORT, true).show();
        }
    }

    public void desenlazar(){

        if(sqLiteQuery.spDel_rfid(rfid.getText().toString()) > 0){
            limpiar();
            Toasty.success(getApplicationContext(), getString(R.string.desenlace_correcto), Toast.LENGTH_SHORT, true).show();
        }else{
            Toasty.error(getApplicationContext(), getString(R.string.error_desenlace), Toast.LENGTH_SHORT, true).show();
        }

    }

    public void limpiar(){
        rfid.setText("");
        linearElementos.setVisibility(View.INVISIBLE);
    }


}
