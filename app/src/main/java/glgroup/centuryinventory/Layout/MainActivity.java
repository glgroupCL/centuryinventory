package glgroup.centuryinventory.Layout;

import android.Manifest;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.core.app.ActivityCompat;

import com.eftimoff.androidplayer.Player;
import com.eftimoff.androidplayer.actions.property.PropertyAction;
import com.google.android.material.textfield.TextInputLayout;
import com.tapadoo.alerter.Alerter;

import java.util.List;
import java.util.Locale;

import es.dmoral.toasty.Toasty;
import glgroup.centuryinventory.Layout.CargaItem.CargaBaseRfidActivity;
import glgroup.centuryinventory.Layout.CargaItem.DescargaBaseActivity;
import glgroup.centuryinventory.Layout.CargaItem.MainCargaItemActivity;
import glgroup.centuryinventory.Layout.Configuracion.MainConfiguracionActivity;
import glgroup.centuryinventory.Layout.Enlace.MainEnlaceActivity;
import glgroup.centuryinventory.Layout.Inventario.MainInventarioActivity;
import glgroup.centuryinventory.Layout.Pad.MainLecturaPadActivity;
import glgroup.centuryinventory.R;
import glgroup.centuryinventory.Services.Alert.AlertCarga;
import glgroup.centuryinventory.Services.Alert.Alerta;
import glgroup.centuryinventory.Services.EnvioCorreo.CorreoInformacion;
import glgroup.centuryinventory.Services.Internet.AccesoInternet;
import glgroup.centuryinventory.Services.Memory.Local;
import glgroup.centuryinventory.Services.Retrofit.ApiService;
import glgroup.centuryinventory.Services.Retrofit.Modelos.Datos;
import glgroup.centuryinventory.Services.Retrofit.Modelos.Resultado;
import glgroup.centuryinventory.Services.SQLite.SQLiteQuery;
import glgroup.centuryinventory.Services.Utilidades.AyudaInicial;
import glgroup.centuryinventory.Services.Utilidades.Imei;
import glgroup.centuryinventory.Services.Utilidades.Activacion;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    //
    TextView fechaValidez;

    //opciones de la vista
    private CardView cCarga, cEnlace, cDesenlace, cInventario, cCargaBase, cDecargaBase, cConfig, cDesenlacePad;

    //Imagen para indicar si existe un inventario pendiente
    private ImageView imgInventario, imgDesenlace, imgEnlace, imgDescargaBase, imgDesenlacePad;

    //lenguaje
    private ImageButton select_lenguaje;


    //codigo para los permisos de la aplicacion
    final int PHONE_CHECK_STORAGE = 200;

    //SQLite
    SQLiteQuery sqLiteQuery;

    //Alerta para inventario pendiente
    AlertDialog alertInventario;

    //condición para salir
    int salir = 0;

    //metodo para la validación
    private Call<Datos> resAutenticacion;

    //datos obtenidos de la validación
    private  Datos datosAutenticacion;

    //valida acceso a internet
    AccesoInternet accesoInternet = new AccesoInternet();

    /** cambio de idioma **/
    private Locale locale;
    private Configuration config = new Configuration();



    //AlertCarga
    AlertCarga alertCarga;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getSupportActionBar().hide();

        alertCarga = new AlertCarga("Validando", MainActivity.this, getLayoutInflater());
        alertCarga.alertCarga();

        sqLiteQuery = new SQLiteQuery(getApplicationContext());

        //se validan los permisos de la aplicación
        if(ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED || ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED || ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            validarPermisosExternal();
        }

       // sqLiteQuery.spIns_Item("7805540001059", "Espuma Limpiadora");

        init();
        if(!Local.getData("autenticacion", getApplicationContext(), "valido").equals("1")) {
            new verificaConexion().execute();

        }else {
            AyudaInicial ayudaInicial = new AyudaInicial();
            if (Local.getData("inicio", getApplicationContext(), "ayuda").equals("")) {
                ayudaInicial.inicio(getWindowManager(), MainActivity.this, cCarga, cEnlace, cDesenlace, cInventario, cConfig);
            }
            if(Activacion.fechaValidez(fechaValidez, Local.getData("autenticacion", getApplicationContext(), "fechaActivacion"), MainActivity.this) == 0){
                alertExpirado();
            }
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            salir ++;
            if(salir == 2){
                super.onBackPressed(); finishAffinity(); System.exit(0);
            }else{
                Toasty.info(getApplicationContext(), getString(R.string.presiona_nuevamente),Toast.LENGTH_SHORT).show();
            }
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    public void init(){

        fechaValidez = findViewById(R.id.fechaValidez);

        cCarga = findViewById(R.id.cardCarga);
        cCarga.setOnClickListener(this);

        cEnlace = findViewById(R.id.cardEnlace);
        cEnlace.setOnClickListener(this);

        cDesenlace = findViewById(R.id.cardDesenlace);
        cDesenlace.setOnClickListener(this);

        cDesenlacePad = findViewById(R.id.cardDesenlacePad);
        cDesenlacePad.setOnClickListener(this);

        cInventario = findViewById(R.id.cardInventario);
        cInventario.setOnClickListener(this);

        cCargaBase = findViewById(R.id.cardCargaBase);
        cCargaBase.setOnClickListener(this);

        cDecargaBase = findViewById(R.id.cardDescargaBase);
        cDecargaBase.setOnClickListener(this);

        cConfig = findViewById(R.id.cardConfig);
        cConfig.setOnClickListener(this);

        imgInventario = findViewById(R.id.imgInventario);
        imgDesenlace = findViewById(R.id.imgDesenlace);
        imgEnlace = findViewById(R.id.imgEnlace);
        imgDescargaBase = findViewById(R.id.imgDescargaBase);
        imgDesenlacePad = findViewById(R.id.imgDesenlacePad);

        select_lenguaje = findViewById(R.id.select_lenguaje);
        select_lenguaje.setOnClickListener(this);

        validaDatos();

        if(!Local.getData("animacion",getApplicationContext(),"inicio").equals("2")) {
            final View carga = findViewById(R.id.cardCarga);
            final View enlace = findViewById(R.id.cardEnlace);
            final View desenlace = findViewById(R.id.cardDesenlace);
            final View pad = findViewById(R.id.cardDesenlacePad);
            final View inventario = findViewById(R.id.cardInventario);
            final View configuracion = findViewById(R.id.cardConfig);
            final View cargaBase = findViewById(R.id.cardCargaBase);
            final View descargaBase = findViewById(R.id.cardDescargaBase);
            Local.setData("animacion", getApplicationContext(), "inicio", "2");
            animarInicio(carga, enlace, desenlace, pad, inventario, configuracion, descargaBase, cargaBase);
        }

    }

    @Override
    protected void onPostResume() {
        salir = 0;
        validaDatos();
        super.onPostResume();
    }
    private void showAlertWithOnClick() {
        Alerter.create(MainActivity.this)
                .setTitle(getString(R.string.aviso))
                .setBackgroundColorRes(R.color.colorAlert)
                .setText(getString(R.string.inventario_pendiente_envio))
                .enableInfiniteDuration(true)
                .setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Alerter.hide();
                    }
                })
                .show();
    }

    public void validaDatos(){
        //se valida si existe un inventario pendiente para cambiar el icono
        if(sqLiteQuery.spVal_resumenInventario()){
            imgInventario.setImageResource(R.drawable.ic_alert);
            showAlertWithOnClick();
        }else{
            if(Alerter.isShowing()){
                Alerter.hide();
            }
            imgInventario.setImageResource(R.drawable.ic_right_arrow);
        }

        //Se valida la existencia de etiquetas enlazadas para mostrar la flecha de acceso o no
        if(sqLiteQuery.spSel_rfid()){
            imgDesenlace.setVisibility(View.VISIBLE);
            imgInventario.setVisibility(View.VISIBLE);
            imgDescargaBase.setVisibility(View.VISIBLE);
            imgDesenlacePad.setVisibility(View.VISIBLE);
            if(!sqLiteQuery.spVal_ExistCorreo()) {
                imgInventario.setVisibility(View.INVISIBLE);
            }
        }else{
            imgDesenlace.setVisibility(View.INVISIBLE);
            imgInventario.setVisibility(View.INVISIBLE);
            imgDescargaBase.setVisibility(View.INVISIBLE);
            imgDesenlacePad.setVisibility(View.INVISIBLE);
        }

        //Se valida si existen items registrado en la base de datos antes de enlazar
        List items = sqLiteQuery.spSel_items();
        if(items.size() > 0) {
            imgEnlace.setVisibility(View.VISIBLE);
        }else{
            imgEnlace.setVisibility(View.INVISIBLE);
        }
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()){
            case R.id.cardCarga:
                if(Local.getData("autenticacion", getApplicationContext(), "valido").equals("1")) {
                    startActivity(new Intent(MainActivity.this, MainCargaItemActivity.class));
                }else{
                    new verificaConexion().execute();
                }
                break;

            case R.id.cardEnlace:
                List items = sqLiteQuery.spSel_items();
                if(items.size() > 0) {
                    startActivity(new Intent(MainActivity.this, MainEnlaceActivity.class));
                }else{
                    Toasty.error(getApplicationContext(), getString(R.string.no_items_almacenados), Toast.LENGTH_SHORT, true).show();
                }
                break;

            case R.id.cardDesenlace:
                if(sqLiteQuery.spSel_rfid()){
                    Alerta.alertDesenlace(getApplicationContext(),getLayoutInflater(),MainActivity.this);
                   // startActivity(new Intent(MainActivity.this, MainDesenlaceActivity.class));
                }else{
                    Toasty.error(getApplicationContext(), getString(R.string.no_items_enlazados), Toast.LENGTH_SHORT, true).show();
                }
                break;

            case R.id.cardDesenlacePad:
                if(sqLiteQuery.spSel_rfid()){
                    startActivity(new Intent(MainActivity.this, MainLecturaPadActivity.class));
                }else{
                    Toasty.error(getApplicationContext(), getString(R.string.no_items_enlazados), Toast.LENGTH_SHORT, true).show();
                }
                break;

            case R.id.cardInventario:

                if(sqLiteQuery.spSel_rfid()){
                    if(sqLiteQuery.spVal_ExistCorreo()) {
                        if (!sqLiteQuery.spVal_resumenInventario()) {
                            startActivity(new Intent(MainActivity.this, MainInventarioActivity.class));
                        } else {
                            alertaInventario();
                        }
                    }else{
                        Toasty.warning(getApplicationContext(), getString(R.string.registra_correo_antes), Toast.LENGTH_SHORT, true).show();
                    }
                }else{
                    Toasty.error(getApplicationContext(), getString(R.string.no_items_inventario), Toast.LENGTH_SHORT, true).show();
                }
                break;

            case R.id.cardCargaBase:
                startActivity(new Intent(MainActivity.this, CargaBaseRfidActivity.class));
                break;

            case R.id.cardDescargaBase:
                if(sqLiteQuery.spSel_rfid()){
                    startActivity(new Intent(MainActivity.this, DescargaBaseActivity.class));
                }else{
                    Toasty.error(getApplicationContext(), getString(R.string.no_existen_item_descarga), Toast.LENGTH_SHORT, true).show();
                }
                break;

            case R.id.cardConfig:
                if(Local.getData("autenticacion", getApplicationContext(), "valido").equals("1")) {
                   // startActivity(new Intent(MainActivity.this, MainLecturaPadActivity.class));
                    startActivity(new Intent(MainActivity.this, MainConfiguracionActivity.class));
                }else{
                    new verificaConexion().execute();
                }
                break;

            case R.id.select_lenguaje:
                Alerta.idioma(getLayoutInflater(), MainActivity.this);
                break;

        }

    }

    //alert para solicitar permisos al usuario
    public void alertaInventario(){
        final AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        LayoutInflater inflater = getLayoutInflater();

        View dialoglayout = inflater.inflate(R.layout.alerta_inventario, null);

        Button enviar = dialoglayout.findViewById(R.id.enviar);
        Button descartar = (Button) dialoglayout.findViewById(R.id.descartar);
        builder.setView(dialoglayout);

        alertInventario = builder.create();
        alertInventario.setCancelable(false);
        enviar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                alertInventario.dismiss();
                new procesarLectura().execute();

            }
        });
        descartar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sqLiteQuery.clearResumenInventario();
                imgInventario.setImageResource(R.drawable.ic_right_arrow);
                startActivity(new Intent(MainActivity.this, MainInventarioActivity.class));
                alertInventario.dismiss();
            }
        });
        alertInventario.show();
    }


    //metodo para inicializar el lector
    public class procesarLectura extends AsyncTask<String, Integer, Boolean> {
        ProgressDialog procesaCorreo;
        @Override
        protected Boolean doInBackground(String... params) {
            CorreoInformacion correoInformacion = new CorreoInformacion();

            correoInformacion.exportar(getApplicationContext(), MainActivity.this);
            while (Local.getData("correo", getApplicationContext(), "estado").equals("")){

            }
            procesaCorreo.cancel();
            return true;
        }
        @Override
        protected void onPostExecute(Boolean result) {
            super.onPostExecute(result);
            if(Local.getData("correo", getApplicationContext(), "estado").equals("true")){
                Toasty.success(getApplicationContext(), getString(R.string.correo_enviado), Toast.LENGTH_SHORT, true).show();
            }
            if(Local.getData("correo", getApplicationContext(), "estado").equals("false")){
                Toasty.error(getApplicationContext(), getString(R.string.error_enviar_correo), Toast.LENGTH_SHORT, true).show();
            }
            validaDatos();
        }
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            procesaCorreo = new ProgressDialog(MainActivity.this);
            procesaCorreo.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            procesaCorreo.setMessage(getString(R.string.enviando_correo));
            procesaCorreo.setCanceledOnTouchOutside(false);
            procesaCorreo.show();
        }
    }



    /************ Solicitud de permisos para la aplicación *************/
    public void validarPermisosExternal(){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
            //nuevas versiones
            requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.READ_PHONE_STATE},PHONE_CHECK_STORAGE);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode){
            case PHONE_CHECK_STORAGE:
                String write = permissions[0];
                String camera = permissions[1];
                String read = permissions[2];
                int resultWrite = grantResults[0];

                if(write.equals(Manifest.permission.WRITE_EXTERNAL_STORAGE) && camera.equals(Manifest.permission.CAMERA) && read.equals(Manifest.permission.READ_EXTERNAL_STORAGE)){
                    //Comprobar si se acepto la petición
                    if(resultWrite == PackageManager.PERMISSION_GRANTED){
                        if(ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                            return;
                        }
                    }else{
                        //No se concedió el permiso
                      //  Toast.makeText(getApplicationContext(), "Permiso denegado", Toast.LENGTH_SHORT).show();
                    }
                }
                break;
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }


    /***************** ANIMACIÓN DE INICIO ******************/

    private void animarInicio(View carga, View enlace, View descenlace, View descPad, View inventario, View config, View descargaBase, View cargaBase) {
        final PropertyAction car = PropertyAction.newPropertyAction(carga).translationY(300).duration(350).alpha(0f).build();
        final PropertyAction en = PropertyAction.newPropertyAction(enlace).translationY(300).duration(350).alpha(0f).build();
        final PropertyAction des = PropertyAction.newPropertyAction(descenlace).translationY(300).duration(350).alpha(0f).build();
        final PropertyAction pad = PropertyAction.newPropertyAction(descPad).translationY(300).duration(350).alpha(0f).build();
        final PropertyAction in = PropertyAction.newPropertyAction(inventario).translationY(300).duration(350).alpha(0f).build();
        final PropertyAction con = PropertyAction.newPropertyAction(config).translationY(300).duration(350).alpha(0f).build();
        final PropertyAction cBas = PropertyAction.newPropertyAction(cargaBase).translationY(300).duration(350).alpha(0f).build();
        final PropertyAction dBas = PropertyAction.newPropertyAction(descargaBase).translationY(300).duration(350).alpha(0f).build();
        Player.init().
                animate(car).
                then().
                animate(en).
                then().
                animate(des).
                then().
                animate(pad).
                then().
                animate(in).
                then().
                animate(dBas).
                then().
                animate(cBas).
                then().
                animate(con).
                play();
    }

    public void validarAplicacion(){
        final AlertDialog alertaCredencial;
        final AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);

        View dialoglayout = getLayoutInflater().inflate(R.layout.valida_acceso, null);

        final TextInputLayout credencial = dialoglayout.findViewById(R.id.credencial);
        final Button cancelar = dialoglayout.findViewById(R.id.btnCancelar);
        Button agregar =  dialoglayout.findViewById(R.id.btnCrear);
        builder.setView(dialoglayout);

        alertaCredencial = builder.create();
        alertaCredencial.setCancelable(false);
        agregar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(accesoInternet.isOnlineNet()) {
                    if (credencial.getEditText().getText().toString().equals("")) {
                        credencial.setErrorEnabled(true);
                        credencial.setError(getString(R.string.ingresar_codigo_activacion));
                    } else {
                        alertaCredencial.dismiss();
                        consultaValidacion(credencial.getEditText().getText().toString());


                    }
                }else{
                    alertaCredencial.dismiss();
                    sinConexion();
                }


            }
        });
        cancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertaCredencial.dismiss();
                finishAffinity(); System.exit(0);
            }
        });
        alertaCredencial.show();
    }



    public void consultaValidacion(String credencial){
        alertCarga.alertShow();
        resAutenticacion = ApiService.getApiService(Local.getData("conexion", getApplicationContext(), "urlConexion")).getActivacion(1,1, Imei.getImei(MainActivity.this),1,credencial);
        resAutenticacion.enqueue(new Callback<Datos>() {
            @Override
            public void onResponse(Call<Datos> call, Response<Datos> response) {
                if(response.isSuccessful()){
                    datosAutenticacion = response.body();
                    System.out.println(datosAutenticacion);
                    int idRespuesta = 0;
                    String mensaje = "";
                    for(Resultado resultado : datosAutenticacion.getDatos().getResultado()){
                        idRespuesta =resultado.getIdRespuesta();
                        mensaje = resultado.getRespuesta();
                        Local.setData("autenticacion", getApplicationContext(), "fechaActivacion", resultado.getFecha());
                    }

                    if(idRespuesta > 0){
                        alertCarga.alerDismiss();
                        Toasty.success(getApplicationContext(), mensaje,Toasty.LENGTH_LONG).show();
                        if(Activacion.fechaValidez(fechaValidez, Local.getData("autenticacion", getApplicationContext(), "fechaActivacion"), MainActivity.this) == 0){
                            alertExpirado();
                        }

                        //producto autenticado
                        Local.setData("autenticacion", getApplicationContext(), "valido","1");

                        AyudaInicial ayudaInicial = new AyudaInicial();
                        if (Local.getData("inicio", getApplicationContext(), "ayuda").equals("")) {
                            ayudaInicial.inicio(getWindowManager(), MainActivity.this, cCarga, cEnlace, cDesenlace, cInventario, cConfig);
                        }
                    }else{
                        alertCarga.alerDismiss();
                        Toasty.warning(getApplicationContext(), mensaje,Toasty.LENGTH_LONG).show();
                        validarAplicacion();
                    }

                }else{
                    alertCarga.alerDismiss();
                }
            }

            @Override
            public void onFailure(Call<Datos> call, Throwable t) {
                alertCarga.alerDismiss();
                System.out.println(call);
            }
        });
    }


    public void sinConexion(){
        final AlertDialog alertaCredencial;
        final AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);

        View dialoglayout = getLayoutInflater().inflate(R.layout.sin_acceso_internet, null);

        final Button cancelar = dialoglayout.findViewById(R.id.btnCancelar);
        Button comprobar =  dialoglayout.findViewById(R.id.btnComprobar);
        builder.setView(dialoglayout);

        alertaCredencial = builder.create();
        alertaCredencial.setCancelable(false);
        comprobar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertaCredencial.dismiss();
                new verificaConexion().execute();
            }
        });
        cancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertaCredencial.dismiss();
                finishAffinity(); System.exit(0);
            }
        });
        alertaCredencial.show();
    }


    public class verificaConexion extends AsyncTask<String, Integer, Boolean> {
        ProgressDialog validaInternet;
        @Override
        protected Boolean doInBackground(String... params) {
            if(accesoInternet.isOnlineNet()){
                validaInternet.cancel();
                return true;
            }else {
                validaInternet.cancel();
                return false;
            }
        }
        @Override
        protected void onPostExecute(Boolean result) {
            super.onPostExecute(result);
            if(result){
                validarAplicacion();
            }else{
                sinConexion();
            }
        }
        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            validaInternet = new ProgressDialog(MainActivity.this);
            validaInternet.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            validaInternet.setMessage(getString(R.string.comprobando_conexion));
            validaInternet.setCanceledOnTouchOutside(false);
            validaInternet.show();
        }
    }



    
    public void cambiarLenguaje(String lng){
        locale = new Locale(lng);
        config.locale =locale;
        Local.setData("lenguaje",getApplicationContext(),"lang",lng);
        getResources().updateConfiguration(config, null);
        Intent refresh = new Intent(MainActivity.this, MainActivity.class);
        startActivity(refresh);
    }

    public void alertExpirado() {
        androidx.appcompat.app.AlertDialog.Builder builder = new androidx.appcompat.app.AlertDialog.Builder(MainActivity.this);
        builder.setTitle("Importante");
        builder.setMessage("La licencia actual se encuentra expirada, necesitará una nueva clave de activación para seguir usando el sistema, sus datos actuales no se perderán.")
                .setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                Local.setData("autenticacion", getApplicationContext(), "valido","0");
                new verificaConexion().execute();
            }
        });
        final androidx.appcompat.app.AlertDialog alert = builder.create();
        alert.show();
    }






}
