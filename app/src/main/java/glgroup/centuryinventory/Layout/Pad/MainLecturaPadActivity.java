package glgroup.centuryinventory.Layout.Pad;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.provider.Settings;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.NumberPicker;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.android.material.navigation.NavigationView;
import com.rscja.deviceapi.RFIDWithUHFBLE;
import com.rscja.deviceapi.entity.UHFTAGInfo;
import com.rscja.deviceapi.interfaces.ConnectionStatus;
import com.rscja.deviceapi.interfaces.ConnectionStatusCallback;
import com.rscja.deviceapi.interfaces.KeyEventCallback;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import es.dmoral.toasty.Toasty;
import glgroup.centuryinventory.Layout.Desenlace.MainDesenlaceActivity;
import glgroup.centuryinventory.Layout.Desenlace.MainDesenlaceMasivoActivity;
import glgroup.centuryinventory.R;
import glgroup.centuryinventory.Services.Alert.Alerta;
import glgroup.centuryinventory.Services.Filtro.Filtro;
import glgroup.centuryinventory.Services.Pad.Constantes;
import glgroup.centuryinventory.Services.Pad.SPUtils;
import glgroup.centuryinventory.Services.Pad.Utils;
import glgroup.centuryinventory.Services.SQLite.SQLiteQuery;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static glgroup.centuryinventory.Layout.Pad.DeviceListActivity.SHOW_HISTORY_CONNECTED_LIST;

public class MainLecturaPadActivity extends AppCompatActivity implements View.OnClickListener {

    private DrawerLayout drawerLayout;
    private NavigationView navigationView;


    public boolean isScanning = false;
    public String remoteBTName = "";
    public String remoteBTAdd = "";
    private final static String TAG = "MainActivity";
    private static final int REQUEST_ENABLE_BT = 2;
    private static final int REQUEST_SELECT_DEVICE = 1;

    public BluetoothDevice mDevice = null;
    public TextView txtInfo, infoUbicacion;
    public BluetoothAdapter mBtAdapter = null;
    public RFIDWithUHFBLE uhf = RFIDWithUHFBLE.getInstance();
    BTStatus btStatus = new BTStatus();


    public boolean mIsActiveDisconnect = true; // 是否主动断开连接
    private static final int RECONNECT_NUM = Integer.MAX_VALUE; // 重连次数
    private int mReConnectCount = RECONNECT_NUM; // 重新连接次数

    private Timer mDisconnectTimer = new Timer();
    private DisconnectTimerTask timerTask;
    private long timeCountCur; // 断开时间选择
    private long period = 1000 * 30; // 隔多少时间更新一次
    private long lastTouchTime = System.currentTimeMillis(); // 上次接触屏幕操作的时间戳

    private static final int RUNNING_DISCONNECT_TIMER = 10;
    private Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case RUNNING_DISCONNECT_TIMER:
                    long time = (long) msg.obj;
                    formatConnectButton(time);
                    break;
            }
        }
    };

    private Toast toast;

    public void showToast(String text) {
        if (toast != null) {
            toast.cancel();
        }
        toast = Toast.makeText(this, text, Toast.LENGTH_SHORT);
        toast.show();
    }

    public void showToast(int resId) {
        showToast(getString(resId));
    }

    Button select, btnLeer, btnProcesar;
    TextView info, contador;
    LinearLayout linLectura;
    ImageButton btnLimpiar, atras, btnDesconectar, btnSetting;

    //SQLite
    SQLiteQuery sqLiteQuery;

    int desenlaceError = 0, desenlaceCorrecto = 0;

    /**
     * variable de lectura
     **/
    boolean isRuning = false;
    private boolean loopFlag = false;
    /**
     * lista de etiquetas leidas
     **/
    private Map<String, String> etiquetasLeidas = new HashMap<String, String>();


    private
    Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case Constantes.FLAG_STOP:

                    break;
                case Constantes.FLAG_START:

                    break;
                case Constantes.FLAG_UPDATE_TIME:
//                    float useTime = (System.currentTimeMillis() - mStrTime) / 1000.0F;
//                    tv_time.setText(NumberTool.getPointDouble(loopFlag ? 1 : 3, useTime) + "s");
                    break;
                case Constantes.FLAG_UHFINFO:
                    UHFTAGInfo info = (UHFTAGInfo) msg.obj;
                    agregarEtiquetas(info.getEPC());
                    break;
            }
        }
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_lectura_pad);
        if (getSupportActionBar() != null) {
            getSupportActionBar().hide();
        }
        sqLiteQuery = new SQLiteQuery(getApplicationContext());

        select = findViewById(R.id.btnSelect);
        info = findViewById(R.id.info);
        linLectura = findViewById(R.id.linLectura);
        btnLeer = findViewById(R.id.btnLeer);
        btnLeer.setOnClickListener(this);
        contador = findViewById(R.id.contador);
        btnLimpiar = findViewById(R.id.btnLimpiar);
        btnLimpiar.setOnClickListener(this);
        atras = findViewById(R.id.atras);
        atras.setOnClickListener(this);
        btnProcesar = findViewById(R.id.btnProcesar);
        btnProcesar.setOnClickListener(this);
        btnDesconectar = findViewById(R.id.btnDesconectar);
        btnDesconectar.setOnClickListener(this);
        btnSetting = findViewById(R.id.btnSetting);
        btnSetting.setOnClickListener(this);

        checkLocationEnable();
        uhf.init(getApplicationContext());
        Utils.initSound(getApplicationContext());
        mBtAdapter = BluetoothAdapter.getDefaultAdapter();
        if (uhf.getConnectStatus() == ConnectionStatus.CONNECTED) {
            disconnect(true);
            select.setVisibility(View.INVISIBLE);
        } else {

        }

        select.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showBluetoothDevice(true);
            }
        });


    }

    @Override
    public void onBackPressed() {
        disconnect(true);
        finish();
        super.onBackPressed();
    }

    @Override
    protected void onDestroy() {
        disconnect(true);
        super.onDestroy();
    }

    public void detener() {

        // lectura.setVisibility(View.INVISIBLE);
        btnLeer.setText(getString(R.string.leer));
        btnLeer.setBackground(getResources().getDrawable(R.drawable.main_button_iniciar));
        stopInventory();
    }

    public void iniciar() {

        //   lectura.setVisibility(View.VISIBLE);
        btnLeer.setText(getString(R.string.detener));
        btnLeer.setBackground(getResources().getDrawable(R.drawable.main_button_detener));
        startScan();
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnLeer:
                if (uhf.getConnectStatus() == ConnectionStatus.CONNECTED) {
                    if (btnLeer.getText().toString().equals(getString(R.string.leer))) {
                        iniciar();
                    } else {
                        detener();
                    }
                } else {
                    Toasty.warning(getApplicationContext(), getString(R.string.no_existe_dispositivo), Toasty.LENGTH_LONG).show();
                }
                break;

            case R.id.btnLimpiar:
                limpiar();
                break;

            case R.id.atras:
                disconnect(true);
                finish();
                break;

            case R.id.btnProcesar:
                detener();
                if(etiquetasLeidas.size() != 0) {
                    new procesarLectura().execute();
                }else{
                    Toasty.warning(getApplicationContext(), getString(R.string.lee_etiquetas_para_desenlace), Toasty.LENGTH_SHORT).show();
                }
                break;

            case R.id.btnDesconectar:
                if(uhf.getConnectStatus() == ConnectionStatus.CONNECTED){
                    disconnect(true);
                }
                break;

            case R.id.btnSetting:
                alertPower();
                break;
        }

    }

    public void limpiar() {
        detener();
        contador.setText("0");
        etiquetasLeidas.clear();
        desenlaceError = 0;
        desenlaceCorrecto = 0;
    }

    public void startScan() {
        if (isRuning) {
            return;
        }
        isRuning = true;
        // cbFilter.setChecked(false);
        new TagThread().start();
    }


    class TagThread extends Thread {
        public void run() {
            Message msg = handler.obtainMessage(Constantes.FLAG_START);
            if (uhf.startInventoryTag()) {
                loopFlag = true;
                isScanning = true;
                msg.arg1 = Constantes.FLAG_SUCCESS;
            } else {
                msg.arg1 = Constantes.FLAG_FAIL;
            }
            handler.sendMessage(msg);
            isRuning = false;//执行完成设置成false
            while (loopFlag) {
                getUHFInfo();
                handler.sendEmptyMessage(Constantes.FLAG_UPDATE_TIME);
            }
            stopInventory();
        }
    }

    /**
     * obtiene información de la etiqueta y la envía al handler
     **/
    private void getUHFInfo() {
        List<UHFTAGInfo> list = uhf.readTagFromBufferList_EpcTidUser();
        if (list != null && !list.isEmpty()) {
            for (int k = 0; k < list.size(); k++) {
                Message msg = handler.obtainMessage(Constantes.FLAG_UHFINFO, list.get(k));
                handler.sendMessage(msg);
                if (!loopFlag) {
                    break;
                }
            }
        }
    }

    /**
     * se detiene el proceso de inventario
     **/
    private void stopInventory() {
        loopFlag = false;
        boolean result = uhf.stopInventory();
        if (isScanning) {
            ConnectionStatus connectionStatus = uhf.getConnectStatus();
            Message msg = handler.obtainMessage(Constantes.FLAG_STOP);
            if (result || connectionStatus == ConnectionStatus.DISCONNECTED) {
                msg.arg1 = Constantes.FLAG_SUCCESS;
            } else {
                msg.arg1 = Constantes.FLAG_FAIL;
            }
            if (connectionStatus == ConnectionStatus.CONNECTED) {
                //在连接的情况下，结束之后继续接收未接收完的数据
                //getUHFInfoEx();
            }
            isScanning = false;
            handler.sendMessage(msg);
        }
    }


    /**
     * agregar las etiquetas a la lista
     **/
    public void agregarEtiquetas(String epc) {
        Log.d("etiquetas", epc);
        if (!etiquetasLeidas.containsKey(epc)) {
            etiquetasLeidas.put(epc, epc);
            contador.setText(String.valueOf(etiquetasLeidas.size()));
        }
    }


    //metodo para procesar la lectura de etiquetas
    public class procesarLectura extends AsyncTask<String, Integer, Boolean> {
        ProgressDialog dialogAlmacena;
        @Override
        protected Boolean doInBackground(String... params) {

            Filtro filtro = new Filtro();
            String tags = filtro.Filtro(etiquetasLeidas);
            String[] partTags = tags.split(",");

            for(int i = 0 ; partTags.length > i; i++){
                if(sqLiteQuery.spVal_rfid(partTags[i])){
                    if(sqLiteQuery.spDel_rfid(partTags[i]) > 0){
                        desenlaceCorrecto ++;
                    }else{
                        desenlaceError ++;
                    }
                }else{
                    desenlaceError ++;
                }
            }

            return true;
        }
        @Override
        protected void onPostExecute(Boolean result) {
            super.onPostExecute(result);
            dialogAlmacena.hide();
            Alerta.Alerta("["+desenlaceCorrecto+"] "+ getString(R.string.etiquetas_correctas) +"\n"+"["+desenlaceError+"] "+getString(R.string.etiquetas_incorrectas)+"\n", getString(R.string.resultado), MainLecturaPadActivity.this);
            limpiar();

        }
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialogAlmacena = new ProgressDialog(MainLecturaPadActivity.this);
            dialogAlmacena.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            dialogAlmacena.setMessage(getString(R.string.almacenando_informacion));
            dialogAlmacena.setCanceledOnTouchOutside(false);
            dialogAlmacena.show();
        }
    }


    /** permite controlar la potencia del lectura para el dipositivo **/
    public void alertPower(){
        final android.app.AlertDialog alertEnlace;
        final android.app.AlertDialog.Builder busqueda = new android.app.AlertDialog.Builder(MainLecturaPadActivity.this);


        View dialog = getLayoutInflater().inflate(R.layout.alert_power_select, null);

        final Button cancelar = dialog.findViewById(R.id.btnCancelar);
        final Button guardar = dialog.findViewById(R.id.btnGuardar);
        final NumberPicker nbPicker = dialog.findViewById(R.id.nbPicker);

        nbPicker.setMinValue(5);
        nbPicker.setMaxValue(30);

        nbPicker.setValue(uhf.getPower());

        busqueda.setView(dialog);
        alertEnlace = busqueda.create();
        alertEnlace.setCancelable(true);

        guardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try{
                    uhf.setPower(nbPicker.getValue());
                }catch (Exception e){

                }
                alertEnlace.dismiss();


            }
        });
        cancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertEnlace.dismiss();

            }
        });
        alertEnlace.show();
    }




///////////////////////////////////////   Conexion ////////////////////////////////////////////////


    /**
     * SDK
     ***/
    private void formatConnectButton(long disconnectTime) {
        if (uhf.getConnectStatus() == ConnectionStatus.CONNECTED) {
            if (!isScanning && System.currentTimeMillis() - lastTouchTime > 1000 * 30 && timerTask != null) {
                long minute = disconnectTime / 1000 / 60;
                if (minute > 0) {
                    txtInfo.setText(getString(R.string.disConnectForMinute, minute)); //倒计时分
                } else {
                    // btn_connect.setText(getString(R.string.disConnectForSecond, disconnectTime / 1000)); // 倒计时秒
                }
            } else {
                //  btn_connect.setText(R.string.disConnect);
            }
        } else {
            // btn_connect.setText("R.string.Connect");
        }
    }

    /**
     * 重置断开时间
     */
    public void resetDisconnectTime() {
        timeCountCur = SPUtils.getInstance(getApplicationContext()).getSPLong(SPUtils.DISCONNECT_TIME, 0);
        if (timeCountCur > 0) {
            formatConnectButton(timeCountCur);
        }
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        lastTouchTime = System.currentTimeMillis();
        resetDisconnectTime();
        return super.dispatchTouchEvent(ev);
    }

    /**
     * resultado del dispositivo bluetooth seleccionado
     **/
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case REQUEST_SELECT_DEVICE:
                //When the DeviceListActivity return, with the selected device address
                if (resultCode == Activity.RESULT_OK && data != null) {
                    if (uhf.getConnectStatus() == ConnectionStatus.CONNECTED) {
                        disconnect(true);
                    }
                    String deviceAddress = data.getStringExtra(BluetoothDevice.EXTRA_DEVICE);
                    mDevice = BluetoothAdapter.getDefaultAdapter().getRemoteDevice(deviceAddress);
                    connect(deviceAddress);

                }
                break;
            case REQUEST_ENABLE_BT:
                if (resultCode == Activity.RESULT_OK) {
                    showToast("Bluetooth has turned on ");
                } else {
                    showToast("Problem in BT Turning ON ");
                }
                break;
            default:
                break;
        }
    }

    /**
     * se solicita mostrar la lista de bluetooth
     **/
    private void showBluetoothDevice(boolean isHistory) {
        if (mBtAdapter == null) {
            showToast("Bluetooth is not available");
            return;
        }
        if (!mBtAdapter.isEnabled()) {
            Log.i(TAG, "onClick - BT not enabled yet");
            Intent enableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableIntent, REQUEST_ENABLE_BT);
        } else {
            Intent newIntent = new Intent(MainLecturaPadActivity.this, DeviceListActivity.class);
            newIntent.putExtra(SHOW_HISTORY_CONNECTED_LIST, isHistory);
            startActivityForResult(newIntent, REQUEST_SELECT_DEVICE);
            cancelDisconnectTimer();
        }
    }

    public void connect(String deviceAddress) {
        if (uhf.getConnectStatus() == ConnectionStatus.CONNECTING) {
            showToast(R.string.connecting);
        } else {
            uhf.connect(deviceAddress, btStatus);
        }
    }

    public void disconnect(boolean isActiveDisconnect) {
        cancelDisconnectTimer();
        mIsActiveDisconnect = isActiveDisconnect; // 主动断开为true
        uhf.disconnect();
    }


    /**
     * 重新连接
     *
     * @param deviceAddress
     */
    private void reConnect(String deviceAddress) {
        if (!mIsActiveDisconnect && mReConnectCount > 0) {
            connect(deviceAddress);
            mReConnectCount--;
        }
    }

    /**
     * 应该提示未连接状态
     *
     * @return
     */
    private boolean shouldShowDisconnected() {
        return mIsActiveDisconnect || mReConnectCount == 0;
    }

    /**
     * estado del bluetooth
     **/
    class BTStatus implements ConnectionStatusCallback<Object> {
        @Override
        public void getStatus(final ConnectionStatus connectionStatus, final Object device1) {
            runOnUiThread(new Runnable() {
                public void run() {
                    BluetoothDevice device = (BluetoothDevice) device1;
                    remoteBTName = "";
                    remoteBTAdd = "";
                    if (connectionStatus == ConnectionStatus.CONNECTED) {
                        remoteBTName = device.getName();
                        remoteBTAdd = device.getAddress();

                        if (shouldShowDisconnected()) {
                            //  showToast(R.string.connect_success);
                        }
                        info.setTextColor(Color.parseColor("#FF4ABD00"));
                        info.setText(String.format("%s(%s)" + getString(R.string.conectado), remoteBTName, remoteBTAdd));
                        select.setVisibility(View.GONE);
                        btnLimpiar.setVisibility(View.VISIBLE);
                        linLectura.setVisibility(View.VISIBLE);
                        btnDesconectar.setVisibility(View.VISIBLE);
                        timeCountCur = SPUtils.getInstance(getApplicationContext()).getSPLong(SPUtils.DISCONNECT_TIME, 0);
                        if (timeCountCur > 0) {
                            startDisconnectTimer(timeCountCur);
                        } else {
                            formatConnectButton(timeCountCur);
                        }

                        // 保存已链接记录
                        if (!TextUtils.isEmpty(remoteBTAdd)) {
                            //  saveConnectedDevice(remoteBTAdd, remoteBTName);
                        }


                        mIsActiveDisconnect = false;
                        mReConnectCount = RECONNECT_NUM;
                    } else if (connectionStatus == ConnectionStatus.DISCONNECTED) {
                        cancelDisconnectTimer();
                        formatConnectButton(timeCountCur);
                        if (device != null) {
                            remoteBTName = device.getName();
                            remoteBTAdd = device.getAddress();
                        }
                        Toasty.error(getApplicationContext(), getString(R.string.dispositivo_desconectado), Toasty.LENGTH_SHORT).show();
                        select.setVisibility(View.VISIBLE);
                        info.setText(getString(R.string.desconectado));
                        linLectura.setVisibility(View.INVISIBLE);
                        btnLimpiar.setVisibility(View.INVISIBLE);
                        btnDesconectar.setVisibility(View.INVISIBLE);
                        info.setTextColor(Color.parseColor("#FFF30606"));
                        limpiar();

                        boolean reconnect = SPUtils.getInstance(getApplicationContext()).getSPBoolean(SPUtils.AUTO_RECONNECT, false);
                        if (mDevice != null && reconnect) {
                            reConnect(mDevice.getAddress()); // 重连
                        }
                    }

                    for (IConnectStatus iConnectStatus : connectStatusList) {
                        if (iConnectStatus != null) {
                            iConnectStatus.getStatus(connectionStatus);
                        }
                    }
                }
            });
        }
    }


//    public void updateConnectMessage(String oldName, String newName) {
//        if (!TextUtils.isEmpty(oldName) && !TextUtils.isEmpty(newName)) {
//            //  tvAddress.setText(tvAddress.getText().toString().replace(oldName, newName));
//            remoteBTName = newName;
//        }
//    }

    private List<IConnectStatus> connectStatusList = new ArrayList<>();

//    public void addConnectStatusNotice(IConnectStatus iConnectStatus) {
//        connectStatusList.add(iConnectStatus);
//    }
//
//    public void removeConnectStatusNotice(IConnectStatus iConnectStatus) {
//        connectStatusList.remove(iConnectStatus);
//    }

    public interface IConnectStatus {
        void getStatus(ConnectionStatus connectionStatus);
    }


    private static final int ACCESS_FINE_LOCATION_PERMISSION_REQUEST = 100;
    private static final int REQUEST_ACTION_LOCATION_SETTINGS = 3;

    public int checkLocationEnable() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, ACCESS_FINE_LOCATION_PERMISSION_REQUEST);
            }
        }
        if (!isLocationEnabled()) {
            Utils.alert(this, R.string.get_location_permission, getString(R.string.tips_open_the_ocation_permission), R.drawable.ic_alert, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                    startActivityForResult(intent, REQUEST_ACTION_LOCATION_SETTINGS);

                }
            });
            return 2;
        } else {
            return 1;
        }
    }

    /**
     * valida si el acceso a la locacion gps esta activa
     **/
    private boolean isLocationEnabled() {
        int locationMode = 0;
        String locationProviders;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            try {
                locationMode = Settings.Secure.getInt(getContentResolver(), Settings.Secure.LOCATION_MODE);
            } catch (Settings.SettingNotFoundException e) {
                e.printStackTrace();
                return false;
            }
            return locationMode != Settings.Secure.LOCATION_MODE_OFF;
        } else {
            locationProviders = Settings.Secure.getString(getContentResolver(), Settings.Secure.LOCATION_PROVIDERS_ALLOWED);
            return !TextUtils.isEmpty(locationProviders);
        }
    }

    private void startDisconnectTimer(long time) {
        timeCountCur = time;
        timerTask = new DisconnectTimerTask();
        mDisconnectTimer.schedule(timerTask, 0, period);
    }

    public void cancelDisconnectTimer() {
        timeCountCur = 0;
        if (timerTask != null) {
            timerTask.cancel();
            timerTask = null;
        }
    }

    /**
     * tiempo de conexión
     **/
    private class DisconnectTimerTask extends TimerTask {
        @Override
        public void run() {
            Log.e(TAG, "timeCountCur = " + timeCountCur);
            Message msg = mHandler.obtainMessage(RUNNING_DISCONNECT_TIMER, timeCountCur);
            mHandler.sendMessage(msg);
            if (isScanning) {
                resetDisconnectTime();
            } else if (timeCountCur <= 0) {
                disconnect(true);
            }
            timeCountCur -= period;
        }
    }


}
