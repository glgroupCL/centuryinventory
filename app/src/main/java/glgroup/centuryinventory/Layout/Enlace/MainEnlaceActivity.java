package glgroup.centuryinventory.Layout.Enlace;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.NavUtils;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.media.AudioManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.InputType;
import android.view.KeyEvent;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.rscja.deviceapi.RFIDWithUHF;
import com.rscja.deviceapi.exception.ConfigurationException;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

import es.dmoral.toasty.Toasty;
import glgroup.centuryinventory.Barcode2D;
import glgroup.centuryinventory.IBarcodeResult;
import glgroup.centuryinventory.R;
import glgroup.centuryinventory.Services.Alert.Alerta;
import glgroup.centuryinventory.Services.Memory.Local;
import glgroup.centuryinventory.Services.SQLite.SQLiteQuery;
import glgroup.centuryinventory.Services.Sound.Sonido;

public class MainEnlaceActivity extends AppCompatActivity implements View.OnClickListener, IBarcodeResult {

    class NoTagFound extends Exception {
        NoTagFound(String msg) {
            super(msg);
        }
    }

    //boton para buscar item en la maestra
    private ImageButton buscar;

    //textos de la vista
    private TextInputLayout txtSku;
    private TextView rfid;
    private TextInputEditText sku;
    private CheckBox fijarProducto;

    //descripcion del producto
    private TextView txtDesc, titProducto;

    //boton de enlace y limpiar
    private Button enlace, limpiar;

    //seccion de botones
    private LinearLayout linEnlace;

    //SQLite
    SQLiteQuery sqLiteQuery;

    //variable para el reader
    private RFIDWithUHF reader;
    boolean lectorRun;

    //Barcode
    private Barcode2D barcodeReader;
//    private BarcodeReader barcodeReader;

    //Sonido
    Sonido sonido;

    //flecha atras
    private ImageButton atras;

    private String tagBLock;
    private boolean grabando = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_enlace);
        getSupportActionBar().hide();

        try {
            init();
        } catch (ConfigurationException e) {
            e.printStackTrace();
        }
    }

    public void init() throws ConfigurationException {

        sonido = new Sonido((AudioManager) getSystemService(AUDIO_SERVICE), MainEnlaceActivity.this);
        sonido.setupSound();

        fijarProducto = findViewById(R.id.fijarProducto);

        buscar = findViewById(R.id.buscarItem);
        buscar.setOnClickListener(this);

        txtSku = findViewById(R.id.edSku);
        sku = findViewById(R.id.sku);
        sku.setOnClickListener(this);

        txtDesc = findViewById(R.id.txtProducto);
        titProducto = findViewById(R.id.titProducto);

        rfid = findViewById(R.id.idRfid);

        linEnlace = findViewById(R.id.linEnlace);

        enlace = findViewById(R.id.btnEnlace);
        enlace.setOnClickListener(this);

        limpiar = findViewById(R.id.btnLimpiar);
        limpiar.setOnClickListener(this);

        atras = findViewById(R.id.atras);
        atras.setOnClickListener(this);

//        barcodeReader = new BarcodeReader(getApplicationContext());
//            barcodeReader = new Barcode2D(this);
        barcodeReader = new Barcode2D(this);
        barcodeReader.open(this, this);

        sqLiteQuery = new SQLiteQuery(getApplicationContext());
        new InitTask().execute();
    }

    //metodo para inicializar el lector
    public class InitTask extends AsyncTask<String, Integer, Boolean> {
        ProgressDialog dialogEnlace;
        @Override
        protected Boolean doInBackground(String... params) {
            try {
                // initialize RFID interface and obtain a global RFID Reader instance
//                reader.free();
                reader = RFIDWithUHF.getInstance();
                reader.init();
                reader.setPower(8);
                String sizeStr = Integer.toString(4);
//                reader.setFrequencyMode(Byte.parseByte(sizeStr));
                lectorRun = true;
            }
            catch(ConfigurationException e) {
                lectorRun = false;
            }
            return lectorRun;
        }
        @Override
        protected void onPostExecute(Boolean result) {
            super.onPostExecute(result);
            dialogEnlace.hide();
            if(result){
              //  Toast.makeText(getApplicationContext(),"Reader Success",Toast.LENGTH_SHORT).show();
            }else{
                Alerta.antenaEnUso(MainEnlaceActivity.this);
               // Toast.makeText(getApplicationContext(),"Otro ",Toast.LENGTH_SHORT).show();
            }
        }
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialogEnlace = new ProgressDialog(MainEnlaceActivity.this);
            dialogEnlace.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            dialogEnlace.setMessage(getString(R.string.iniciando_lector));
            dialogEnlace.setCanceledOnTouchOutside(false);
            dialogEnlace.show();
        }
    }


    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        //se validan los botones presionados
        //boton regresar
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            NavUtils.navigateUpFromSameTask(this);
            return true;
        }
        if(txtSku.getEditText().getText().toString().equals("")) {
            onKey(event);
        }else {
            if (lectorRun == true) {
                //boton de lectura
                if (keyCode == 139 || keyCode == 280 || keyCode == 293) {
                    if(rfid.getText().toString().equals("")){
                        readKPwd();
                    }
                   // startScan();
                    return true;
                }
            }
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        if(lectorRun) {
            barcodeReader.stopScan(this);
            stopScan();
        }

        return super.onKeyUp(keyCode, event);
    }

    private void stopScan() {
        if (reader == null || !reader.isPowerOn()) return;
        reader.stopInventory();
    }

    //metodo para iniciar la lectura de etiquetas
    private void startScan() {
        //se comprueba la existencia de la instancia del lector
        if (reader == null || reader.isPowerOn()) return;

//        try {
            //se llama un proceso propio para la lectura
            addTag(reader.inventorySingleTag());
//            reader.inventory(new RFIDCallback() {
//                @Override
//                public void onTagRead(Tag tag) {
//                    addTag(tag);
//                }
//            });
//        }
//        catch (ReaderException e) {
//            Toast.makeText(this, "Error RFID: " + e, Toast.LENGTH_LONG).show();
//        }
    }

    //metodo para capturar el rfid
    public void addTag(final String tag) {
        //se comprueba que se ha leído algo
        if(tag.isEmpty()) return;

        //hilo de lecutra
        runOnUiThread(new Runnable() {
            @Override
            public void run() {

                //se obtiene la información del campo rfid
                String codigo = rfid.getText().toString();

                //se valida el contenido del txtCod, ya que si encuentra una etiqueta ésta tomará un valor de encontrado, así se evita volver a leer otra etiqueta
                if (codigo.equals("")) {
                    sonido.playBeep();
                    String epc = reader.convertUiiToEPC(tag);
                    tagBLock = tag;
                    //se asgina el valor encontrado al campo rfid
                    rfid.setText(epc);
                    stopScan();
                }
            }
        });
    }

    public boolean onKey(KeyEvent keyEvent) {

        int keyaction = keyEvent.getAction();
        int key = keyEvent.getKeyCode();
        switch (keyaction) {
            case KeyEvent.ACTION_DOWN:
                if (key == 139 || key == 280 || key == 293) {
//                        barcodeReader = Barcode2D.getInstance();
                        barcodeReader.startScan(this);
//                        buscaItemSku(barcodeReader.scan());
//                        sonido.playBeep();
//                    barcodeReader.start(new BarcodeCallback() {
//                        @Override
//                        public void onBarcodeRead(String s) {
//                            sonido.playBeep();
//                            buscaItemSku(s);
//                        }
//                    });
                    return true;
                }
                break;
            case KeyEvent.ACTION_UP:
                if (key == 139 || key == 280 || key == 293) {
                    barcodeReader.stopScan(this);
                    return true;
                }
                break;
        }
        return false;
    }




    @Override
    public void onClick(View view) {

        switch (view.getId()){
            case R.id.sku:
                alertSku();
                break;

            case R.id.buscarItem:
                SelecItem();
                break;

            case R.id.btnLimpiar:
                ocultarElementos();
                break;

            case R.id.btnEnlace:
                if(rfid.getText().toString().equals("")){
                    Toasty.info(getApplicationContext(), getString(R.string.leer_rfid_antes_enlazar), Toast.LENGTH_SHORT, true).show();
                }else{
                    enlazar();
                }
                break;

            case R.id.atras:
                NavUtils.navigateUpFromSameTask(this);
                break;
        }
    }

    //alerta que muestra los item en la base de datos y permite al usuario seleccionar uno de la lista
    public void SelecItem(){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(getString(R.string.selecciona_item_lista));

        final List<String> listItems = sqLiteQuery.spSel_items();

        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_dropdown_item_1line, listItems);
        builder.setAdapter(dataAdapter, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                txtSku.getEditText().setText(listItems.get(which));
                txtDesc.setText(listItems.get(which));
                buscarItemDesc(listItems.get(which));
            }
        });
        builder.setNegativeButton(getString(R.string.cancelar), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        AlertDialog dialog = builder.create();
        dialog.show();
    }




    //alerta para solicitar al usuario que que ingrese el sku manualmente
    public void alertSku(){

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setIcon(R.drawable.ic_barcode);
        builder.setTitle(getString(R.string.ingresa_codigo));
        //configura el tipo de input
        final EditText input = new EditText(this);
        // Especifica el tipo de texto a utilizar
        input.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_CLASS_TEXT);
        //se inserta el input al constructor
        builder.setView(input);

        // botones
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if(!input.getText().toString().equals("")) {
                    String sku = input.getText().toString();
                    buscaItemSku(sku);
                    //txtSku.getEditText().setText(epc);
                }
            }
        });
        builder.setNegativeButton(getString(R.string.cancelar), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        });
        builder.show();
    }

    //se busca el item por la descripción obtenida desde la lista de item
    public void buscarItemDesc(String desc){
        ArrayList<String> item = sqLiteQuery.spSel_idItem_desc(desc);
        if(item.size() == 0){
            Toasty.warning(getApplicationContext(), getString(R.string.no_existe_elementos_asociados), Toast.LENGTH_SHORT, true).show();
        }else{
            txtSku.getEditText().setText(item.get(1));
            txtDesc.setText(desc);
            Local.setData("enlace", getApplicationContext(), "idItem", item.get(0));
            mostrarElementos();
        }
    }

    //se busca el item por su sku
    public void buscaItemSku(String sku){
        ArrayList<String> item = sqLiteQuery.spSel_idItem_sku(sku);
        if(item.size() == 0){
            Toasty.warning(getApplicationContext(), getString(R.string.no_existe_elementos_asociados_sku), Toast.LENGTH_SHORT, true).show();
        }else{
            txtSku.getEditText().setText(sku);
            txtDesc.setText(item.get(1));
            Local.setData("enlace", getApplicationContext(), "idItem", item.get(0));
            mostrarElementos();
        }
    }

    //metodo para enlazar, se valida si existe el rfid ya enlazado
    public void enlazar(){
        if(sqLiteQuery.spVal_rfid(rfid.getText().toString())){
            Toasty.warning(getApplicationContext(), getString(R.string.rfid_ya_registrado), Toast.LENGTH_SHORT, true).show();
            rfid.setText("");
        }else{
            if(sqLiteQuery.spIns_rfid(rfid.getText().toString(),Local.getData("enlace", getApplicationContext(), "idItem" )) > 0){
                Toasty.success(getApplicationContext(), getString(R.string.enlace_correcto), Toast.LENGTH_SHORT, true).show();
                if(fijarProducto.isChecked()){
                    rfid.setText("");
                }else {
                    ocultarElementos();
                }
            }else{
                Toasty.error(getApplicationContext(), getString(R.string.error_enlace), Toast.LENGTH_SHORT, true).show();
            }
        }
    }


    //metodo para mostrar los elementos luego de encontrar un item válido
    public void mostrarElementos(){
        rfid.setVisibility(View.VISIBLE);
        txtDesc.setVisibility(View.VISIBLE);
        linEnlace.setVisibility(View.VISIBLE);
        titProducto.setVisibility(View.VISIBLE);
    }

    //método para ocultar elementos despues de enlazar o la ocurrencia de un error
    public void ocultarElementos(){
        rfid.setVisibility(View.INVISIBLE);
        txtDesc.setVisibility(View.INVISIBLE);
        linEnlace.setVisibility(View.INVISIBLE);
        titProducto.setVisibility(View.INVISIBLE);
        rfid.setText("");
        txtSku.getEditText().setText("");
    }

    /************ PRUEBAS DE BLOQUEO **************/

    /* access modifiers changed from: private */
    public void readKPwd(){
//        try {
//            reader.setAccessPwd("00000000");
//            reader.free();
//            try {
//                reader = RFIDWithUHF.getInstance();
//            }catch (ConfigurationException algo) {
//                Toast.makeText(getApplicationContext(), "Pasa: " + algo, Toast.LENGTH_SHORT).show();
//            }
//
//            Boolean veamos = reader.init();
//            reader.setPower(5);
//            prueba.setAccessPwd("");
            if (reader.startInventoryTag(0,0, 0)) {
                tagBLock = null;
                String[] result = null;
                result = reader.readTagFromBuffer();
                if (result != null) {
                    assert false;
                    tagBLock = reader.convertUiiToEPC(result[1]);
                }
            }
//            tagBLock = null;
//            String[] result = null;
//            result = reader.readTagFromBuffer();
//            if (result != null) {
//                assert false;
//                tagBLock = reader.convertUiiToEPC(result[0]);
//            }
            if(tagBLock != null) {
//                String epc = tagBLock;
                rfid.setText(tagBLock);
//                reader.getPwm();
//                String APwd = (String) tagBLock.readAccessPwd().getData();
//                String KPwd = (String) tagBLock.readKillPwd().getData();
//                if (APwd == null && KPwd == null) {
//                   // Toast.makeText(getApplicationContext(), "Bloqueada", Toast.LENGTH_SHORT).show();
//                    reader.setAccessPwd(Valores.ACCESS_PASSWORD);
//                    result = reader.read();
//                    tagBLock = (Tag) result.getData();
//                    if (tagBLock.readAccessPwd().getData() != null && tagBLock.readKillPwd().getData() != null) {
//                     //   Toast.makeText(getApplicationContext(), "Desbloqueada: " + APwd, Toast.LENGTH_SHORT).show();
//                        rfid.setText(tagBLock.getEPC());
//                    } else {
//                        rfid.setText(tagBLock.getEPC());
//                   //     Toast.makeText(getApplicationContext(), "bloqueda por otro codigo", Toast.LENGTH_SHORT).show();
//                    }
//
//                } else {
//                    if (APwd == null) {
//                        reader.setAccessPwd(Valores.ACCESS_PASSWORD);
//                        writeAPwd();
//                    } else {
//                        writeAPwd();
//                    }
//                }
//            }else{
//                Toasty.info(getApplicationContext(), getString(R.string.porvafor_mantenga_etiqueta_lector), Toasty.LENGTH_SHORT).show();
//            }

//        } catch (ConfigurationException e) {
//        //    Toast.makeText(getApplicationContext(), "ERROR: "+e, Toast.LENGTH_SHORT).show();
//            e.printStackTrace();
//         //   return false;
//        }


    }
    /* access modifiers changed from: private */
//    public void writeKPwd(){
//        try {
//
//            if (!this.tagBLock.writeKillPwd(Valores.KILL_PASSWORD).isSuccess()) {
//                Toasty.info(getApplicationContext(), getString(R.string.porvafor_mantenga_etiqueta_lector), Toasty.LENGTH_SHORT).show();
//                return false;
//            }
//            bloquearMemoria();
//            return true;
//        } catch (ReaderException e) {
//            e.printStackTrace();
//            return false;
//        }
//    }

    /* access modifiers changed from: private */
//    public void writeAPwd(){
//        try {
//
//            if (!this.tagBLock.writeAccessPwd(Valores.ACCESS_PASSWORD).isSuccess()) {
//                Toasty.info(getApplicationContext(), getString(R.string.porvafor_mantenga_etiqueta_lector), Toasty.LENGTH_SHORT).show();
//                return false;
//            }
//
//            return writeKPwd();
//        } catch (ReaderException e) {
//
//            e.printStackTrace();
//            return false;
//        }
//    }

//    public void bloquearMemoria() {
//        try {
//            LockFields fields = new LockFields(LockFields.EPC | LockFields.KILL_PWD | LockFields.ACCESS_PWD);
//            reader.lockMem(Valores.ACCESS_PASSWORD,LockType.LOCK, fields);
//            if (reader.lock(fields, LockType.LOCK, Valores.ACCESS_PASSWORD).isSuccess()) {
//                rfid.setText(tagBLock.getEPC());
//            } else {
//             //   Toast.makeText(getApplicationContext(), "Lock EPC", Toast.LENGTH_SHORT).show();
//            }
//
//        } catch (Exception e) {
//            Toast.makeText(getApplicationContext(), "Error: " + e, Toast.LENGTH_SHORT).show();
//            e.printStackTrace();
//
//        }
    }

    @Override
    public void getBarcode(String barcode) {
        buscaItemSku(barcode);
        sonido.playBeep();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        reader.free();
    }


}
