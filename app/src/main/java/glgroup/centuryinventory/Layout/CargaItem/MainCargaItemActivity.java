package glgroup.centuryinventory.Layout.CargaItem;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.NavUtils;

import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.webkit.MimeTypeMap;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

//import org.apache.poi.hssf.usermodel.HSSFRow;
//import org.apache.poi.hssf.usermodel.HSSFSheet;
//import org.apache.poi.hssf.usermodel.HSSFWorkbook;
//import org.apache.poi.ss.usermodel.Cell;
//import org.apache.poi.ss.usermodel.CellType;
//import org.apache.poi.ss.usermodel.Row;

import org.apache.poi.hssf.usermodel.HSSFDateUtil;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellValue;
import org.apache.poi.ss.usermodel.FormulaEvaluator;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.util.WorkbookUtil;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import es.dmoral.toasty.Toasty;
import glgroup.centuryinventory.Layout.MainActivity;
import glgroup.centuryinventory.R;
import glgroup.centuryinventory.Services.Adaptador.AdaptadorDocumento;
import glgroup.centuryinventory.Services.Alert.Alerta;
import glgroup.centuryinventory.Services.Model.CargaDescarga;
import glgroup.centuryinventory.Services.Model.Documento;
import glgroup.centuryinventory.Services.SQLite.SQLiteQuery;

public class MainCargaItemActivity extends AppCompatActivity implements View.OnClickListener {
    //Lista de la vista
    ListView lista;

    //lista de elementos obtenidos del excel
    ArrayList<Documento> documentos = new ArrayList<>();

    //valor para obtener le respuesta del archivo a seleccionar
    private int VALOR_RETORNO = 1;

    //botones de la vista
    Button select, cancelar, almacenar;

    //Titulo de la vista
    TextView titulo;

    //adaptador para la informacion del excel
    AdaptadorDocumento adaptadorDocumento;

    //grupo de elementos que se muestra y ocultan
    LinearLayout vistaDatos;

    //lista de elementos obtenidos del excel
    ArrayList<String[]> arrayDatosExcel;

    //lista de items no insertados
    List<String> datosError;

    //SQLite
    SQLiteQuery sqLiteQuery;

    boolean validaCampos = true;
    boolean validaCabecera = true;

    //Alerta para mostrar ayuda de carga
    android.app.AlertDialog alertCarga, alertDescarga;

    //flecha atras
    private ImageButton atras;

    int  contadorError;
    Uri uri;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_carga_item);
        getSupportActionBar().hide();

        sqLiteQuery = new SQLiteQuery(getApplicationContext());

        lista = findViewById(R.id.listaElementos);

        titulo = findViewById(R.id.titulo_carga);

        select = findViewById(R.id.btnSelect);
        select.setOnClickListener(this);

        vistaDatos = findViewById(R.id.linearDatos);

        cancelar = findViewById(R.id.btnCancelar);
        cancelar.setOnClickListener(this);

        almacenar = findViewById(R.id.btnAlmacenar);
        almacenar.setOnClickListener(this);

        atras = findViewById(R.id.atras);
        atras.setOnClickListener(this);

    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            NavUtils.navigateUpFromSameTask(this);
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()){
            case R.id.btnSelect:
                Uri uri = Uri.parse("/storage/emulated/0/");
                Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
                //intent.setDataAndType(uri,"application/vnd.ms-excel");
                intent.setDataAndType(uri,"*/*");
                startActivityForResult(Intent.createChooser(intent, getString(R.string.chose_file)), VALOR_RETORNO);
                break;

            case R.id.btnAlmacenar:
                new almacenar().execute();
                break;

            case R.id.btnCancelar:
                limpiar();
                break;

            case R.id.atras:
                NavUtils.navigateUpFromSameTask(this);
                break;
        }

    }

    public void limpiar(){
        select.setVisibility(View.VISIBLE);
        vistaDatos.setVisibility(View.GONE);
        documentos.clear();
        validaCampos = true;
        titulo.setText(getString(R.string.busca_maestra_item));
    }

    public void almacenar(){
        contadorError = 0;
        datosError = new ArrayList<>();
        for(int i = 0; documentos.size()> i; i++){
            if(sqLiteQuery.spVal_item(documentos.get(i).getSku())){
                contadorError ++;
                datosError.add(documentos.get(i).getSku()+" - "+ documentos.get(i).getDescripcion());
                sqLiteQuery.spMod_Item(documentos.get(i).getSku(), documentos.get(i).getDescripcion());
            }else{
                sqLiteQuery.spIns_Item(documentos.get(i).getSku(), documentos.get(i).getDescripcion());
            }
        }
        if(contadorError > 0){
            itemError();

        }else{
            limpiar();
            Toasty.success(getApplicationContext(), getString(R.string.almacenado_exitoso), Toast.LENGTH_SHORT, true).show();
        }
    }

    //metodo para inicializar el lector
    public class almacenar extends AsyncTask<String, Integer, Boolean> {
        ProgressDialog dialogAlmacena;
        @Override
        protected Boolean doInBackground(String... params) {

            boolean retorno = true;

            contadorError = 0;
            datosError = new ArrayList<>();
            for(int i = 0; documentos.size()> i; i++){
                if(sqLiteQuery.spVal_item(documentos.get(i).getSku())){
                    contadorError ++;
                    datosError.add(documentos.get(i).getSku()+" - "+ documentos.get(i).getDescripcion());
                    sqLiteQuery.spMod_Item(documentos.get(i).getSku(), documentos.get(i).getDescripcion());
                }else{
                    sqLiteQuery.spIns_Item(documentos.get(i).getSku(), documentos.get(i).getDescripcion());
                }
            }
            return retorno;
        }
        @Override
        protected void onPostExecute(Boolean result) {
            super.onPostExecute(result);
            dialogAlmacena.dismiss();
            if(contadorError > 0){
                itemError();

            }else{
                limpiar();
                Toasty.success(getApplicationContext(), getString(R.string.almacenado_exitoso), Toast.LENGTH_SHORT, true).show();
            }
        }
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialogAlmacena = new ProgressDialog(MainCargaItemActivity.this);
            dialogAlmacena.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            dialogAlmacena.setMessage(getString(R.string.procesando_documento));
            dialogAlmacena.setCanceledOnTouchOutside(false);
            dialogAlmacena.show();
        }
    }





    //alerta que muestra los item en la base de datos y permite al usuario seleccionar uno de la lista
    public void itemError(){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(getString(R.string.items_actualizados));

        final List<String> listItems = datosError;

        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,android.R.layout.simple_dropdown_item_1line, listItems);
        builder.setAdapter(dataAdapter, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        });
        builder.setNegativeButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Toasty.success(getApplicationContext(), getString(R.string.almacenado_exitoso), Toast.LENGTH_SHORT, true).show();
                limpiar();
                dialog.dismiss();
            }
        });
        builder.setCancelable(false);
        AlertDialog dialog = builder.create();
        dialog.show();
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_CANCELED) {
            //Cancelado por el usuario
        }
        if ((resultCode == RESULT_OK) && (requestCode == VALOR_RETORNO)) {
            //Procesar el resultado
            uri = data.getData(); //obtener el uri content
            System.out.println(uri);
               // leer(new File("/storage/emulated/0/" + uri.getPath().replace("primary:", "").replace("document/", "")));

                new procesarLectura().execute();
//                arrayDatosExcel = leer(new File("/storage/emulated/0/" + uri.getPath().replace("primary:", "").replace("document/", "")));
//                int r = 0;
//                validaCabecera = true;
//                if (validaCampos) {
//                    for (String[] next : arrayDatosExcel) {
//                        while (validaCabecera) {
//                            if ((next[0].toLowerCase().equals("codigo") || next[0].toLowerCase().equals("código")) && (next[1].toLowerCase().equals("descripcion") || next[1].toLowerCase().equals("descripción"))) {
//                                validaCabecera = false;
//                            } else {
//                                break;
//                            }
//                        }
//                        if ((next[0] != null && !next[0].equals("BLANK") && !next[0].trim().equals("")) && (next[1] != null && !next[1].equals("BLANK") && !next[1].trim().equals(""))) {
//                            if (!(next[0].toLowerCase().equals("codigo") || next[0].toLowerCase().equals("código")) && !(next[1].toLowerCase().equals("descripcion") || next[1].toLowerCase().equals("descripción"))) {
//                                documentos.add(new Documento(next[0], next[1]));
//                            }
//                        }
//                    }
//                    select.setVisibility(View.GONE);
//                    vistaDatos.setVisibility(View.VISIBLE);
//                    titulo.setText(getString(R.string.datos_archivo));
//                    adaptadorDocumento = new AdaptadorDocumento(getApplicationContext(), documentos);
//                    lista.setAdapter(adaptadorDocumento);
//                } else {
//                    validaCabecera = false;
//                    limpiar();
//                    Alerta.alertErrorCarga(MainCargaItemActivity.this);
//                }
//                if (validaCabecera) {
//                    limpiar();
//                    Alerta.alertErrorCarga(MainCargaItemActivity.this);
//                }



//            }else{
//                Alerta.errorArchivo(MainCargaItemActivity.this);
//            }

        }
    }

    //metodo para inicializar el lector
    public class procesarLectura extends AsyncTask<String, Integer, Boolean> {
        ProgressDialog dialogAlmacena;
        @Override
        protected Boolean doInBackground(String... params) {

            boolean retorno = true;
            String prueba = "/storage/emulated/0/Download/listado items.xlsx";
            String direction = uri.getPath().replace("/raw:/", "").replace("document", "");
            arrayDatosExcel = leer(new File(direction));
//            arrayDatosExcel = leer(new File("/storage/emulated/0" + uri.getPath().replace("/raw:/", "").replace("document", "")));
            int r = 0;
            documentos.clear();
            validaCabecera = true;
            if (validaCampos) {
                for (String[] next : arrayDatosExcel) {
                    while (validaCabecera) {
                        if ((next[0].toLowerCase().trim().equals("codigo") || next[0].trim().toLowerCase().equals("código")) && (next[1].trim().toLowerCase().equals("descripcion") || next[1].trim().toLowerCase().equals("descripción"))) {
                            validaCabecera = false;
                        } else {
                            break;
                        }
                    }
                    if ((next[0] != null && !next[0].equals("BLANK") && !next[0].trim().equals("")) && (next[1] != null && !next[1].equals("BLANK") && !next[1].trim().equals(""))) {
                        if (!(next[0].trim().toLowerCase().equals("codigo") || next[0].trim().toLowerCase().equals("código")) && !(next[1].trim().toLowerCase().equals("descripcion") || next[1].trim().toLowerCase().equals("descripción"))) {
                            documentos.add(new Documento(next[0], next[1]));
                        }
                    }
                }

            } else {
                validaCabecera = false;

                retorno = false;

            }

            return retorno;
        }
        @Override
        protected void onPostExecute(Boolean result) {
            super.onPostExecute(result);
            dialogAlmacena.dismiss();

            if(result){
                select.setVisibility(View.GONE);
                vistaDatos.setVisibility(View.VISIBLE);
                titulo.setText(getString(R.string.datos_archivo));
                adaptadorDocumento = new AdaptadorDocumento(getApplicationContext(), documentos);
                lista.setAdapter(adaptadorDocumento);
                if (validaCabecera) {
                    limpiar();
                    Alerta.alertErrorCarga(MainCargaItemActivity.this);
                }
            }else{
                limpiar();
                Alerta.alertErrorCarga(MainCargaItemActivity.this);
            }



        }
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialogAlmacena = new ProgressDialog(MainCargaItemActivity.this);
            dialogAlmacena.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            dialogAlmacena.setMessage(getString(R.string.procesando_documento));
            dialogAlmacena.setCanceledOnTouchOutside(false);
            dialogAlmacena.show();
        }
    }

    public ArrayList leer(File excelFile) {
        ArrayList<String[]> arrayDatos = new ArrayList<>();
        try {

            InputStream stream = new FileInputStream(excelFile);
            XSSFWorkbook workbook = new XSSFWorkbook(stream);
            XSSFSheet sheet = workbook.getSheetAt(0);
            int rowsCount = sheet.getPhysicalNumberOfRows();
            FormulaEvaluator formulaEvaluator = workbook.getCreationHelper().createFormulaEvaluator();
            int cellCount = 0;
            String[] datos = new String[2];
            for (int r = 0; r < rowsCount; r++) {
                Row row = sheet.getRow(r);
                cellCount = 0;
                int cellsCount = row.getPhysicalNumberOfCells();
                for (int c = 0; c < cellsCount; c++) {
                    cellCount++;
                    datos[cellCount-1] = getCellAsString(row, c, formulaEvaluator);
                    if(cellCount == 2){
                        break;
                    }
//                    String value = getCellAsString(row, c, formulaEvaluator);
//                    String cellInfo = "r:" + r + "; c:" + c + "; v:" + value;
//                    Log.d("lecturas",cellInfo);
                }
                Log.d("datos1: ", String.valueOf(datos[0] + datos[1]));
                arrayDatos.add(datos);
                datos = new String[2];
            }

        } catch (Exception e) {
            /* proper exception handling to be here */

        }
        return arrayDatos;
    }

    protected String getCellAsString(Row row, int c, FormulaEvaluator formulaEvaluator) {
        String value = "";
        try {
            Cell cell = row.getCell(c);
            CellValue cellValue = formulaEvaluator.evaluate(cell);
            switch (cellValue.getCellType()) {
                case Cell.CELL_TYPE_BOOLEAN:
                    value = ""+cellValue.getBooleanValue();
                    break;
                case Cell.CELL_TYPE_NUMERIC:
                    double numericValue = cellValue.getNumberValue();
                    if(HSSFDateUtil.isCellDateFormatted(cell)) {
                        double date = cellValue.getNumberValue();
                        SimpleDateFormat formatter =
                                new SimpleDateFormat("dd/MM/yy");
                        value = formatter.format(HSSFDateUtil.getJavaDate(date));
                    } else {
                        value = ""+numericValue;
                    }
                    break;
                case Cell.CELL_TYPE_STRING:
                    value = ""+cellValue.getStringValue();
                    break;
                default:
            }
        } catch (NullPointerException e) {
            /* proper error handling should be here */

        }
        return value;
    }

//    public ArrayList<String[]> readExcelFileToArray(File excelFile){
//        ArrayList<String[]> arrayDatos = new ArrayList<>();
//        InputStream excelStream = null;
//        try {
//            excelStream = new FileInputStream(excelFile);
//            // High level representation of a workbook.
//            // Representación del más alto nivel de la hoja excel.
//            HSSFWorkbook hssfWorkbook = new HSSFWorkbook(excelStream);
//            // We chose the sheet is passed as parameter.
//            // Elegimos la hoja que se pasa por parámetro.
//            HSSFSheet hssfSheet = hssfWorkbook.getSheetAt(0);
//            // An object that allows us to read a row of the excel sheet, and extract from it the cell contents.
//            // Objeto que nos permite leer un fila de la hoja excel, y de aquí extraer el contenido de las celdas.
//            if(hssfSheet.getPhysicalNumberOfRows() != 0) {
//                HSSFRow hssfRow = hssfSheet.getRow(hssfSheet.getTopRow());
//                String[] datos = new String[hssfRow.getLastCellNum()];
//                // For this example we'll loop through the rows getting the data we want
//                // Para este ejemplo vamos a recorrer las filas obteniendo los datos que queremos
//                int cellCount = 0;
//                for (Row row : hssfSheet) {
//                    cellCount = 0;
//                    for (Cell cell : row) {
//                        cellCount++;
//                        datos[cell.getColumnIndex()] =
//                                (cell.getCellTypeEnum() == CellType.STRING) ? cell.getStringCellValue() :
//                                        (cell.getCellTypeEnum() == CellType.NUMERIC) ? "" + cell.getNumericCellValue() :
//                                                (cell.getCellTypeEnum() == CellType.BOOLEAN) ? "" + cell.getBooleanCellValue() :
//                                                        (cell.getCellTypeEnum() == CellType.BLANK) ? "BLANK" :
//                                                                (cell.getCellTypeEnum() == CellType.FORMULA) ? "FORMULA" :
//                                                                        (cell.getCellTypeEnum() == CellType.ERROR) ? "ERROR" : "";
//                        if(cellCount == 2){
//                            break;
//                        }
//                    }
//                    if (cellCount > 2) {
//                        validaCampos = false;
//                        break;
//                    }
//                    arrayDatos.add(datos);
//                    datos = new String[hssfRow.getLastCellNum()];
//                }
//            }else{
//                validaCampos = false;
//            }
//        } catch (FileNotFoundException fileNotFoundException) {
//            System.out.println("The file not exists (No se encontró el fichero): " + fileNotFoundException);
//        } catch (IOException ex) {
//            System.out.println("Error in file procesing (Error al procesar el fichero): " + ex);
//        } finally {
//            System.out.println("Error in file processing after close it (Error al procesar el fichero después de cerrarlo): " );
////            try {
////          //      excelStream.close();
////            } catch (IOException ex) {
////                System.out.println("Error in file processing after close it (Error al procesar el fichero después de cerrarlo): " + ex);
////            }
//        }
//        return arrayDatos;
//    }




}
