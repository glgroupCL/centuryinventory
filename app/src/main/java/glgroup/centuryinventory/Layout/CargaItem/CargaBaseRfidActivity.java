package glgroup.centuryinventory.Layout.CargaItem;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.NavUtils;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

//import org.apache.poi.hssf.usermodel.HSSFRow;
//import org.apache.poi.hssf.usermodel.HSSFSheet;
//import org.apache.poi.hssf.usermodel.HSSFWorkbook;
//import org.apache.poi.ss.usermodel.Cell;
//import org.apache.poi.ss.usermodel.CellType;
//import org.apache.poi.ss.usermodel.Row;

import org.apache.poi.hssf.usermodel.HSSFDateUtil;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellValue;
import org.apache.poi.ss.usermodel.FormulaEvaluator;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

import es.dmoral.toasty.Toasty;
import glgroup.centuryinventory.Layout.Desenlace.MainDesenlaceActivity;
import glgroup.centuryinventory.Layout.Desenlace.MainDesenlaceMasivoActivity;
import glgroup.centuryinventory.Layout.Inventario.MainInventarioActivity;
import glgroup.centuryinventory.Layout.Inventario.MainInventarioProcesadoActivity;
import glgroup.centuryinventory.R;
import glgroup.centuryinventory.Services.Adaptador.AdaptadorCargaDescarga;
import glgroup.centuryinventory.Services.Adaptador.AdaptadorDocumento;
import glgroup.centuryinventory.Services.Alert.Alerta;
import glgroup.centuryinventory.Services.Filtro.Filtro;
import glgroup.centuryinventory.Services.Memory.Local;
import glgroup.centuryinventory.Services.Model.CargaDescarga;
import glgroup.centuryinventory.Services.Model.Documento;
import glgroup.centuryinventory.Services.SQLite.SQLiteQuery;

public class CargaBaseRfidActivity extends AppCompatActivity implements View.OnClickListener{

    //flecha atras
    private ImageButton atras;

    //botones de la vista
    Button select, btnLimpiar, btnAlmacenar;

    TextView tvTituloCargaBase;

    //valor para obtener le respuesta del archivo a seleccionar
    private int VALOR_RETORNO = 1;

    //lista de elementos obtenidos del excel
    ArrayList<String[]> arrayDatosExcel;

    //base de datos local
    SQLiteQuery sqLiteQuery;

    //valida los campos del excel
    boolean validaCampos = true;
    boolean validaCabecera = true;

    //adaptador para mostrar datos del excel
    private AdaptadorCargaDescarga mAdapter;

    //seccion en donde se muestran los datos
    private LinearLayout linearDatos;

    //lista de la interfaz para mostrar datos
    private ListView listaElementos;

    //lista que almacena la informacion del excel
    ArrayList<CargaDescarga> listaEnlaces = new ArrayList<>();

    Uri uri;
    ArrayList<CargaDescarga> listaError = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_carga_base_rfid);
        getSupportActionBar().hide();
        init();

    }

    /** inicio de los elementos de la vista **/
    public void init() {
        atras = findViewById(R.id.atras);
        atras.setOnClickListener(this);

        select = findViewById(R.id.btnSelect);
        select.setOnClickListener(this);

        btnLimpiar = findViewById(R.id.btnLimpiar);
        btnLimpiar.setOnClickListener(this);

        btnAlmacenar = findViewById(R.id.btnAlmacenar);
        btnAlmacenar.setOnClickListener(this);

        tvTituloCargaBase = findViewById(R.id.tvTituloCargaBase);

        linearDatos = findViewById(R.id.linearDatos);

        listaElementos = findViewById(R.id.listaElementos);

        sqLiteQuery = new SQLiteQuery(getApplicationContext());
    }

    /** metodo para controlar la accion del boton regresar **/
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            NavUtils.navigateUpFromSameTask(this);
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.atras:
                NavUtils.navigateUpFromSameTask(this);
                break;

            case R.id.btnSelect:
                /** se permite al usuario seleccionar el archivo desde sus carpetas **/
                Uri uri = Uri.parse("/storage/emulated/0/");
                Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
                //intent.setDataAndType(uri,"application/vnd.ms-excel");
                intent.setDataAndType(uri,"*/*");
                startActivityForResult(Intent.createChooser(intent, getString(R.string.chose_file)), VALOR_RETORNO);
                break;

            case R.id.btnLimpiar:
                limpiar();
                break;

            case R.id.btnAlmacenar:
                alertAdvertencia(getApplicationContext(), getLayoutInflater(), CargaBaseRfidActivity.this);
                //almacena();
                break;
        }

    }

    /** limpia los elementos de la vista **/
    public void limpiar(){
        select.setVisibility(View.VISIBLE);
        listaEnlaces.clear();
        linearDatos.setVisibility(View.INVISIBLE);
        listaElementos.setAdapter(null);
        tvTituloCargaBase.setText(getString(R.string.buscar_archivo_base));
    }

    /** proceso para almacenar la informacion del excel **/
    public void almacena(){

        ArrayList<CargaDescarga> listaAuxDescarga = sqLiteQuery.spSel_AuxDescarga();
        listaError.clear();
        /** se agrega a la maestra el sku y su descripcion **/
        for(int i = 0; listaAuxDescarga.size() > i; i++){
            /** se valida si existe o no para actualizar o insertar **/
            if(sqLiteQuery.spVal_item(listaAuxDescarga.get(i).getSku())){
                sqLiteQuery.spMod_Item(listaAuxDescarga.get(i).getSku(), listaAuxDescarga.get(i).getDescripcion());
            }else{
                sqLiteQuery.spIns_Item(listaAuxDescarga.get(i).getSku(), listaAuxDescarga.get(i).getDescripcion());
            }
        }

        /** se genera el enlace de elementos **/
        for(int i = 0; listaAuxDescarga.size() > i; i++){
            /** se valida si el rfid ingresado en la base de datos local **/
            if(sqLiteQuery.spVal_rfid(listaAuxDescarga.get(i).getCantidad())){
                //Toasty.warning(getApplicationContext(), "El rfid ya se encuentra registrado", Toast.LENGTH_SHORT, true).show();
                /** si ya esta enlazado se agrega a una lista diferente para informar al usuario al finalizar el proceso **/
                listaError.add(new CargaDescarga(listaAuxDescarga.get(i).getSku(),listaAuxDescarga.get(i).getDescripcion(),listaAuxDescarga.get(i).getCantidad()));
            }else{
                /** se obtiene el id del sku para realizar el enlace **/
                String idSku = sqLiteQuery.spSel_idItem_sku_ins(listaAuxDescarga.get(i).getSku());
                if(sqLiteQuery.spIns_rfid(listaAuxDescarga.get(i).getCantidad(), idSku) > 0){
                  //  Toasty.success(getApplicationContext(), "Enlazado correctamente", Toast.LENGTH_SHORT, true).show();
                }else{
                  //  Toasty.error(getApplicationContext(), "Error al enlazar", Toast.LENGTH_SHORT, true).show();
                    listaError.add(new CargaDescarga(listaAuxDescarga.get(i).getSku(),listaAuxDescarga.get(i).getDescripcion(),listaAuxDescarga.get(i).getCantidad()));
                }
            }
        }
        /** si existen errores en el almacenado, se muestran los items **/
        if(listaError.size() > 0){
            Alerta.alertListErrorCargaBase(CargaBaseRfidActivity.this, listaError);
        }else{
            Toasty.success(getApplicationContext(),getString(R.string.carga_correcta), Toasty.LENGTH_SHORT).show();
        }
        limpiar();
    }

    //metodo para inicializar el lector
    public class almacenarLectura extends AsyncTask<String, Integer, Boolean> {
        ProgressDialog dialogAlmacena;
        @Override
        protected Boolean doInBackground(String... params) {

            ArrayList<CargaDescarga> listaAuxDescarga = sqLiteQuery.spSel_AuxDescarga();
            listaError.clear();
            /** se agrega a la maestra el sku y su descripcion **/
            for(int i = 0; listaAuxDescarga.size() > i; i++){
                /** se valida si existe o no para actualizar o insertar **/
                if(sqLiteQuery.spVal_item(listaAuxDescarga.get(i).getSku())){
                    sqLiteQuery.spMod_Item(listaAuxDescarga.get(i).getSku(), listaAuxDescarga.get(i).getDescripcion());
                }else{
                    sqLiteQuery.spIns_Item(listaAuxDescarga.get(i).getSku(), listaAuxDescarga.get(i).getDescripcion());
                }
            }

            /** se genera el enlace de elementos **/
            for(int i = 0; listaAuxDescarga.size() > i; i++){
                /** se valida si el rfid ingresado en la base de datos local **/
                if(sqLiteQuery.spVal_rfid(listaAuxDescarga.get(i).getCantidad())){
                    //Toasty.warning(getApplicationContext(), "El rfid ya se encuentra registrado", Toast.LENGTH_SHORT, true).show();
                    /** si ya esta enlazado se agrega a una lista diferente para informar al usuario al finalizar el proceso **/
                    listaError.add(new CargaDescarga(listaAuxDescarga.get(i).getSku(),listaAuxDescarga.get(i).getDescripcion(),listaAuxDescarga.get(i).getCantidad()));
                }else{
                    /** se obtiene el id del sku para realizar el enlace **/
                    String idSku = sqLiteQuery.spSel_idItem_sku_ins(listaAuxDescarga.get(i).getSku());
                    if(sqLiteQuery.spIns_rfid(listaAuxDescarga.get(i).getCantidad(), idSku) > 0){
                        //  Toasty.success(getApplicationContext(), "Enlazado correctamente", Toast.LENGTH_SHORT, true).show();
                    }else{
                        //  Toasty.error(getApplicationContext(), "Error al enlazar", Toast.LENGTH_SHORT, true).show();
                        listaError.add(new CargaDescarga(listaAuxDescarga.get(i).getSku(),listaAuxDescarga.get(i).getDescripcion(),listaAuxDescarga.get(i).getCantidad()));
                    }
                }
            }


            return true;
        }
        @Override
        protected void onPostExecute(Boolean result) {
            super.onPostExecute(result);
            dialogAlmacena.dismiss();
            /** si existen errores en el almacenado, se muestran los items **/
            if(listaError.size() > 0){
                Alerta.alertListErrorCargaBase(CargaBaseRfidActivity.this, listaError);
            }else{
                Toasty.success(getApplicationContext(),getString(R.string.carga_correcta), Toasty.LENGTH_SHORT).show();
            }
            limpiar();

        }
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialogAlmacena = new ProgressDialog(CargaBaseRfidActivity.this);
            dialogAlmacena.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            dialogAlmacena.setMessage(getString(R.string.almacenando_informacion));
            dialogAlmacena.setCanceledOnTouchOutside(false);
            dialogAlmacena.show();
        }
    }


    /** metodo de escucha para obtener el archivo seleccionado por el usuario **/
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_CANCELED) {
            //Cancelado por el usuario
        }
        if ((resultCode == RESULT_OK) && (requestCode == VALOR_RETORNO)) {
            //Procesar el resultado
            uri = data.getData(); //obtener el uri content
            System.out.println(uri);
            /** se valida que la ruta sea accesible **/
            if(!uri.toString().contains("com.android.providers.downloads")) {
                listaEnlaces.clear();
                new procesarLectura().execute();


            }else{
                /** si la ruta contiene un tramo no accesible, se muestra un error y su mensaje de ayuda **/
                Alerta.errorArchivo(CargaBaseRfidActivity.this);
            }

        }
    }

    //metodo para inicializar el lector
    public class procesarLectura extends AsyncTask<String, Integer, Boolean> {
        ProgressDialog dialogAlmacena;
        @Override
        protected Boolean doInBackground(String... params) {

            arrayDatosExcel = leer(new File("/storage/emulated/0/" + uri.getPath().replace("primary:", "").replace("document/", "")));
            validaCabecera = true;
            /** se limpia la tabla auxiliar para el proceso de datos **/
            sqLiteQuery.clearauxDescarga();

            /** si el archivo seleccionado no corresponde a un documento xlsx, la validacion de campos será false **/
            if (validaCampos) {
                /** se recorre el arreglo para validar y guardar los datos **/
                for (String[] next : arrayDatosExcel) {
                    while (validaCabecera) {
                        /** se las cabeceras son correctas, la validacion se cambia el estado a falso **/
                        if ((next[0].trim().toLowerCase().equals("rfid")) && (next[2].trim().toLowerCase().equals("descripcion") || next[2].trim().toLowerCase().equals("descripción")) && (next[1].trim().toLowerCase().equals("sku"))) {
                            validaCabecera = false;
                        } else {
                            break;
                        }
                    }
                    /** se valida que los campos no se encuentren vacíos o que correspondan a las cabeceras, luego se guardan los datos **/
                    if ((next[0] != null && !next[0].trim().equals("BLANK") && !next[0].trim().equals("")) && (next[1] != null && !next[1].trim().equals("BLANK") && !next[1].trim().equals("")) && (next[2] != null && !next[2].trim().equals("BLANK")  && !next[2].trim().equals(""))) {
                        if (!(next[0].trim().toLowerCase().equals("rfid")) && !(next[2].trim().toLowerCase().equals("descripcion") || next[2].trim().toLowerCase().equals("descripción")) && !(next[1].trim().toLowerCase().equals("sku"))) {
                            sqLiteQuery.spIns_auxDescarga(next[1], next[2],next[0]);
                        }
                    }
                }
            }else{
                /** si el documento no es correcto, se muestra un mensaje de error junto a una ayuda **/
                validaCabecera = false;
               // Alerta.alertErrorCargaBase(CargaBaseRfidActivity.this);
            }


            return true;
        }
        @Override
        protected void onPostExecute(Boolean result) {
            super.onPostExecute(result);
            dialogAlmacena.dismiss();
            /** si la validacion de las cabeceras es true, es poque ocurrio un error o éstas no corresponden **/
            if (validaCabecera) {
                Alerta.alertErrorCargaBase(CargaBaseRfidActivity.this);
            }else{
                if(validaCampos){
                    /**em caso contrario se realiza la carga de items **/
                    tvTituloCargaBase.setText(getString(R.string.contenido_excel_cargado));
                    linearDatos.setVisibility(View.VISIBLE);
                    select.setVisibility(View.GONE);
                    listaEnlaces = sqLiteQuery.spSel_EnlacesCargados();
                    mAdapter = new AdaptadorCargaDescarga(getApplicationContext(), listaEnlaces);
                    listaElementos.setAdapter(mAdapter);
                }else {
                    Alerta.alertErrorCargaBase(CargaBaseRfidActivity.this);
                }
            }

        }
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialogAlmacena = new ProgressDialog(CargaBaseRfidActivity.this);
            dialogAlmacena.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            dialogAlmacena.setMessage("Procesando documento");
            dialogAlmacena.setCanceledOnTouchOutside(false);
            dialogAlmacena.show();
        }
    }

    /** se procesa el archivo seleccionado **/
    public ArrayList leer(File excelFile) {
        ArrayList<String[]> arrayDatos = new ArrayList<>();
        try {

            InputStream stream = new FileInputStream(excelFile);
            XSSFWorkbook workbook = new XSSFWorkbook(stream);
            XSSFSheet sheet = workbook.getSheetAt(0);
            int rowsCount = sheet.getPhysicalNumberOfRows();
            FormulaEvaluator formulaEvaluator = workbook.getCreationHelper().createFormulaEvaluator();
            int cellCount = 0;
            String[] datos = new String[3];
            for (int r = 0; r < rowsCount; r++) {
                Row row = sheet.getRow(r);
                cellCount = 0;
                int cellsCount = row.getPhysicalNumberOfCells();
                for (int c = 0; c < cellsCount; c++) {
                    cellCount++;
                    datos[cellCount-1] = getCellAsString(row, c, formulaEvaluator);
                    /** se limita la lectura solo a 3 columnas **/
                    if(cellCount == 3){
                        break;
                    }
//                    String value = getCellAsString(row, c, formulaEvaluator);
//                    String cellInfo = "r:" + r + "; c:" + c + "; v:" + value;
//                    Log.d("lecturas",cellInfo);
                }
                Log.d("datos1: ", String.valueOf(datos[0] + datos[1]));
                arrayDatos.add(datos);
                datos = new String[3];
            }

        } catch (Exception e) {
            validaCampos = false;
            /* proper exception handling to be here */

        }
        return arrayDatos;
    }

    /** se obtienen los datos del excel por su tipo y se transforman a string **/
    protected String getCellAsString(Row row, int c, FormulaEvaluator formulaEvaluator) {
        String value = "";
        try {
            Cell cell = row.getCell(c);
            CellValue cellValue = formulaEvaluator.evaluate(cell);
            switch (cellValue.getCellType()) {
                case Cell.CELL_TYPE_BOOLEAN:
                    value = ""+cellValue.getBooleanValue();
                    break;
                case Cell.CELL_TYPE_NUMERIC:
                    double numericValue = cellValue.getNumberValue();
                    if(HSSFDateUtil.isCellDateFormatted(cell)) {
                        double date = cellValue.getNumberValue();
                        SimpleDateFormat formatter =
                                new SimpleDateFormat("dd/MM/yy");
                        value = formatter.format(HSSFDateUtil.getJavaDate(date));
                    } else {
                        value = ""+numericValue;
                    }
                    break;
                case Cell.CELL_TYPE_STRING:
                    value = ""+cellValue.getStringValue();
                    break;
                default:
            }
        } catch (NullPointerException e) {
            /* proper error handling should be here */

        }
        return value;
    }


    /** pregunta el proceso de carga **/
    public void alertAdvertencia(Context context, LayoutInflater layoutInflater, final Activity activity){
        final android.app.AlertDialog alertEnlace;
        final android.app.AlertDialog.Builder busqueda = new android.app.AlertDialog.Builder(activity);


        View dialog = layoutInflater.inflate(R.layout.alert_advertencia_carga, null);

        final Button btnGuardar = dialog.findViewById(R.id.btnGuardar);
        final Button btnBorrar = dialog.findViewById(R.id.btnBorrar);

        busqueda.setView(dialog);
        alertEnlace = busqueda.create();
        alertEnlace.setCancelable(true);

        btnGuardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertEnlace.dismiss();
                new almacenarLectura().execute();

            }
        });
        btnBorrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertEnlace.dismiss();
                confirmarEliminar(getApplicationContext(), getLayoutInflater(), CargaBaseRfidActivity.this);
            }
        });
        alertEnlace.show();
    }

    /** pregunta el proceso de carga **/
    public void confirmarEliminar(Context context, LayoutInflater layoutInflater, final Activity activity){
        final android.app.AlertDialog alertEnlace;
        final android.app.AlertDialog.Builder busqueda = new android.app.AlertDialog.Builder(activity);


        View dialog = layoutInflater.inflate(R.layout.confirma_carga, null);

        final Button btnCancelar = dialog.findViewById(R.id.btnCancelar);
        final Button btnContinuar = dialog.findViewById(R.id.btnContinuar);

        busqueda.setView(dialog);
        alertEnlace = busqueda.create();
        alertEnlace.setCancelable(true);

        btnCancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertEnlace.dismiss();
            }
        });
        btnContinuar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertEnlace.dismiss();
                eliminarDatos();
            }
        });
        alertEnlace.show();
    }

    public void eliminarDatos(){
        sqLiteQuery.clearItem();
        sqLiteQuery.clearRfid();
        sqLiteQuery.clearResumenInventario();
        sqLiteQuery.clearInventario();
        new almacenarLectura().execute();
    }
}