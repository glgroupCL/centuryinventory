package glgroup.centuryinventory.Layout.CargaItem;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.app.NavUtils;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputLayout;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.util.WorkbookUtil;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.regex.Pattern;

import es.dmoral.toasty.Toasty;
import glgroup.centuryinventory.Layout.Inventario.MainInventarioProcesadoActivity;
import glgroup.centuryinventory.Layout.MainActivity;
import glgroup.centuryinventory.R;
import glgroup.centuryinventory.Services.Adaptador.AdaptadorCargaDescarga;
import glgroup.centuryinventory.Services.EnvioCorreo.CorreoInformacion;
import glgroup.centuryinventory.Services.Memory.Local;
import glgroup.centuryinventory.Services.Model.CargaDescarga;
import glgroup.centuryinventory.Services.SQLite.SQLiteQuery;
import glgroup.centuryinventory.Services.Utilidades.DescargaBase;

public class DescargaBaseActivity extends AppCompatActivity implements View.OnClickListener{

    //flecha atras
    private ImageButton atras;

    //base de datos
    private SQLiteQuery sqLiteQuery;

    private AdaptadorCargaDescarga mAdapter;

    private ListView listaElementos;

    private Button btnDescarga, btnCancelar;


    AlertDialog observacionCorreo;

    String directory_path, nombreDoc, correoDestino;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_descarga_base);
        getSupportActionBar().hide();
        init();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
            //Verifica permisos para Android 6.0+
            checkExternalStoragePermission();
        }
    }

    public void init() {

        sqLiteQuery = new SQLiteQuery(getApplicationContext());

        atras = findViewById(R.id.atras);
        atras.setOnClickListener(this);

        btnDescarga = findViewById(R.id.btnDescarga);
        btnDescarga.setOnClickListener(this);

        btnCancelar = findViewById(R.id.btnCancelar);
        btnCancelar.setOnClickListener(this);

        listaElementos = findViewById(R.id.listaElementos);

        ArrayList<CargaDescarga> listaEnlaces = sqLiteQuery.spSel_Enlaces();
        if(listaEnlaces.size() > 0){
            mAdapter = new AdaptadorCargaDescarga(getApplicationContext(), listaEnlaces);
            listaElementos.setAdapter(mAdapter);
        }

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.atras:
            case R.id.btnCancelar:
                NavUtils.navigateUpFromSameTask(this);
                break;

            case R.id.btnDescarga:

                generarDescarga();

                break;
        }

    }


    public void generarDescarga(){
        final AlertDialog.Builder builder = new AlertDialog.Builder(DescargaBaseActivity.this);
        final LayoutInflater inflater = getLayoutInflater();

        View dialoglayout = inflater.inflate(R.layout.vista_nombre_descarga, null);

        final TextInputLayout observacion = dialoglayout.findViewById(R.id.edObservacion);
        final TextInputLayout correo = dialoglayout.findViewById(R.id.edCorreo);

        Button cancelar = dialoglayout.findViewById(R.id.btnCancelar);
        Button enviar = dialoglayout.findViewById(R.id.btnEnviar);
        builder.setView(dialoglayout);

        observacionCorreo = builder.create();
        observacionCorreo.setCancelable(false);
        enviar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String tituloDoc = "";
                if(observacion.getEditText().getText().toString().trim().equals("")){
                    tituloDoc = "Respaldo_Base_RFID";
                }else{
                    tituloDoc = observacion.getEditText().getText().toString();

                }
                nombreDoc = tituloDoc;
                if(correo.getEditText().getText().toString().trim().equals("")){
                    observacionCorreo.dismiss();
                    correoDestino = "";
                    exportarBase();
                }else{
                    if(validarEmail(correo.getEditText().getText().toString().trim())){
                        observacionCorreo.dismiss();
                        correoDestino = correo.getEditText().getText().toString().trim();
                        exportarBase();
                    }else{
                        correo.setErrorEnabled(true);
                        correo.setError(getString(R.string.correo_invalido));
                    }
                }



//                DescargaBase descargaBase = new DescargaBase();
//                descargaBase.exportarBase(getApplicationContext(), tituloDoc, DescargaBaseActivity.this);



            }
        });
        cancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                observacionCorreo.dismiss();
            }
        });
        observacionCorreo.show();
    }

    private boolean validarEmail(String email) {
        Pattern pattern = Patterns.EMAIL_ADDRESS;
        return pattern.matcher(email).matches();
    }

    public void exportarBase() {



        directory_path = Environment.getExternalStorageDirectory()+ "/ExcelDescargaBase/";
        File file = new File(Environment.getExternalStorageDirectory(), "ExcelDescargaBase");
        if(isStoragePermissionGranted(DescargaBaseActivity.this)) {
            if (!file.exists()) {
                file.mkdir();
            }
        }
        new procesarLectura().execute();

        //      onWriteClick(nombre, directory_path, activity);
        // Export SQLite DB as EXCEL FILE
//        SQLiteToExcel sqliteToExcel = new SQLiteToExcel(context, adminSQLOpenHelper.DATABASE_NAME, directory_path);
//        sqliteToExcel.exportSpecificTables(table1List, nombreArchivo , new SQLiteToExcel.ExportListener() {
//            @Override
//            public void onStart() {
//            }
//
//            @Override
//            public void onCompleted(String filePath) {
//                Toasty.success(context, "Exportado correctamente en: "+directory_path, Toast.LENGTH_SHORT).show();
//                activity.startActivity(new Intent(activity, MainActivity.class));
//            }
//
//            @Override
//            public void onError(Exception e) {
//                Toasty.error(context, "Error al exportar la información", Toast.LENGTH_SHORT, true).show();
//            }
//        });
    }
    //metodo para inicializar el lector
    public class procesarLectura extends AsyncTask<String, Integer, Boolean> {
        ProgressDialog dialogAlmacena;
        @Override
        protected Boolean doInBackground(String... params) {

            boolean retorno;

            SQLiteQuery sqLiteQuery = new SQLiteQuery(getApplicationContext());
            sqLiteQuery.clearauxDescarga();
            sqLiteQuery.spSel_DescargaBase();

            //    printlnToUser("writing xlsx file");
            XSSFWorkbook workbook = new XSSFWorkbook();
            XSSFSheet sheet = workbook.createSheet(WorkbookUtil.createSafeSheetName("BaseRFID"));

            Row row = sheet.createRow(0);
            Cell cell = row.createCell(0);
            Cell cell2 = row.createCell(1);
            Cell cell3 = row.createCell(2);
            cell.setCellValue("RFID");
            cell2.setCellValue("SKU");
            cell3.setCellValue("DESCRIPCION");

            ArrayList<CargaDescarga> data = sqLiteQuery.spSel_AuxDescarga();

            for (int i=1; i<= data.size();i++) {
                row = sheet.createRow(i);
                cell = row.createCell(0);
                cell2 = row.createCell(1);
                cell3 = row.createCell(2);

                cell.setCellValue(data.get(i-1).getCantidad());
                cell2.setCellValue(data.get(i-1).getSku());
                cell3.setCellValue(data.get(i-1).getDescripcion());
            }
            String outFileName = nombreDoc+".xlsx";
            try {
                //       printlnToUser("writing file " + outFileName);
                //   File cacheDir = getCacheDir();
                File outFile = new File(directory_path, outFileName);
                OutputStream outputStream = new FileOutputStream(outFile.getAbsolutePath(), true);
                workbook.write(outputStream);
                workbook.close();
                outputStream.flush();
                outputStream.close();

                retorno = true;

            } catch (Exception e) {
                retorno = false;
                System.out.println(e.toString());
            }
            if(!correoDestino.equals("")){
                CorreoInformacion correoInformacion = new CorreoInformacion();
                correoInformacion.EnviarCorreo(directory_path,  nombreDoc+".xlsx",getApplicationContext(), "BaseRFID Century Inventory", "Base RFID ", correoDestino);

            }
            return retorno;
        }
        @Override
        protected void onPostExecute(Boolean result) {
            super.onPostExecute(result);
            dialogAlmacena.dismiss();
            if(result){
                Toasty.success(getApplicationContext(), getString(R.string.exportado_correctamente)+": "+directory_path, Toast.LENGTH_SHORT).show();
                startActivity(new Intent(DescargaBaseActivity.this, MainActivity.class));
            }else{
                Toasty.error(getApplicationContext(), getString(R.string.error_al_exportar), Toast.LENGTH_SHORT).show();
            }



        }
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialogAlmacena = new ProgressDialog(DescargaBaseActivity.this);
            dialogAlmacena.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            dialogAlmacena.setMessage(getString(R.string.exportando_informacion));
            dialogAlmacena.setCanceledOnTouchOutside(false);
            dialogAlmacena.show();
        }
    }

    public  boolean isStoragePermissionGranted(Activity activity) {
        if (Build.VERSION.SDK_INT >= 23) {
            if (activity.checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_GRANTED) {
                return true;
            } else {
                ActivityCompat.requestPermissions(activity,
                        new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
                return false;
            }
        }
        else { //permission is automatically granted on sdk<23 upon installation
            return true;
        }
    }

    private void checkExternalStoragePermission() {
        int permissionCheck = ContextCompat.checkSelfPermission(
                this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
            Log.i("Mensaje", "No se tiene permiso para leer.");
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 225);
        } else {
            Log.i("Mensaje", "Se tiene permiso para leer!");
        }
    }
}