package glgroup.centuryinventory.Layout.Configuracion;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.NavUtils;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Patterns;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputLayout;

import java.util.ArrayList;
import java.util.regex.Pattern;

import es.dmoral.toasty.Toasty;
import glgroup.centuryinventory.Layout.MainActivity;
import glgroup.centuryinventory.R;
import glgroup.centuryinventory.Services.Adaptador.AdaptadorCorreo;
import glgroup.centuryinventory.Services.Model.Correo;
import glgroup.centuryinventory.Services.SQLite.SQLiteQuery;

public class MainConfiguracionActivity extends AppCompatActivity {

    private ArrayList<Correo> listaCorreos;

    private RecyclerView mRecyclerView;
    private AdaptadorCorreo mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;

    private ImageButton add;

    //alerta solicitud de datos para nueva ubicacion
    private AlertDialog alertCorreos, alertModifica;

    SQLiteQuery adminSQLiteQuery;

    //flecha atras
    private ImageButton atras;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_configuracion);
        getSupportActionBar().hide();

        adminSQLiteQuery = new SQLiteQuery(getApplicationContext());

        atras = findViewById(R.id.atras);
        atras.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               startActivity(new Intent(MainConfiguracionActivity.this, MainActivity.class));
            }
        });

        add = findViewById(R.id.agregarCorreo);
        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                agregaCorreo();
            }
        });

        cargaCorreos();
        buildRecyclerView();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            NavUtils.navigateUpFromSameTask(this);
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    public void removeItem(final int position) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(MainConfiguracionActivity.this);
        final LayoutInflater inflater = getLayoutInflater();

        View dialoglayout = inflater.inflate(R.layout.confirma_eliminar, null);

        Button cancelar = dialoglayout.findViewById(R.id.btnCancelar);
        Button eliminar = (Button) dialoglayout.findViewById(R.id.btnEliminar);
        builder.setView(dialoglayout);

        alertCorreos = builder.create();
        alertCorreos.setCancelable(false);
        eliminar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(adminSQLiteQuery.spDel_Correo(selectNombreItem(position)) > 0 ){
                    listaCorreos.remove(position);
                    mAdapter.notifyItemRemoved(position);
                    alertCorreos.dismiss();
                    Toasty.success(getApplicationContext(), getString(R.string.correo_eliminado), Toast.LENGTH_SHORT, true).show();
                }

            }
        });
        cancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertCorreos.dismiss();
            }
        });
        alertCorreos.show();
    }

    public String selectNombreItem(int position) {
        //listaUbicaciones.get(position).changeText1(text);
        String dato = listaCorreos.get(position).getCorreo();
        mAdapter.notifyItemChanged(position);
        return dato;
    }

    public void cargaCorreos() {
        listaCorreos = new ArrayList<>();
        listaCorreos = adminSQLiteQuery.spSel_ListaCorreo();
    }

    public void buildRecyclerView() {
        mRecyclerView = findViewById(R.id.recyclerView);
        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(this);
        mAdapter = new AdaptadorCorreo(listaCorreos);

        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setAdapter(mAdapter);

        mAdapter.setOnItemClickListener(new AdaptadorCorreo.OnItemClickListener() {
            @Override
            public void onItemClick(int position) {
                Toast.makeText(getApplicationContext(),  selectNombreItem(position), Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onDeleteClick(int position) {
                removeItem(position);
            }

            @Override
            public void onEditClick(int position) {
                ArrayList<String> data = adminSQLiteQuery.spSel_infoCorreo(selectNombreItem(position));
                if(data.size() > 0){
                    modificaCorreo(data.get(0), data.get(1), position);
                }

            }
        });
    }

    //alert para solicitar datos del correo
    public void agregaCorreo(){
        final AlertDialog.Builder builder = new AlertDialog.Builder(MainConfiguracionActivity.this);
        final LayoutInflater inflater = getLayoutInflater();

        View dialoglayout = inflater.inflate(R.layout.agrega_correo, null);

        final TextInputLayout descripcion = dialoglayout.findViewById(R.id.edDescripcion);
        final TextInputLayout correo = dialoglayout.findViewById(R.id.edCorreo);
        Button cancelar = dialoglayout.findViewById(R.id.btnCancelar);
        Button agregar = (Button) dialoglayout.findViewById(R.id.btnAgregar);
        builder.setView(dialoglayout);

        alertCorreos = builder.create();
        alertCorreos.setCancelable(false);
        agregar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String desc = descripcion.getEditText().getText().toString();
                String corr = correo.getEditText().getText().toString();
                if(!desc.equals("")){
                    descripcion.setErrorEnabled(false);
                    if(!corr.equals("")) {
                        if(validarEmail(corr)){
                            correo.setErrorEnabled(false);
                            if (!adminSQLiteQuery.spSel_ExistCorreo(corr)) {
                                long insert = adminSQLiteQuery.spIns_correo(desc, corr);
                                if (insert > 0) {
                                    listaCorreos.add(new Correo(desc, corr));
                                    mAdapter.notifyDataSetChanged();
                                    alertCorreos.dismiss();
                                    Toasty.success(getApplicationContext(), getString(R.string.correo_almacenado), Toast.LENGTH_SHORT, true).show();
                                } else {
                                    Toasty.error(getApplicationContext(), getString(R.string.error_guardar_correo), Toast.LENGTH_SHORT, true).show();
                                }
                            } else {
                                Toasty.warning(getApplicationContext(), getString(R.string.correo_registrado), Toast.LENGTH_SHORT, true).show();
                            }
                        }else{
                            correo.setErrorEnabled(true);
                            correo.setError(getString(R.string.correo_invalido));
                        }

                    }else{
                        correo.setErrorEnabled(true);
                        correo.setError(getString(R.string.campo_requerido));
                    }
                }else{
                    descripcion.setErrorEnabled(true);
                    descripcion.setError(getString(R.string.campo_requerido));
                    if(!corr.equals("")) {
                        correo.setErrorEnabled(false);
                    }
                }


            }
        });
        cancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertCorreos.dismiss();
            }
        });
        alertCorreos.show();
    }

    public void modificaCorreo(String descEdit, final String correoEdit, final int position){
        final AlertDialog.Builder builder = new AlertDialog.Builder(MainConfiguracionActivity.this);
        final LayoutInflater inflater = getLayoutInflater();

        View dialoglayout = inflater.inflate(R.layout.edita_correo, null);

        final TextInputLayout descripcion = dialoglayout.findViewById(R.id.edDescripcion);
        final TextInputLayout correo = dialoglayout.findViewById(R.id.edCorreo);
        descripcion.getEditText().setText(descEdit);
        correo.getEditText().setText(correoEdit);
        Button cancelar = dialoglayout.findViewById(R.id.btnCancelar);
        Button agregar = (Button) dialoglayout.findViewById(R.id.btnAgregar);
        builder.setView(dialoglayout);

        alertModifica = builder.create();
        alertModifica.setCancelable(false);
        agregar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String desc = descripcion.getEditText().getText().toString();
                String corr = correo.getEditText().getText().toString();
                if(!desc.equals("")){
                    descripcion.setErrorEnabled(false);
                    if(!corr.equals("")) {
                        if(validarEmail(corr)){
                            correo.setErrorEnabled(false);
                            if(adminSQLiteQuery.spDel_Correo(correoEdit)>0) {
                                listaCorreos.remove(position);
                                mAdapter.notifyItemRemoved(position);
                                if (!adminSQLiteQuery.spSel_ExistCorreo(corr)) {
                                    long insert = adminSQLiteQuery.spIns_correo(desc, corr);
                                    if (insert > 0) {
                                        listaCorreos.add(new Correo(desc, corr));
                                        buildRecyclerView();
                                        alertModifica.dismiss();
                                        Toasty.success(getApplicationContext(), getString(R.string.correo_modificado), Toast.LENGTH_SHORT, true).show();
                                    } else {
                                        Toasty.error(getApplicationContext(), getString(R.string.error_modificar_correo), Toast.LENGTH_SHORT, true).show();
                                    }
                                } else {
                                    Toasty.warning(getApplicationContext(), getString(R.string.correo_registrado), Toast.LENGTH_SHORT, true).show();
                                }
                            }
                        }else{
                            correo.setErrorEnabled(true);
                            correo.setError(getString(R.string.correo_invalido));
                        }

                    }else{
                        correo.setErrorEnabled(true);
                        correo.setError(getString(R.string.campo_requerido));
                    }
                }else{
                    descripcion.setErrorEnabled(true);
                    descripcion.setError(getString(R.string.campo_requerido));
                }


            }
        });
        cancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertModifica.dismiss();
            }
        });
        alertModifica.show();
    }

    private boolean validarEmail(String email) {
        Pattern pattern = Patterns.EMAIL_ADDRESS;
        return pattern.matcher(email).matches();
    }
}
