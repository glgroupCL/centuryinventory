package glgroup.centuryinventory.Layout.update;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import java.io.File;

import glgroup.centuryinventory.Layout.MainActivity;
import glgroup.centuryinventory.R;
import glgroup.centuryinventory.Services.Memory.Local;

public class MainNewsActivity extends AppCompatActivity {

    Button init;
    TextView news;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_news);
        if(getSupportActionBar() != null){
            getSupportActionBar().hide();
        }

        news = (TextView)findViewById(R.id.novedades);

        init = (Button)findViewById(R.id.iniciar);
        init.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EliminarArchivos(new File("/storage/sdcard0/apk") );
                Local.setData("web", getApplicationContext(),"update", "");
               // startActivity(new Intent(MainNewsActivity.this, MainLoginActivity.class));
                startActivity(new Intent(MainNewsActivity.this, MainActivity.class));
            }
        });
    }

    void EliminarArchivos(File ArchivoDirectorio) { /* Parametro File (Ruta) */
        if (ArchivoDirectorio.isDirectory()) /* Si es Directorio */
        {
            /* Recorremos sus Hijos y los ELiminamos */
            for (File hijo : ArchivoDirectorio.listFiles())
                EliminarArchivos(hijo); /*Recursividad Para Saber si no hay Subcarpetas */
        }
        else
            ArchivoDirectorio.delete(); /* Si no, se trata de un File ,solo lo Eliminamos*/
    }
}
