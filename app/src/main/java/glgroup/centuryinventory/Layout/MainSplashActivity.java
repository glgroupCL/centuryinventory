package glgroup.centuryinventory.Layout;

import android.Manifest;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.os.AsyncTask;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.UiThread;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.Locale;

import es.dmoral.toasty.Toasty;
import glgroup.centuryinventory.Layout.update.MainNewsActivity;
import glgroup.centuryinventory.Layout.update.MainUpdateActivity;
import glgroup.centuryinventory.R;
import glgroup.centuryinventory.Services.Alert.Alerta;
import glgroup.centuryinventory.Services.Internet.AccesoInternet;
import glgroup.centuryinventory.Services.Memory.Local;
import glgroup.centuryinventory.Services.Model.ConexionCentury;
import glgroup.centuryinventory.Services.Pad.Constantes;
import glgroup.centuryinventory.Services.Retrofit.ApiService;
import glgroup.centuryinventory.Services.Retrofit.Modelos.Datos;
import glgroup.centuryinventory.Services.Utilidades.Imei;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainSplashActivity extends AppCompatActivity {

    String versionactual = "3.10.5463";

    String versionfirebase, url_firebase;

    //valida acceso a internet
    AccesoInternet accesoInternet = new AccesoInternet();

    TextView version;
    private FirebaseFirestore db = FirebaseFirestore.getInstance();
    private CollectionReference conexionReference;


    /** cambio de idioma **/
    private Locale locale;
    private Configuration config = new Configuration();


    private Call<Datos> resAutenticacion;

    private  Datos datosAutenticacion;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_splash);
        getSupportActionBar().hide();
        Local.setData("animacion", getApplicationContext(), "inicio", "1");


        String lang = Local.getData("lenguaje",getApplicationContext(),"lang");
        if(!lang.equals("")){
            locale = new Locale(lang);
            config.locale =locale;
            getResources().updateConfiguration(config, null);
        }

        version = findViewById(R.id.version);
        version.setText(getString(R.string.version)+": "+versionactual);
        obtieneDatosConexion();
        //initTime(0);
    }

    private void initTime(final int Time) {

//        //Luego de tiempo determinado se cambia de pantalla
//        Thread timerTread = new Thread(){
//            public void run(){
//                try{
//                    try {
//                        Thread.sleep(Time);
//
//                    } catch (InterruptedException e) {
//                        e.printStackTrace();
//                    }
//
//                } finally {
//
//                    if(accesoInternet.isOnlineNet()) {
//                        //comprueba si existe una nueva version de la aplicación en firebase
//                        FirebaseDatabase database = FirebaseDatabase.getInstance();
//                        DatabaseReference referencia_version, referencia_url, referencia_conexion;
//
//                        //se obtiene la url de descarga de la app
//                        referencia_url = database.getReference("H460urlCenturyInventory");
//                        referencia_url.addListenerForSingleValueEvent(new ValueEventListener() {
//                            @Override
//                            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
//                                String dataAux = dataSnapshot.getValue().toString();
//                                Local.setData("update",getApplicationContext(),"urlUpdate", dataAux);
//
//                            }
//
//                            @Override
//                            public void onCancelled(@NonNull DatabaseError databaseError) {
//                                Toast.makeText(getApplicationContext(), "Url: " + databaseError.getMessage(), Toast.LENGTH_SHORT).show();
//                            }
//                        });
//
//                        //se obtiene la url de descarga de la app
//                        referencia_conexion = database.getReference("conexionCenturyInventory");
//                        referencia_conexion.addListenerForSingleValueEvent(new ValueEventListener() {
//                            @Override
//                            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
//                                String dataAux = dataSnapshot.getValue().toString();
//                                Local.setData("update",getApplicationContext(),"urlConexion", dataAux);
//                            }
//
//                            @Override
//                            public void onCancelled(@NonNull DatabaseError databaseError) {
//                                Toast.makeText(getApplicationContext(), "Url: " + databaseError.getMessage(), Toast.LENGTH_SHORT).show();
//                            }
//                        });
//
//                        //se obtiene la versión existente en firebase
//                        referencia_version = database.getReference("H460versionCenturyInventory");
//                        referencia_version.addListenerForSingleValueEvent(new ValueEventListener() {
//
//                            @Override
//                            //se compara la versión en firebase y se compara con la versión actual de la aplicación
//                            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
//                                versionfirebase = (dataSnapshot.getValue().toString());
//                                if (!versionfirebase.trim().equals(versionactual.trim())) {
//
//                                    //si las versiones son diferentes se procede a actualizar la aplicación
//                                    Intent actualizar = new Intent(MainSplashActivity.this, MainUpdateActivity.class);
//                                    actualizar.putExtra("version", versionfirebase);
//                                    actualizar.putExtra("url", url_firebase);
//                                    startActivity(actualizar);
//                                } else {
//                                    //en caso contrario se inicia la aplicación de forma normal
//                                    String update = Local.getData("web", getApplicationContext(), "update");
//                                    if (update.equals("")) {
//                                        startActivity(new Intent(MainSplashActivity.this, MainActivity.class));
//                                    } else {
//                                        startActivity(new Intent(MainSplashActivity.this, MainNewsActivity.class));
//                                    }
//
//                                }
//                            }
//                            @Override
//                            public void onCancelled(@NonNull DatabaseError databaseError) {
//                                Toast.makeText(getApplicationContext(), "Versión: " + databaseError.getMessage(), Toast.LENGTH_SHORT).show();
//                            }
//                        });
//                    }else{
//                        if(Local.getData("autenticacion", getApplicationContext(), "valido").equals("1")) {
//                            //en caso contrario se inicia la aplicación de forma normal
//                            startActivity(new Intent(MainSplashActivity.this, MainActivity.class));
//                        }else{
//                            runOnUiThread(new Runnable() {
//                                @Override
//                                public void run() {
//                                    sinConexion();
//                                }
//                            });
//                        }
//
//                    }
//
//                }
//            }
//        };
//        timerTread.start();

    }

    public void sinConexion(){
        final AlertDialog alertaCredencial;
        final AlertDialog.Builder builder = new AlertDialog.Builder(MainSplashActivity.this);

        View dialoglayout = getLayoutInflater().inflate(R.layout.sin_acceso_internet, null);

        final Button cancelar = dialoglayout.findViewById(R.id.btnCancelar);
        Button comprobar =  dialoglayout.findViewById(R.id.btnComprobar);
        builder.setView(dialoglayout);

        alertaCredencial = builder.create();
        alertaCredencial.setCancelable(false);
        comprobar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertaCredencial.dismiss();
                new verificaConexion().execute();
            }
        });
        cancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertaCredencial.dismiss();
                finishAffinity(); System.exit(0);
            }
        });
        alertaCredencial.show();
    }

    public void obtieneDatosConexion(){
        ApiService.reiniciarApiService();
        String coll = Constantes.COLLECTION_CONEXION;
        String doc = Constantes.DOCUMENT_CONEXION;
        conexionReference = db.collection(coll);
        conexionReference.document(doc)
                .get()
                .addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                    @Override
                    public void onSuccess(DocumentSnapshot documentSnapshot) {
                        if (documentSnapshot.exists()) {

                            ConexionCentury conexionCentury = documentSnapshot.toObject(ConexionCentury.class);
                            if (conexionCentury != null) {
                                Local.setData("conexion", getApplicationContext(), "urlConexion", conexionCentury.getConexion()+conexionCentury.getPuerto());

                                String imei = Imei.getImei(MainSplashActivity.this);
                                if(!conexionCentury.getImeiRestringido().contains(imei) || imei.equals("")) {

                                    /** verificar actualizacion **/
                                    if (Constantes.VERSION_APP.equals(conexionCentury.getVersion())) {
                                        startActivity(new Intent(MainSplashActivity.this, MainActivity.class));
                                        finish();
                                    } else {
                                        Intent intent = new Intent(MainSplashActivity.this, MainUpdateActivity.class);
                                        intent.putExtra(Constantes.PARAMETRO_ACT, conexionCentury.getUrlActualizacion());
                                        startActivity(intent);
                                        finish();
                                    }
                                }else{
                                    Alerta.AlertaAutorizado(getString(R.string.no_autorizado), getString(R.string.aviso), MainSplashActivity.this);
                                    //Toast.makeText(getApplicationContext(),"El dispositivo no está autorizado para ingresar al sistema", Toast.LENGTH_LONG).show();
                                }
                            }
                        }else{
                            Toast.makeText(getApplicationContext(),getString(R.string.error_obtener_datos), Toast.LENGTH_LONG).show();
                        }
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {

                        Toast.makeText(getApplicationContext(),getString(R.string.error_obtener_datos), Toast.LENGTH_LONG).show();
                    }
                });
    }




    public class verificaConexion extends AsyncTask<String, Integer, Boolean> {
        ProgressDialog validaInternet;

        @Override
        protected Boolean doInBackground(String... params) {
            if (accesoInternet.isOnlineNet()) {
                validaInternet.cancel();
                return true;
            } else {
                validaInternet.cancel();
                return false;
            }
        }

        @Override
        protected void onPostExecute(Boolean result) {
            super.onPostExecute(result);
            if (result) {
                startActivity(new Intent(MainSplashActivity.this, MainSplashActivity.class));
            } else {
                sinConexion();
            }
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            validaInternet = new ProgressDialog(MainSplashActivity.this);
            validaInternet.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            validaInternet.setMessage(getString(R.string.comprobando_conexion));
            validaInternet.setCanceledOnTouchOutside(false);
            validaInternet.show();
        }
    }



}
