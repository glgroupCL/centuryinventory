package glgroup.centuryinventory.Layout.Inventario;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.NavUtils;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputLayout;

import java.util.ArrayList;

import es.dmoral.toasty.Toasty;
import glgroup.centuryinventory.Layout.MainActivity;
import glgroup.centuryinventory.R;
import glgroup.centuryinventory.Services.Adaptador.AdaptadorInventario;
import glgroup.centuryinventory.Services.EnvioCorreo.CorreoInformacion;
import glgroup.centuryinventory.Services.Memory.Local;
import glgroup.centuryinventory.Services.Model.Inventario;
import glgroup.centuryinventory.Services.SQLite.SQLiteQuery;

public class MainInventarioProcesadoActivity extends AppCompatActivity {

    //muestra etiquetas leidas
    private TextView etiquetas;

    //lista de los datos
    ListView listaDatos;

    //SQLite
    SQLiteQuery sqLiteQuery;
    //Lista de datos
    ArrayList<Inventario> listaResumen = new ArrayList<>();

    //adaptador de la vista
    private AdaptadorInventario adaptadorInventario;

    private Button enviarInformacion;

    //flecha atras
    private ImageButton atras;

    AlertDialog observacionCorreo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_inventario_procesado);
        getSupportActionBar().hide();

        etiquetas = findViewById(R.id.etiquetasLeidas);
        etiquetas.setText(getString(R.string.etiquetas_leidas)+": "+Local.getData("inventario", getApplicationContext(), "cantidadLeida"));

        sqLiteQuery = new SQLiteQuery(getApplicationContext());
        listaResumen = sqLiteQuery.spSel_resumenInventario();

        adaptadorInventario = new AdaptadorInventario(getApplicationContext(), listaResumen);
        listaDatos = findViewById(R.id.resultadoInventario);
        listaDatos.setAdapter(adaptadorInventario);

        enviarInformacion = findViewById(R.id.btnEnviar);
        enviarInformacion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                enviarCorreo();

            }
        });

        atras = findViewById(R.id.atras);
        atras.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sqLiteQuery.clearResumenInventario();
                startActivity(new Intent(MainInventarioProcesadoActivity.this, MainInventarioActivity.class));
            }
        });
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        //se validan los botones presionados
        //boton regresar
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            sqLiteQuery.clearResumenInventario();
            NavUtils.navigateUpFromSameTask(this);
            return true;
        }

        return super.onKeyDown(keyCode, event);
    }


    public void enviarCorreo(){
        final AlertDialog.Builder builder = new AlertDialog.Builder(MainInventarioProcesadoActivity.this);
        final LayoutInflater inflater = getLayoutInflater();

        View dialoglayout = inflater.inflate(R.layout.vista_observaciones, null);

        final TextInputLayout observacion = dialoglayout.findViewById(R.id.edObservacion);

        Button cancelar = dialoglayout.findViewById(R.id.btnCancelar);
        Button enviar = dialoglayout.findViewById(R.id.btnEnviar);
        builder.setView(dialoglayout);

        observacionCorreo = builder.create();
        observacionCorreo.setCancelable(false);
        enviar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(observacion.getEditText().getText().toString().equals("")){
                    Local.setData("correo", getApplicationContext(), "observacion", "Documento Century Inventory");
                }else{
                    Local.setData("correo", getApplicationContext(), "observacion", observacion.getEditText().getText().toString());
                }
                observacionCorreo.dismiss();
                new procesarLectura().execute();


            }
        });
        cancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                observacionCorreo.dismiss();
            }
        });
        observacionCorreo.show();
    }

    //metodo para inicializar el lector
    public class procesarLectura extends AsyncTask<String, Integer, Boolean> {
        ProgressDialog procesaCorreo;
        @Override
        protected Boolean doInBackground(String... params) {
            CorreoInformacion correoInformacion = new CorreoInformacion();
            correoInformacion.exportar(getApplicationContext(), MainInventarioProcesadoActivity.this);
            while (Local.getData("correo", getApplicationContext(), "estado").equals("")){ }
            procesaCorreo.cancel();
            return true;
        }
        @Override
        protected void onPostExecute(Boolean result) {
            super.onPostExecute(result);
            if(Local.getData("correo", getApplicationContext(), "estado").equals("true")){
                Toasty.success(getApplicationContext(), getString(R.string.correo_enviado), Toast.LENGTH_SHORT, true).show();
            }
            if(Local.getData("correo", getApplicationContext(), "estado").equals("false")){
                Toasty.error(getApplicationContext(), getString(R.string.error_enviar_correo), Toast.LENGTH_SHORT, true).show();
            }
            startActivity(new Intent(MainInventarioProcesadoActivity.this, MainActivity.class));
        }
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            procesaCorreo = new ProgressDialog(MainInventarioProcesadoActivity.this);
            procesaCorreo.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            procesaCorreo.setMessage(getString(R.string.enviando_correo));
            procesaCorreo.setCanceledOnTouchOutside(false);
            procesaCorreo.show();
        }
    }
}
