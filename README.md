# Century Inventory

_Ésta aplicación es un sistema inspirado en centurycloud, pero más simplificado, permite la carga de maestras de items para su posterior enlace, adicionalmente el proceso de inventario realiza una lectura masiva de items, el resultado de ésta será reflejado al final de la vista, adicionalmente se enviará al correo almacenado un documento xls con los datos_


### Pre-requisitos 📋

_Dispositivo de lectura ALR-H460, exclusivo de ALIEN_


### Instalación 🔧

_Es necesario contar con android studio para el caso de clonar el proyecto, de lo contrario es necesaria la apk para su instalación directa_

_AndroidStudio_

```
Instala AndroidStudio, en el dispositivo móvil habilita el modo desarrollador a través de configuración->Acerca del dispositivo-> presionar varias veces "número de compilación" hasta que se indique que el modo desarrollador está habilitado. 
Posteriormente, al volver al menú principal se mostrará la opción de "opciones de programador", debes ingresar a la opción y habilitar la "depuración usb".

Para finalizar conectar el dispositvo al equipo a través del cable usb, abrir el proyecto en android studio y correr el programa.
```

_Instalar apk_

```
Conecta el dispositivo móvil al equipo y copia el archivo de aplicación a éste. Luego desconecta el móvil, accede a la ruta e instala la aplicación.
```

## Despliegue 📦

_Para el correcto uso de las funciones de la aplicación, es necesario contar con un usuario de prueba, adicionalmente configurar la ruta para la conexión y los puertos necesarios_

## Construido con 🛠️

* [AndroidStudio](https://developer.android.com/studio) - El framework para móviles más usado
* [Maven](https://maven.apache.org/) - Manejador de dependencias


## Autores ✒️

_Equipo de excelentes intelectuales_

* **Erick Oyarce** - *Desarrollador*
* **Diego Arancibia** - *Desarrollador*
* **Claudio Fuentes** - *Documentación*


## Licencia 📄

Este proyecto está bajo la Licencia de GLGroup

## Expresiones de Gratitud 🎁

* Comenta a otros sobre este proyecto 📢
* Invita una cerveza 🍺 o un café ☕ a alguien del equipo. 
* Da las gracias públicamente 🤓.
* etc.



---
⌨️ con ❤️ por el Equipo de GLGroup 😊